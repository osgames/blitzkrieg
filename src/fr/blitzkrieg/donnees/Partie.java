/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.donnees;

import java.awt.Color ;
import java.util.ArrayList ;
import java.util.Arrays ;
import java.util.List ;
import java.util.Observer ;
import java.util.Random ;

import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.traitements.Regles ;
import fr.blitzkrieg.utiles.igraphique.mvc.Observable ;



// Données sur une partie en cours
public class Partie extends Observable
{

    // Configuration de la partie
    private Regles  regles ;                    // Règles de la partie courante
    private String  nom ;                       // Nom de la partie (pour les scénarios surtout)
    private String  description ;               // Description de la partie (pour les scénarios surtout)
    private int     nbTours ;                   // Nombre de tours
    private int     nbPointsCase ;              // Nombre de points rapportés par une case
    private int     nbPointsVille ;             // Nombre de points rapportés par une ville
    private boolean invasionsNavalesPermises ;  // Indique si les attaques à travers la mer sont autorisées

    // Objets évoluant avec la partie
    private List<Joueur> joueurs ;              // Joueurs participant à la partie
    private Terrain      terrain ;              // Terrain de jeu

    // Déroulement de la partie elle-même
    private int     numTour ;                   // Numéro du tour courant
    private Integer iJoueurCourant ;            // Indice du joueur courant dans la liste des joueurs (null : plus de joueurs actifs)
    private boolean commencee ;                 // Indique si la partie a commencé
    private boolean terminee ;                  // Indique si la partie est terminée
    private boolean suspendue ;                 // Indique que la partie est suspendue dans un état non-observable
    
    
    
    // Constructeur
    // TODO A supprimer
    public Partie (Regles regles)
    {
        this.nom = "" ;
        this.description = "" ;
        this.regles = regles ;
        
        this.numTour        =  0 ;      // Tour de mise en place
        this.iJoueurCourant = -1 ;      // TODO Vérifier que ça fonctionne bien...
        
        this.invasionsNavalesPermises = true ;
        
// Partie bidon en dur pour les tests
// TODO Dans les scénarios prédéfinis ou générées automatiquement il faut pouvoir indiquer le nombre de
//      troupes initial pour chaque joueur ; par contre pas forcément de nombre d'actions initial, si ?
//          => On peut toujours le prévoir dans le code, on mettra ça à 0, et ce sera prêt pour si on
//             veut le préciser dans les scénarios (grande percée, ...)
//          => Oui, besoin pour le scénario en croix de la vieille version de Pendulous
this.joueurs = Arrays.asList (new Joueur ("Joueur 1", 2, 1, 4, 2, 10, 0, Color.RED),
                              new Joueur ("Joueur 2", 3, 0, 5, 1, 15, 0, Color.GRAY)) ;
this.terrain = new Terrain() ;
this.nbPointsCase  =  1 ;
this.nbPointsVille = 10 ;
this.nbTours = 5 ;
// (propriétaires et visibilité)
Random genAlea = new Random (693) ;
for (int y = 0 ; y < this.terrain.hauteur() ; y++)
for (int x = 0 ; x < this.terrain.largeur() ; x++)
{
    Case caseCourante = this.terrain.caseEn(x,y) ;
    
    // (donner la terre à quelqu'un)
    if (caseCourante.type() != TypeCase.CASE_MER)
        caseCourante.choisirProprietaireInitial (this.joueurs.get (genAlea.nextInt (2))) ;
    
//    if (! (0 <= x && x < 3) && (2 <= y && y < 5))
//        caseCourante.decouvrir (this.joueurs.get(0)) ;
//    if (! (5 <= x && x < 7) && (4 <= y && y < 10))
//        caseCourante.decouvrir (this.joueurs.get(1)) ;
}
// (déterminer la visibilité)
this.terrain.mettreAJourVisibiliteJoueurs() ;
// (calculer les cases mortes)
this.terrain.mettreAJourCasesMortes() ;

        // Lier les règles à la partie
        this.regles.associerPartie (this) ;
    }
    
    
    // Constructeur
    public Partie (Regles regles,
                   String nom, String description,
                   int nbTours, int nbPointsCase, int nbPointsVille, boolean invasionsNavalesPermises,
                   List<Joueur> joueurs, Terrain terrain)
    {
        // Mémorise les paramètres
        this.regles                   = regles ;
        this.nom                      = nom ;
        this.description              = description ;
        this.nbTours                  = nbTours ;
        this.nbPointsCase             = nbPointsCase ;
        this.nbPointsVille            = nbPointsVille ;
        this.invasionsNavalesPermises = invasionsNavalesPermises ;
        this.joueurs                  = joueurs ;
        this.terrain                  = terrain ;
        
        // Lier les règles à la partie
        this.regles.associerPartie (this) ;
        
        // Initialiser les autres attributs
        this.numTour        =  0 ;      // Tour de mise en place
        this.iJoueurCourant = -1 ;      // TODO Vérifier que ça fonctionne bien...
        this.commencee      = false ;
        this.terminee       = false ;
        this.suspendue      = false ;
        
        // Déterminer la visibilité des joueurs
        this.terrain.mettreAJourVisibiliteJoueurs() ;
        
        // Calculer les cases mortes
        this.terrain.mettreAJourCasesMortes() ;
    }
    
    
    // Constructeur complet (pour le chargement d'une partie sauvegardée)
    // TODO A voir : il faudrait sans doute ajouter le booléen "suspendu" à la liste des attributs
    //      sauvegardés/chargés, mais en pratique ça devrait être toujours faux (pour l'instant)
    //          => A modifier pour être sûr
    public Partie (Regles regles,
                   String nom, String description,
                   int nbTours, int nbPointsCase, int nbPointsVille, boolean invasionsNavalesPermises,
                   List<Joueur> joueurs, Terrain terrain,
                   int numTour, Integer iJoueurCourant, boolean commencee, boolean terminee)
    {
        this.regles                   = regles ;
        this.nom                      = nom ;
        this.description              = description ;
        this.nbTours                  = nbTours ;
        this.nbPointsCase             = nbPointsCase ;
        this.nbPointsVille            = nbPointsVille ;
        this.invasionsNavalesPermises = invasionsNavalesPermises ;
        this.joueurs                  = joueurs ;
        this.terrain                  = terrain ;
        this.numTour                  = numTour ;
        this.iJoueurCourant           = iJoueurCourant ;
        this.commencee                = commencee ;
        this.terminee                 = terminee ;
        
        // Lier les règles à la partie
        this.regles.associerPartie (this) ;
    }


// TODO Accesseurs juste pour la sauvegarde/chargement : à voir
public String nom ()
{
    return this.nom ;
}
public String description ()
{
    return this.description ;
}
public int iJoueurCourant ()
{
    return this.iJoueurCourant ;
}
    
    
    
// *****************************************************************************
//                  Gestion des observateurs
// *****************************************************************************
    
    
    // Quand un observateur s'inscrit pour observer la partie, il faut l'inscrire automatiquement
    //   à toutes les classes observables de la partie (les notifications ne remontent pas jusqu'à
    //   la partie elle-même)
    public void addObserver (Observer observateur)
    {
        // Inscrire l'observateur à la partie
        super.addObserver (observateur) ;
        
        // Inscrire l'observateur aux autres classes observables de la partie
        // NOTE Pour simplifier c'est a partie qui inscrit directement l'observateur aux cases du
        //      terrain au lieu de confier ça à une méthode de Terrain (qui devrait devenir observable
        //      et implémenter addObserver)
        for (Joueur joueur : this.joueurs)
            joueur.addObserver (observateur) ;
        int largeurTerrain = this.terrain.largeur() ;
        int hauteurTerrain = this.terrain.largeur() ;
        for (int x = 0 ; x < largeurTerrain ; x++)
        for (int y = 0 ; y < hauteurTerrain ; y++)
            this.terrain.caseEn(x,y).addObserver (observateur) ;
    }
    
    
    // Quand un observateur se retire de l'observation de la partie, il faut le retirer automatiquement
    //   des listes d'observateurs des autres classes observables de la partie (voir addObserver)
    public void deleteObserver (Observer observateur)
    {
        // Retirer l'observateur l'observateur à la partie
        super.deleteObserver (observateur) ;
        
        // Retirer l'observateur des autres classes observables de la partie
        for (Joueur joueur : this.joueurs)
            joueur.deleteObserver (observateur) ;
        int largeurTerrain = this.terrain.largeur() ;
        int hauteurTerrain = this.terrain.largeur() ;
        for (int x = 0 ; x < largeurTerrain ; x++)
        for (int y = 0 ; y < hauteurTerrain ; y++)
            this.terrain.caseEn(x,y).deleteObserver (observateur) ;
    }
    
    
    
// *****************************************************************************
//                  Déroulement de la partie
// *****************************************************************************
    
    
    // Lance la partie (initialisations)
    // NOTE Cette méthode ne devrait pas être appelée par autre chose que le gestionnaire de partie
    public void commencer ()
    {
        // La partie est en cours
        this.commencee = true ;

        // C'est parti !
        avertirObservateurs() ;
    }
    
    
    // Termine la partie
    // NOTE Cette méthode ne devrait pas être appelée par autre chose que le gestionnaire de partie
    public void terminer ()
    {
        this.terminee       = true ;
        this.iJoueurCourant = null ;
        avertirObservateurs() ;
    }
    
    // Indique si la partie a commencé
    public boolean commencee ()
    {
        return this.commencee ;
    }
    
    // Indique si la partie est terminée
    public boolean terminee ()
    {
        return this.terminee ;
    }
    
    // Indique si la partie est en cours (commencée et non terminée)
    public boolean enCours ()
    {
        return this.commencee && ! this.terminee ;
    }
    
    // Indique si la partie en est au tour d'initialisation
    public boolean tourInitialisation ()
    {
        return this.numTour == 0 ;
    }
    
    // Suspend l'observation de la partie (indique aux observateurs de ne plus représenter la partie)
    public void suspendreObservation ()
    {
        this.suspendue = true ;
        avertirObservateurs() ;
    }
    
    // Permet à nouveau l'observation de la partie
    public void permettreObservation ()
    {
        this.suspendue = false ;
        avertirObservateurs() ;
    }
    
    // Indique si l'observation de la partie est suspendue
    public boolean observationSuspendue ()
    {
        return this.suspendue ;
    }
    
    
    
// *****************************************************************************
//                  Règles de la partie
// *****************************************************************************
    
    
    // Renvoie les règles de la partie
    public Regles regles ()
    {
        return this.regles ;
    }

    // Renvoie le nombre de tours que dure la partie
    public int nbTours ()
    {
        return this.nbTours ;
    }

    // Renvoie le nombre de points que rapporte chaque case
    public int nbPointsCase ()
    {
        return this.nbPointsCase ;
    }

    // Renvoie le nombre de points que rapporte chaque ville
    public int nbPointsVille ()
    {
        return this.nbPointsVille ;
    }
    
    // Indique si les attaques navales sont permises
    public boolean invasionsNavalesPermises ()
    {
        return this.invasionsNavalesPermises ;
    }

    
    
// *****************************************************************************
//                  Autres méthodes
// *****************************************************************************
    
    
    // Renvoie le terrain sur lequel se joue la partie
    public Terrain terrain ()
    {
        return this.terrain ;
    }
    
    
    // Renvoie le nombre de joueurs
    public int nbJoueurs ()
    {
        return this.joueurs.size() ;
    }
    
    // Renvoie les joueurs de la partie
    public List<Joueur> joueurs ()
    {
        return new ArrayList (this.joueurs) ;
    }
    
    
    // Renvoie le numéro du tour
    public int numTour ()
    {
        return this.numTour ;
    }
    
    // Renvoie le joueur courant
    // Renvoie null s'il n'y a pas ou plus de joueur courant
    public Joueur joueurCourant ()
    {
        return (this.iJoueurCourant == null || this.iJoueurCourant == -1 ? null : this.joueurs.get (this.iJoueurCourant)) ;
    }
    
    // Indique s'il reste des joueurs
    public boolean resteDesJoueurs ()
    {
        for (Joueur joueur : this.joueurs)
        {
            if (joueur.estActif())
                return true ;
        }
        return false ;
    }
    
    
    // Passe au joueur suivant
    // Renvoie vrai si on a atteint la fin de la liste des joueurs (si un tour de jeu s'est terminé)
    // La partie se termine elle-même si le nombre de tours prévus a été atteint
    // La méthode ne doit pas être appelée s'il n'y a plus de joueurs actifs
    // NOTE Cette méthode ne devrait pas être appelée par autre chose que le gestionnaire de partie
    public boolean passerAuJoueurSuivant ()
    {
        if (! resteDesJoueurs())
            throw new IllegalStateException ("Plus de joueurs actifs dans la partie") ;
        
        // Parcourir les joueurs pour trouver le suivant
        for (int cpt = 1 ; cpt <= this.joueurs.size() ; cpt++)
        {
            int iJoueur = (this.iJoueurCourant + cpt) % this.nbJoueurs() ;
            Joueur joueur = this.joueurs.get (iJoueur) ;
            if (joueur.estActif())
            {
                // Vérification fin du tour (ensemble des joueurs)
                boolean finListeAtteinte = iJoueur <= this.iJoueurCourant ;
                if (finListeAtteinte)
                    this.numTour++ ;
                
                // Mettre à jour le joueur courant si la partie n'est pas terminée
                if (this.numTour <= this.nbTours)
                    this.iJoueurCourant = iJoueur ;
                else
                    this.terminer() ;
                
                // Renvoyer les informations sur le parcours de la liste de joueurs
                this.avertirObservateurs() ;
                return finListeAtteinte ;
            }
        }
        
        // (on ne devrait pas arriver là grâce au test des joueurs actifs en début de méthode)
        throw new IllegalStateException ("On ne devrait pas arriver là : plus de joueurs actifs dans la partie !") ;
    }
    
}
