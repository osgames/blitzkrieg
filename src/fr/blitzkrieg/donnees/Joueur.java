/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.donnees;

import java.awt.Color ;

import fr.blitzkrieg.utiles.igraphique.mvc.Observable ;



// Informations sur un joueur
// NOTE Pas de nombre d'actions/troupes par ville dans le vieux jeu apparemment ; mais dans l'ancien il
//      me semble que c'était un truc du genre 2 actions et 1 troupe par ville (dans les jeux aléatoires)
// TODO Tests unitaires (ce programme s'y prête assez)
// NOTE Pour l'instant deux joueurs sont égaux s'ils sont représentés exactement par le même objet, ce
//      qui est cohérent avec leur utlisation ; à noter quand même, pour vérifier que c'est bien ce qu'on
//      souhaite
public class Joueur extends Observable
{

    // Description du joueur
    private String  nom ;                   // Nom du joueur
    private Color   couleur ;               // Couleur du joueur
    private boolean humain ;                // Indique s'il s'agit d'un joueur humain 
    private boolean actif ;                 // Indique si le joueur est encore actif dans la partie (non-éliminé)
    
    // Caractéristiques de l'armée
    private double efficacite ;             // Efficacité des troupes du joueur
    private int    nbTroupesMax ;           // Nombre maximal de troupes
    private int    nbTroupesParTour ;       // Nombre de troupes gagnées par tour
    private int    nbTroupesParVille ;      // Nombre de troupes gagnées par ville
    private int    nbActionsParTour ;       // Nombre d'actions gagnées par tour
    private int    nbActionsParVille ;      // Nombre d'actions gagnées par ville
    
    // Données sur la partie courante
    private int nbActions ;                 // Nombre d'actions (ressources) en réserve
    private int nbTroupesReserve ;          // Nombre de troupes en réserve
    private int nbTroupesTerrain ;          // Nombre de troupes sur le terrain
    private int nbPointsVictoire ;          // Nombre de points de victoire
    
    // Statistiques sur la partie
    private int    nbAttaquesLancees ;          // Nombre d'attaques lancées
    private int    nbAttaquesReussies ;         // Nombre d'attaques réussies
    private double probaReussiteAttMoyenne ;    // Moyenne des probabilités de réussite des attaques lancées
    private int    nbDefenses ;                 // Nombre d'attaques subies
    private int    nbDefensesReussies ;         // Nombre de défenses réussies (la case a résisté)
    private double probaReussiteDefMoyenne ;    // Moyenne des probabilités de réussite des attaques subies
    
    
    
    // Constructeur
    // TODO A supprimer
    public Joueur (String nom, int nbTroupesParTour, int nbTroupesParVille, int nbActionsParTour,
                   int nbActionsParVille, int nbTroupesReserve, int nbActions, Color couleur)
    {
        this.nom = nom ;
        this.couleur = couleur ;
        this.nbTroupesParTour  = nbTroupesParTour ;
        this.nbTroupesParVille = nbTroupesParVille ;
        this.nbActionsParTour  = nbActionsParTour ;
        this.nbActionsParVille = nbActionsParVille ;
        this.efficacite = 1.0 ;
        this.nbTroupesReserve = nbTroupesReserve ;
        this.nbActions = nbActions ;
        this.nbTroupesMax = 50 ;
        this.actif = true ;         // TODO Plutôt à déterminer automatiquement en étudiant la situation (beuh... le problème c'est que ça doit être fait de l'extérieur, par exemple dans une fonction d'initialisation de la partie, qui fait ça pour tous les joueurs)
    }
    
    public Joueur (String nom, Color couleur, boolean humain,
                   double efficacite, int nbTroupesMax, int nbTroupesParTour, int nbTroupesParVille,
                   int nbActionsParTour, int nbActionsParVille,
                   int nbActionsInitial, int nbTroupesReserveInitial)
    {
        this.nom               = nom ;
        this.couleur           = couleur ;
        this.actif             = true ;
        this.humain            = humain ;
        this.efficacite        = efficacite ;
        this.nbTroupesMax      = nbTroupesMax ;
        this.nbTroupesParTour  = nbTroupesParTour ;
        this.nbTroupesParVille = nbTroupesParVille ;
        this.nbActionsParTour  = nbActionsParTour ;
        this.nbActionsParVille = nbActionsParVille ;
        this.nbActions         = nbActionsInitial ;
        this.nbTroupesReserve  = nbTroupesReserveInitial ;
        // TODO Pour l'instant à zéro, mais il faudrait sans doute une version pour les mettre à une
        //      autre valeur (au moins les points de victoire, pour les troupes ça dépend de comment
        //      on les palce sur le terrain [en principe on les compte à ce moment-là])
        this.nbTroupesTerrain = 0 ;
        this.nbPointsVictoire = 0 ;
    }
    
    // Constructeur complet, pour le chargement d'un joueur depuis une sauvegarde
    public Joueur (String nom, Color couleur, boolean humain, boolean actif,
                   double efficacite, int nbTroupesMax, int nbTroupesParTour, int nbTroupesParVille,
                     int nbActionsParTour, int nbActionsParVille,
                   int nbActions, int nbTroupesReserve, int nbTroupesTerrain, int nbPointsVictoire,
                   int nbAttaquesLancees, int nbAttaquesReussies, double probaReussiteAttMoyenne,
                   int nbDefenses,        int nbDefensesReussies, double probaReussiteDefMoyenne)
    {
        this.nom                     = nom ;
        this.couleur                 = couleur ;
        this.humain                  = humain ;
        this.actif                   = actif ;
        this.efficacite              = efficacite ;
        this.nbTroupesMax            = nbTroupesMax ;
        this.nbTroupesParTour        = nbTroupesParTour ;
        this.nbTroupesParVille       = nbTroupesParVille ;
        this.nbActionsParTour        = nbActionsParTour ;
        this.nbActionsParVille       = nbActionsParVille ;
        this.nbActions               = nbActions ;
        this.nbTroupesReserve        = nbTroupesReserve ;
        this.nbTroupesTerrain        = nbTroupesTerrain ;
        this.nbPointsVictoire        = nbPointsVictoire ;
        this.nbAttaquesLancees       = nbAttaquesLancees ;
        this.nbAttaquesReussies      = nbAttaquesReussies ;
        this.probaReussiteAttMoyenne = probaReussiteAttMoyenne ;
        this.nbDefenses              = nbDefenses ;
        this.nbDefensesReussies      = nbDefensesReussies ;
        this.probaReussiteDefMoyenne = probaReussiteDefMoyenne ;
    }


    
// TODO A voir : accesseurs pas utiles au jeu mais nécessaires pour sauvegarder la partie
//          => Indique plutôt comme protégés et permettre d'y accéder dans une classe
//             qui encapsule celle-ci, juste pour les sauvegardes/chargement ? Ca permet
//             de vérifier qu'on n'accède pas à n'importe quoi quand on écrit les règles
//             (surtout pour les modificateurs)
//      ! Ben finalement rien pour l'instant
//          ! Et peut-être rien à terme : on peut charger toutes les données puis
//            ensuite créer l'objet qui correspond


    
// *****************************************************************************
//                  Description du joueur
// *****************************************************************************
    
    
    // Renvoie le nom du joueur
    // TODO Supprimer les commentaires de tous ces accesseurs et les coller ? Ben le problème c'est
    //      que certains accesseurs sont plus complexes (mais pas forcément ici ; et par exemple pour
    //      cette partie on pourrait prendre le grand commentaire entre les lignes de "******" et
    //      le mettre pour cette série de 3-4 accesseurs)
    public String nom ()
    {
        return this.nom ;
    }
    
    // Renvoie la couleur du joueur
    public Color couleur ()
    {
        return this.couleur ;
    }
    
    // Renvoie l'efficacité des troupes du joueur
    public double efficacite ()
    {
        return this.efficacite ;
    }
    
    // Indique si le joueur est humain
    public boolean estHumain ()
    {
        return this.humain ;
    }
    
    // Indique si le joueur est actif
    public boolean estActif ()
    {
        return this.actif ;
    }
    
    
    
// *****************************************************************************
//                  Caractéristiques de l'armée
// *****************************************************************************
    

    // Nombre maximal de troupes dans l'armée
    public int nbTroupesMax ()
    {
        return this.nbTroupesMax ;
    }
    
    // Nombre de troupes de renfort fixe par tour
    public int nbTroupesParTour ()
    {
        return this.nbTroupesParTour ;
    }

    // Nombre de troupes de renfort par ville à chaque tour
    public int nbTroupesParVille ()
    {
        return this.nbTroupesParVille ;
    }
    
    // Nombre d'actions supplémentaires fixe par tour
    public int nbActionsParTour ()
    {
        return this.nbActionsParTour ;
    }

    // Nombre d'actions supplémentaires par ville à chaque tour
    public int nbActionsParVille ()
    {
        return this.nbActionsParVille ;
    }
    

    
// *****************************************************************************
//                  Partie courante (troupes, actions, score, ...)
// *****************************************************************************
    
    
    // Indique le nombre total de troupes d'un joueur (sur le terrain et dans le réserve)
    // TODO Réorganiser les méthodes de cette classe
    public int nbTroupes ()
    {
        return this.nbTroupesTerrain + this.nbTroupesReserve ;
    }
    
    // Renvoie le nombre de troupes en réserve
    public int nbTroupesReserve ()
    {
        return this.nbTroupesReserve ;
    }
    
    // Renvoie le nombre de troupes sur le terrain
    public int nbTroupesTerrain ()
    {
        return this.nbTroupesTerrain ;
    }
    
    // Renvoie le nombre d'actions disponibles
    public int nbActions ()
    {
        return this.nbActions ;
    }
    
    // Renvoie le nombre de points de victoire du joueur
    public int nbPointsVictoire ()
    {
        return this.nbPointsVictoire ;
    }
    
    // Ajoute des troupes à la réserve du joueur
    // Si le nombre de troupes maximal est dépassé, les troupes en supplément sont perdues
    public void ajouterTroupesReserves (int nbTroupes)
    {
        // Calculer le nombre de troupes à ajouter réellement, en tenant compte de la limite
        int nbPlacesDisponibles = this.nbTroupesMax - this.nbTroupes() ;
        int nbTroupesAAjouer    = Math.min (nbTroupes, nbPlacesDisponibles) ;
        
        // Ajouter les troupes
        this.nbTroupesReserve += nbTroupesAAjouer ;
        avertirObservateurs() ;
    }
    
    // Retire une troupe du terrain
    public void retirerTroupeTerrain ()
    {
        this.nbTroupesTerrain-- ;
        avertirObservateurs() ;
    }
    
    // Retire une troupe de la réserve
    public void retirerTroupeReserve ()
    {
        this.nbTroupesReserve-- ;
        avertirObservateurs() ;
    }
    
    // Transfère une troupe du terrain vers la réserve
    public void transfererDeTerrainVersReserve ()
    {
        this.nbTroupesTerrain-- ;
        this.nbTroupesReserve++ ;
        avertirObservateurs() ;
    }
    
    // Transfère une troupe de la réserve vers le terrain
    public void transfererDeReserveVersTerrain ()
    {
        this.nbTroupesReserve-- ;
        this.nbTroupesTerrain++ ;
        avertirObservateurs() ;
    }
    
    // Ajoute un certain nombre d'actions à la réserve du joueur
    public void ajouterActionsReserve (int nbActions)
    {
        this.nbActions += nbActions ;
        avertirObservateurs() ;
    }
    
    // Consomme une action du joueur
    public void consommerAction ()
    {
        if (this.nbActions < 1)
            throw new IllegalStateException ("Il n'y a pas d'action disponible") ;
        this.nbActions-- ;
        avertirObservateurs() ;
    }
    
    // Ajoute les points de victoire en paramètre aux points du joueur
    // CODE Mieux vérifier les paramètres dans les classes de données ? C'est à ça que servent ces méthodes
    //      qui ne font pas que modifier/lire les attributs, non ? Par exemple ici vérifier que le paramètre
    //      est positif (ou nul)
    public void ajouterPointsVictoire (int nbPointsVictoire)
    {
        this.nbPointsVictoire += nbPointsVictoire ;
    }
    
    
    
// *****************************************************************************
//                  Statistiques sur la partie
// *****************************************************************************
    
    
    // Note qu'une attaque a été lancée, avec la probabilité de réussite et le résultat
    public void modifStatsAttaques (double probaReussite, boolean attaqueReussie)
    {
        this.probaReussiteAttMoyenne = (this.probaReussiteAttMoyenne * this.nbAttaquesLancees + probaReussite) / (this.nbAttaquesLancees + 1) ;
        this.nbAttaquesLancees++ ;
        if (attaqueReussie)
            this.nbAttaquesReussies++ ;
    }
    
    // Note qu'une attaque a été subie, avec la probabilité de réussite (pour le défenseur) et le résultat
    public void modifStatsDefenses (double probaReussite, boolean defenseReussie)
    {
        this.probaReussiteDefMoyenne = (this.probaReussiteDefMoyenne * this.nbDefenses + probaReussite) / (this.nbDefenses + 1) ;
        this.nbDefenses++ ;
        if (defenseReussie)
            this.nbDefensesReussies++ ;
    }
    
    // Renvoie le nombre d'attaques lancées
    public int nbAttaquesLancees ()
    {
        return this.nbAttaquesLancees ;
    }
    
    // Renvoie le nombre d'attaques réussies
    public int nbAttaquesReussies ()
    {
        return this.nbAttaquesReussies ;
    }

    // Renvoie la moyenne des probabilités de réussite des attaques lancées
    public double probaReussiteAttMoyenne ()
    {
        return this.probaReussiteAttMoyenne ;
    }
    
    // Renvoie le nombre d'attaques subies
    public int nbDefenses ()
    {
        return this.nbDefenses ;
    }
    
    // Renvoie le nombre de défenses réussies
    public int nbDefensesReussies ()
    {
        return this.nbDefensesReussies ;
    }

    // Renvoie la moyenne des probabilités de réussite des défenses subies
    public double probaReussiteDefMoyenne ()
    {
        return this.probaReussiteDefMoyenne ;
    }
    
    // Renvoie la proportion d'attaques réussies
    // S'il n'y a pas eu d'attaques, renvoie 0
    public double proportionAttaquesReussies ()
    {
        return (this.nbAttaquesLancees == 0 ? 0 : (double) this.nbAttaquesReussies / this.nbAttaquesLancees) ;
    }
    
    // Renvoie la proportion de défenses réussies
    // S'il n'y a pas eu de défenses, renvoie 0
    public double proportionDefensesReussies ()
    {
        return (this.nbDefenses == 0 ? 0 : (double) this.nbDefensesReussies / this.nbDefenses) ;
    }
    
}
