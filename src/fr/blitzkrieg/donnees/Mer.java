/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.donnees;

import java.util.ArrayList ;
import java.util.List ;

import fr.blitzkrieg.donnees.Case.TypeCase ;



// Mer/lac contenue sur le terrain
public class Mer
{

    private int              numero ;       // Numéro identifiant la mer
    private List<List<Case>> cotes ;        // Listes de cases de côtes bordant la mer, classées par terre (les cases d'une même côte dans une liste)
    private List<Case>       ports ;        // Listes des ports d'une mer

    
    
    // Constructeur
    public Mer (int numero, List<List<Case>> cotes)
    {
        // Mémoriser les paramètres
        this.numero = numero ;
        this.cotes  = cotes ;
        
        // Calculer les valeurs des autres attributs
        this.ports = new ArrayList() ;
        for (Case caseCote : this.cotes())
        {
            if (caseCote.type() == TypeCase.CASE_VILLE)
                this.ports.add (caseCote) ;
        }
    }
    
    
    
    // Renvoie le numéro de la mer
    public int numero ()
    {
        return this.numero ;
    }
    
    
    // Renvoie les cases de côtes bordant la mer (une liste par terre bordant la mer)
    public List<List<Case>> cotesParTerre ()
    {
        return this.cotes ;
    }
    
    // Renvoie les cases de côtes bordant la mer (toutes les cases de côtes)
    public List<Case> cotes ()
    {
        List<Case> casesCotes = new ArrayList() ;
        for (List<Case> cases : this.cotes)
            casesCotes.addAll (cases) ;
        return casesCotes ;
    }
    
    
    // Indique si le joueur en paramètre possède un port sur cette mer
    public boolean portAppartenantA (Joueur joueur)
    {
        for (Case port : this.ports)
        {
            if (joueur.equals (port.proprietaire()))
                return true ;
        }
        return false ;
    }
    
}
