/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.donnees;

import java.util.ArrayList ;
import java.util.List ;

import fr.blitzkrieg.utiles.igraphique.mvc.Observable ;



// Représente une case du terrain
// NOTE Sur la "case masquante" : les modifications appliquées sur la case sont appliquées sur cette
//      "case masquante", ce qui permet de travailler dessus et d'avoir des lecture/écritures cohérentes.
//          => Expliquer dans la doc à quoi ça sert (planification, ...)
public class Case extends Observable implements Cloneable
{

    // Constantes
    public static enum TypeCase {CASE_VILLE, CASE_TERRE, CASE_MER} ;

    // Case masquante
    private Case caseMasquante ;            // Données masquant éventuellement celles de la case réelle
    
    // Données
    private int      x ;                    // Coordonnées de la case sur le terrain NOTE : ne pas utiliser pour effectuer des opérations sur le terrain depuis cette classe, mais parfois on manipule les objets cases (méthode qui sélectionne des cases sur un critère donné et on a besoin de savoir où elles sont pour chercher leurs voisines sur le terrain par exemple)
    private int      y ;
    private TypeCase type ;                 // Type de terrain
    private Joueur   proprietaire ;         // Joueur à qui appartiennent la case
    private boolean  troupePresente ;       // Indique si une troupe est présente sur la case
    private boolean  vivante ;              // Indique si la case est vivante ou non
    
    private List<Joueur> decouvertes ;      // Joueurs qui ont découvert cette case

    
    
    // Constructeur
    // Le propriétaire peut être null (pour les mers)
    //   TODO => Ce sont des choses qui devraient être vérifiées et corrigées automatiquement dans le
    //           constructeur
    // TODO Revoir les constructeurs dont on a besoin
    // NOTE Il faudra peut-être interdire de ne pas fournir de propriétaire en créant la case et/ou
    //      une méthode de modification du propriétaire... à voir (et pour l'éditeur de scénario on
    //      aura sans doute besoin de pouvoir modifier un peu n'importe quoi n'importe comment...
    //      bouhouhou)
    //          => Peut-être faire des modificateurs protégés et une classe fille qui les rend
    //             publics (CaseEditeur) ; ça éviterait quand même de pouvoir faire n'importe quoi
    //             avec les cases du jeu ; par contre les accesseurs protégés on peut y accéder
    //             dans tout le paquetage, donc dans le Terrain par exemple (mais pas dans les
    //             contreurs, c'est ça qui est important)
    // NOTE Actuellement deux cases sont égales si elles sont identiques uniquement, donc les versions par
    //      défut de equals et hashcode sont valides (utilisées dans le programme)
    public Case (int x, int y, TypeCase type, Joueur proprietaire, boolean troupePresente)
    {
        this.x              = x ;
        this.y              = y ;
        this.type           = type ;
        if (proprietaire != null)
            choisirProprietaireInitial (proprietaire) ;
        this.troupePresente = troupePresente ;
        this.vivante        = true ;
        this.decouvertes    = new ArrayList() ;
    }
    
    
    // Constructeur complet (pour le chargement d'une sauvegarde)
    public Case (int x, int y, TypeCase type, Joueur proprietaire, boolean troupePresente, boolean vivante,
                 List<Joueur> decouvertes)
    {
        this.x              = x ;
        this.y              = y ;
        this.type           = type ;
        this.proprietaire   = proprietaire ;
        this.troupePresente = troupePresente ;
        this.vivante        = vivante ;
        this.decouvertes    = decouvertes ;
    }

    
    // Détermine le propriétaire initial de la case (si on ne l'a pas indiqué dans le constructeur)
    // NOTE Voir si on garde ça : plus pratique à construire, mais moins fiable du point de vue de la
    //      cohérence des données
    public void choisirProprietaireInitial (Joueur joueur)
    {
        if (this.proprietaire != null)
            throw new IllegalStateException ("Le propriétaire initial de la case a déjà été choisi") ;
        if (this.type != TypeCase.CASE_TERRE && this.type != TypeCase.CASE_VILLE)
            throw new IllegalStateException ("Ce type de case ne peut pas avoir de propriétaire : " + this.type) ;
        this.proprietaire = joueur ;
    }

    
    // Clone la case courante.
    // TODO Ou est-ce que ça sert ça ? On ne le délègue par à l'autre case ?
    public Object clone ()
    {
        return new Case (this.x, this.y, this.type, this.proprietaire, this.troupePresente,
                         this.vivante, new ArrayList (this.decouvertes));
    }
    
    
    
    // Ajoute ou retire une "case masquante" à la case. Les informations de la case masquante masquent
    //   les informations de la vraie case. 
    // Pour retirer la case masquante, passer null en paramètre de cette méthode.
    public void modifCaseMasquante (Case caseMasquante)
    {
        this.caseMasquante = caseMasquante ;
    }
    
    
    

// TODO Accesseur juste pour la sauvegarde/chargement, voir ce qu'on en fait
//      (+ renommer)
public List<Joueur> joueursDecouverte ()
{
    if (this.caseMasquante != null)
        return this.caseMasquante.joueursDecouverte() ;
    else
        return this.decouvertes ;
}
    
    
    // Indique si le joueur en paramètre a découvert la case
    // TODO Renommer en estDecouvertePar ?
    public boolean decouverte (Joueur joueur)
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.decouverte (joueur) ;
        else
            return this.decouvertes.contains (joueur) ;
    }
    
    
    // Accesseurs
    public int x ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.x() ;
        else
            return this.x ;
    }
    public int y ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.y() ;
        else
            return this.y ;
    }
    public TypeCase type ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.type() ;
        else
            return this.type ;
    }
    public Joueur proprietaire ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.proprietaire() ;
        else
            return this.proprietaire ;
    }
    public boolean troupePresente ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.troupePresente() ;
        else
            return this.troupePresente ;
    }
    public boolean estVivante ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.estVivante() ;
        else
            return this.vivante ;
    }
    public boolean estMorte ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.estMorte() ;
        else
            return ! estVivante() ;
    }
    
    
    // Affiche la position de la case
    public String posEnChaine ()
    {
        if (this.caseMasquante != null)
            return this.caseMasquante.posEnChaine () ;
        else
            return "(" + this.x + ", " + this.y + ")" ;
    }
    
    
    
    // Ajoute une troupe sur la case
    public void deposerTroupe ()
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.deposerTroupe() ;
        }
        else
        {
            if (this.troupePresente)
                throw new IllegalStateException ("Il y a déjà une troupe sur la case") ;
            this.troupePresente = true ;
            avertirObservateurs() ;
        }
    }
    

    // Retire la troupe de la case
    public void retirerTroupe ()
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.retirerTroupe() ;
        }
        else
        {
            if (! this.troupePresente)
                throw new IllegalStateException ("Il n'y a pas de troupe sur la case") ;
            this.troupePresente = false ;
            avertirObservateurs() ;
        }
    }
    
    
    // Capture la case
    // Si la case peut contenir une troupe, une troupe est déposée
    // Le joueur en paramètre devient le nouveau propriétaire de la case
    // Le booléen indique si la case a été capturée par une troupe ou par encerclement (pour le
    //   suivi des troupes)
    // TODO Pour simplifier les vérifcations, y compris depuis l'extérieur, on pourrait avoir une méthode
    //      qui indique si la case peut avoir un propriétaire et une autre si elle peut avoir une troupe
    public void capturerCase (Joueur joueur, boolean captureParTroupe)
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.capturerCase (joueur, captureParTroupe) ;
        }
        else
        {
            if (this.proprietaire.equals (joueur))
                throw new IllegalArgumentException ("Ce joueur est déjà propriétaire de la case") ;
            this.proprietaire = joueur ;
            this.vivante      = true ;
            this.troupePresente = (this.type == TypeCase.CASE_TERRE && captureParTroupe) ;
            avertirObservateurs() ;
        }
    }

    
    // Indique que le joueur en paramètre a découvert la case
    public void decouvrir (Joueur joueur)
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.decouvrir (joueur) ;
        }
        else
        {
            if (! this.decouvertes.contains (joueur))
                this.decouvertes.add (joueur) ;
            avertirObservateurs() ;
        }
    }
    
    
    // Fait mourir la case (la case est morte)
    public void mourir ()
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.mourir() ;
        }
        else
        {
            this.vivante = false ;
            avertirObservateurs() ;
        }
    }
    
    // Fait naître la case (la case est vivante)
    public void naitre ()
    {
        if (this.caseMasquante != null)
        {
            this.caseMasquante.naitre() ;
        }
        else
        {
            this.vivante = true ;
            avertirObservateurs() ;
        }
    }
    
}
