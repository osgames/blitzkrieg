/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg;

import java.io.File ;
import java.io.IOException ;
import java.util.Random ;

import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.igraphique.FenPrincipale ;
import fr.blitzkrieg.igraphique.animations.Animations ;
import fr.blitzkrieg.stockage.StockagePartie ;
import fr.blitzkrieg.traitements.GestDeroulementPartie ;
import fr.blitzkrieg.traitements.Regles ;



// Classe représentant l'application
// TODO Vérifier que tout se passe bien quand on a un grand plateau et qu'on charge une partie avec un
//      plus petit (notamment quand les barres de défilement était vers en bas à droite)
public class Blitzkrieg
{
    
    // Instance unique de l'application
    private static final Blitzkrieg instance = new Blitzkrieg() ;
    
    // Données
    private FenPrincipale         fenPrincipale ;           // Fenêtre principale de l'application NOTE La fenêtre principale sert de point d'accès aux objets graphiques comme les boîtes de dialogue, ... (en plus des panneaux qui font effectivement partie de la fenêtre courante)
    private Animations            animations ;              // Classe de gestion des animations de l'appli
    private GestDeroulementPartie gestPartieCourante ;      // Gestionnaire de la partie en cours
//    private Partie        partieCourante ;                  // Partie en cours
    private Random                genAlea = new Random() ;  // Générateur de nombres aléatoires
    
    
    
    // Renvoie l'instance de l'application
    public static Blitzkrieg instance ()
    {
        return instance ;
    }
    
    
    // Affiche l'interface graphique de l'application
    public void afficherInterface ()
    {
        this.fenPrincipale = new FenPrincipale() ;
    }
    
//    // Créee une nouvelle partie aléatoire
//    // Renvoie la partie créée
//    public Partie creerPartie ()
//    {
//        // TODO Commencer la partie plus tard ?
////        this.partieCourante = new Regles().creerNouvellePartie() ;
//        this.partieCourante = new GenerateurCarteAleatoire().genererCarte (new Regles(), Arrays.asList ("Joueur 1", "Joueur2")) ;
//        this.fenPrincipale.initialiserObjetsGraphiquesPartie (this.partieCourante) ;
//        this.partieCourante.commencer() ;
//        return this.partieCourante ;
//    }
    // Prépare l'application pour une partie chargée
    // Renvoie la partie passée en paramètre
    // TODO Si on garde toutes les méthodes, les appuyer les unes sur les autres (et renommer, notamment celle-ci)
//    public Partie creerPartie (Partie partie)
    public Partie creerPartie (File ficChoisi) throws IOException
    {
        Partie partieCourante = new StockagePartie().charger (ficChoisi.getAbsolutePath()) ;
        this.gestPartieCourante = new GestDeroulementPartie (partieCourante) ;
        this.fenPrincipale.initialiserObjetsGraphiquesPartie (partieCourante) ;
//        this.partieCourante.commencer() ;     // Non : ne pas modifier l'état de la partie
        return partieCourante ;
    }
    // Prépare l'application pour une nouvelle partie
    // Renvoie la partie passée en paramètre
//    public Partie creerPartieScenario (Partie partie)
    // TODO Vérifier que le scénario ne peut pas faire de bêtises ; et si le nom du scénario est
    //      "coucou/truc", qu'est-ce que ça fait ? Est-ce qu'on peut utiliser un "/" dans un nom de
    //      fichier en l'échappant ? Je ne crois pas, mais à vérifier ; réfléchir aux mesures à prendre
    //      pour vérifier que le fichier ne pose pas problème (par exemple qu'il se trouve bien dans le
    //      répertoire des scénarios)
    public Partie creerPartieScenario (String nomScenario) throws IOException
    {
        // TODO Commencer la partie plus tard ?
//        this.partieCourante = partie ;
        Partie partieCourante = new StockagePartie().charger ("donnees/scenarios/" + nomScenario) ;
        this.gestPartieCourante = new GestDeroulementPartie (partieCourante) ;
        this.fenPrincipale.initialiserObjetsGraphiquesPartie (partieCourante) ;
        this.gestPartieCourante.commencerPartie() ;
        return partieCourante ;
    }
    
    // Indique à l'application qu'il n'y a plus de partie en cours
    // S'il n'y a pas de partie en cours, rien ne se passe
    public void terminerPartie ()
    {
        if (this.gestPartieCourante != null)
        {
            this.gestPartieCourante.terminerPartie() ;
            this.gestPartieCourante = null ;
            this.fenPrincipale.finaliserObjetsGraphiquesPartie() ;
        }
    }
    
    // Renvoie la fenêtre principale de l'applicatio
    public FenPrincipale fenPrincipale ()
    {
        return this.fenPrincipale ;
    }
    
    // Renvoie le gestionaire des animations
    public Animations animations ()
    {
        if (this.animations == null)
            this.animations = new Animations() ;
        return this.animations ;
    }
    
    // Renvoie le gestionnaire de la partie courante
    public GestDeroulementPartie gestPartieCourante ()
    {
        return this.gestPartieCourante ;
    }
    
    // Renvoie la partie en cours (null si aucune partie en cours)
    public Partie partieCourante ()
    {
        return (this.gestPartieCourante == null ? null : this.gestPartieCourante.partie()) ;
    }
    
    // Renvoie les règles de la partie en cours (null si aucune partie en cours)
    public Regles reglesCourantes ()
    {
        return (this.gestPartieCourante == null ? null : this.gestPartieCourante.partie().regles()) ;
    }
    
    // Renvoie le générateur de nombres aléatoires de l'application
    public Random genAlea ()
    {
        return this.genAlea ;
    }
    
    
    // Méthode principale
    // TODO Capturer les exceptions et afficher un message d'erreur (au cas où on aurait un problème pour
    //      afficher l'interface principale)
    public static void main (String[] args) throws Exception
    {
        Blitzkrieg.instance().afficherInterface() ;
        // TODO Ne pas créer de partie automatiquement
        //      (mais afficher éventuellement quelque chose pour remplir la fenêtre et informer l'utilisateur)
//        Blitzkrieg.instance().creerPartie() ;
//        Blitzkrieg.instance().creerPartieScenario ("scenario-aleatoire.scn") ;
//        Blitzkrieg.instance().creerPartieScenario ("scenario-aleatoire-petit.scn") ;
        Blitzkrieg.instance().creerPartieScenario ("front-russe.scn") ;
    }

}
