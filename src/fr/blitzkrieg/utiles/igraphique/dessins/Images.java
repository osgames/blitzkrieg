/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.igraphique.dessins;

import java.awt.Color ;
import java.awt.Image ;
import java.awt.image.BufferedImage ;
import java.io.File ;
import java.io.IOException ;

import javax.imageio.ImageIO ;
import javax.swing.JPanel ;



// Fonctions utiles pour la manipulation des images
public class Images
{

    // Charge une image depuis un fichier
    public static BufferedImage charger (String chemFic) throws IOException
    {
        // TODO Trouver quelque chose qui marche aussi (de manière transparente) quand l'image se trouve
        //      dans un jar
        return ImageIO.read (new File (chemFic)) ;
    }
    
    
    // Renvoie une image de type BufferedImage à partir de l'image en paramètre.
    // Si l'image est déjà de cette classe, elle est renvoyée.
    // Si l'image en paramètre vaut null, null est renvoyé.
    // Si l'image est d'une autre classe d'image, une nouvelle image est créée et l'image en paramètre
    //   est dessinée dessus.
    // TODO A tester, en particulier la conversion (pas forcément exécutée)
    // NOTE Attention : quand on a récupéré une image d'un autre type que BufferedImage c'est peut-être
    //      qu'on est passé par des méthodes de chargement d'AWT plutôt qu'un constructeur direct de
    //      BufferedImage ou par les fonctions de ImageIO. Et alors l'image n'est pas forcément complètement
    //      chargée, il peut y avoir des erreurs quand on demande sa taille. Prendre ça en compte ici et
    //      patienter jusqu'au chargement de l'image ?
    public static BufferedImage imageTampon (Image image)
    {
        // Cas particuliers
        if (image == null || image instanceof BufferedImage)
            return (BufferedImage) image ;
        
        // Créer une nouvelle image et la remplir
        JPanel p_tmp = new JPanel() ;       // TODO Il doit y avoir moyen d'utiliser quelque chose de mieux
        BufferedImage nouvImage = new BufferedImage (image.getWidth(p_tmp), image.getHeight(p_tmp), BufferedImage.TYPE_INT_ARGB) ;
        nouvImage.getGraphics().drawImage (image, 0, 0, p_tmp) ;
        
        // Renvoyer la nouvelle image
        return nouvImage ;
    }
    
    
    // Copie une image dans une autre
    // L'image en sortie est de type BufferedImage.TYPE_INT_ARGB
    //   NOTE C'est bizarre, mais en créant une image du même type que l'image source et en copiant les
    //        données rvba de l'une à l'autre ça ne marche pas toujours (exemple d'une image PNG chargée
    //        depuis un fichier, qui utilise une palette de 256 couleurs et qui est de type TYPE_BYTE_INDEXED)
    public static BufferedImage copie (BufferedImage imgSource)
    {
        // Créer la nouvelle image
        BufferedImage imgDest = new BufferedImage (imgSource.getWidth(), imgSource.getHeight(), BufferedImage.TYPE_INT_ARGB) ;
        
        // Copier les données
        // TODO On devrait pouvoir faire mieux. Mais attention à ce que les données soient bien copiées et
        //      que la transparence soit prise en compte !
        int largeur = imgSource.getWidth() ;
        int hauteur = imgSource.getHeight() ;
        for (int x = 0 ; x < largeur ; x++)
        for (int y = 0 ; y < hauteur ; y++)
            imgDest.setRGB (x, y, imgSource.getRGB (x, y)) ;
        
        // Renvoyer l'image
        return imgDest ;
    }
    
    
    // Inverse les couleurs de l'image
    // x La valeur alpha (transparence) n'est pas modifiée
    //      => Euh ben en fait elle est supprimée
    // TODO Pourquoi la transparence est perdue ? Je suis obligé de mettre 255 (pas de transparence),
    //      sinon l'image est toujours transparente
    // Attention : ne renvoie pas une copie mais l'image d'origine
    public static BufferedImage couleursInversees (BufferedImage image)
    {
        int largeur = image.getWidth() ;
        int hauteur = image.getHeight() ;
        for (int x = 0 ; x < largeur ; x++)
        for (int y = 0 ; y < hauteur ; y++)
        {
            int rvba  = image.getRGB (x, y) ;
            Color coul = new Color (rvba, true) ;
            Color coulInverse = new Color (255 - coul.getRed(), 255 - coul.getGreen(), 255 - coul.getBlue(), 255) ;
            image.setRGB (x, y, coulInverse.getRGB()) ;
        }
        
        // Renvoyer l'image
        return image ;
    }
    
    
    // Assombrit l'image de la valeur indiquée en paramètre (80% => 20% de la valeur de rouge/vert/bleu
    //   est conservée)
    // Problème avec alpha : même problème qu'avec l'inversion des couleurs
    // Attention : ne renvoie pas une copie mais l'image d'origine
    public static BufferedImage assombrie (BufferedImage image, double pourcentAssombrissement)
    {
        int largeur = image.getWidth() ;
        int hauteur = image.getHeight() ;
        double pourcentConserve = 1 - pourcentAssombrissement ;
        for (int x = 0 ; x < largeur ; x++)
        for (int y = 0 ; y < hauteur ; y++)
        {
            int rvba  = image.getRGB (x, y) ;
            Color coul = new Color (rvba, true) ;
            Color coulInverse = new Color ((int) (pourcentConserve * coul.getRed()),
                                           (int) (pourcentConserve * coul.getGreen()),
                                           (int) (pourcentConserve * coul.getBlue()),
                                           255) ;
            image.setRGB (x, y, coulInverse.getRGB()) ;
        }
        
        // Renvoyer l'image
        return image ;
    }
 
}
