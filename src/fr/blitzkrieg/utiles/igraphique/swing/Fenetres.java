/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.igraphique.swing;

import java.awt.Dialog ;
import java.awt.Dimension ;
import java.awt.FlowLayout ;
import java.awt.Frame ;
import java.awt.Toolkit ;
import java.awt.event.ActionEvent ;
import java.awt.event.ActionListener ;
import java.awt.event.KeyEvent ;

import javax.swing.JButton ;
import javax.swing.JComponent ;
import javax.swing.JDialog ;
import javax.swing.JPanel ;
import javax.swing.KeyStroke ;



// Méthodes utiles pour la gestion des fenêtres
public class Fenetres
{

    // Centre la fenêtre sur l'écran
    public static void centrer (Frame fenetre)
    {
        Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize() ;
        int xFen = (int) (tailleEcran.getWidth()  - fenetre.getWidth())  / 2 ;
        int yFen = (int) (tailleEcran.getHeight() - fenetre.getHeight()) / 2 ;
        fenetre.setLocation (xFen, yFen) ;
    }
    
    
    // Centre la boîte de dialogue sur une fenêtre dont on donne les coordonnées
    //   (xDeb, yDeb) et (xFin, yFin)
    public static void centrer (Dialog dialogue, Frame fenetre)
    {
        // CODE Vérifier les paramètres (notamment le fait que les coordonnées de fin sont supérieures ou égales aux coordonnées de début
        
        int xDial = (int) (fenetre.getX() + ((double) (fenetre.getWidth()  - dialogue.getWidth())  / 2)) ;
        int yDial = (int) (fenetre.getY() + ((double) (fenetre.getHeight() - dialogue.getHeight()) / 2)) ;
        dialogue.setLocation (xDial, yDial) ;
    }
    
    
    // Crée un panneau contenant un bouton Ok permettant de fermer la boîte de dialogue en paramètre
    public static JPanel panneauOk (final Dialog dialogue)
    {
        // Créer le bouton et son écouteur
        JButton bt_ok = new JButton ("Ok") ;
        bt_ok.addActionListener (new ActionListener() { public void actionPerformed (ActionEvent evt) { dialogue.setVisible (false) ; } }) ;
        
        // Créer le panneau
        JPanel p_ok = new JPanel (new FlowLayout (FlowLayout.CENTER)) ;
        p_ok.add (bt_ok) ;
        
        // Renvoyer le panneau
        return p_ok ;
    }
    
    
    // Ajouter un écouteur qui masque la boîte de dialogue en paramètre quand on presse la touche Echap
    public static void ajouterEcouteurEchapPourMasquer (final JDialog dialogue)
    {
        dialogue.getRootPane().registerKeyboardAction
                (new ActionListener() { public void actionPerformed (ActionEvent evt) { dialogue.setVisible(false) ; } },
                 KeyStroke.getKeyStroke (KeyEvent.VK_ESCAPE, 0), 
                 JComponent.WHEN_IN_FOCUSED_WINDOW) ; 
    }

    // Ajouter un écouteur qui masque la boîte de dialogue en paramètre quand on presse la touche Entrée
    public static void ajouterEcouteurEntreePourMasquer (final JDialog dialogue)
    {
        dialogue.getRootPane().registerKeyboardAction
                (new ActionListener() { public void actionPerformed (ActionEvent evt) { dialogue.setVisible(false) ; } },
                 KeyStroke.getKeyStroke (KeyEvent.VK_ENTER, 0), 
                 JComponent.WHEN_IN_FOCUSED_WINDOW) ; 
    }
    
}
