package fr.blitzkrieg.utiles.exttypes;



// Fonctions utiles sur les nombres flottants
public class Flottants
{

    // Arrondit un nombre flottant à la nième décimale
    // Les valeurs du système décimal ne peuvent pas être stockées de manière exacte en binaire en virgule
    //  flottante (0,1 n'a pas de valeur exacte en binaire). La valeur binaire la plus proche est utilisée.
    //  Les opérations sur les nombres en virgule flottante (additions, ...) introduisent donc des dérives et
    //  l'addition 0,1 + 0,1 + 0,1 peut donner des choses comme 0,300000000001 par exemple
    // Lorsque des opérations en virgules flottantes sont effectuées, il est parfois utile de les corriger
    //  (pour les afficher par exemple)
    // TODO L'opération Math.pow étant très lente, on pourrait faire une optimisation pour les cas courants :
    //      10, 100, 1000, ...
    public static double arrondir (double nombre, int nbDecimales)
    {
        long multiplicateur = (long) Math.pow (10, nbDecimales) ;
        return (double) Math.round (nombre * multiplicateur) / multiplicateur ;
    }
    
    
    // Indique si un nombre flottant est nul (<=> assez proche de zéro)
    // TODO Une version en précisant la différence acceptée et une fonction de comparaison plus compléte
    public static boolean estNul (double nombre)
    {
        return Math.abs(nombre) < 0.00000001 ;
    }
    
    
    // Indique si deux nombre flottants sont égaux (<=> assez proches l'un de l'autre)
    // TODO Une version en précisant la différence acceptée et une fonction de comparaison plus compléte
    public static boolean egaux (double nb1, double nb2)
    {
        return estNul (nb1 - nb2) ;
    }
    
    
    // Affiche une valeur réelle (2 chiffres après la virgule, séparateur des milliers et des décimales
    //  corrects)
    // NOTE A laisser dans les flottants ou mettre dans une classe de gestion des affichage ? [mieux sans doute]
    // NOTE Faire une fonction plus générique, regarder les fonctions spécifiques qui sont utiles (une comme ça
    //      pour les valeurs monétaires par exemple) ; regarder les fonctions de Java pour la localisation
    // (séparateur de milliers : <aucun> ; séparateur décimal : virgule)
    public static String formater (double valeur)
    {
        String chValeur = "" + arrondir (valeur, 2) ;
        chValeur = chValeur.replaceAll ("\\.", ",") ;
        return chValeur ;
    }
    // (comme formater, mais si le nombre se termine par ",0" ces deux caractères sont supprimés)
    public static String formaterSimplifier (double valeur)
    {
        String chValeur = formater (valeur) ;
        return (chValeur.endsWith (",0") ? chValeur.substring (0, chValeur.length() - 2) : chValeur) ;
    }
    // (séparateur de milliers : <espace> ; séparateur décimal : <aucun> [valeurs arrondies])
    public static String formater2 (double valeur)
    {
        String chValeur ;                   // Valeur transformée en chaîne
        String chFormatee ;                 // Valeur formatée
        
        // Récupérer la valeur
        chValeur = "" + Math.round (valeur) ;
        chFormatee = "" ;
        
        // Ajouter les espaces
        int cpt = 0 ;
        for (int i = chValeur.length()-1 ; i >= 0 ; i--)
        {
            chFormatee = chValeur.charAt (i) + chFormatee ;
            if (((++cpt % 3) == 0) && (i != 0))
                chFormatee = " " + chFormatee ;
        }
        
        // Renvoyer le résultat
        return chFormatee ;
    }
    
}
