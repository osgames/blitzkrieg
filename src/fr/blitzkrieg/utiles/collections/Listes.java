/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.utiles.collections;

import java.util.ArrayList ;
import java.util.List ;



// Fonctions utiles pour la manipulation des listes
public class Listes
{

    // Crée une liste contenant des entiers de 1 à nbElts, rangés par ordre croissant
    public static List<Integer> creerSequenceNombres (int nbElts)
    {
        List<Integer> sequence = new ArrayList() ;
        for (int i = 1 ; i <= nbElts ; i++)
            sequence.add (i) ;
        return sequence ;
    }
    
}
