/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia.chemins;

import static fr.blitzkrieg.donnees.Terrain.nAppartenantPasA ;

import java.util.Arrays ;
import java.util.HashMap ;
import java.util.LinkedList ;
import java.util.List ;
import java.util.Map ;
import java.util.TreeMap ;
import java.util.Map.Entry ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;



// Calculs de plus court chemins entre deux cases.
// NOTE La classe (ou les méthodes) doit être paramétrée avec des modules de calculs de coût d'accès à une case.
// Attention, le sens de parcours du chemin est important parce que certaines trajets sont à sens unique
//   (débarquements).
// TODO Bien tester tous les algos. Ce serait mieux que ça ne gèle pas le programme parce qu'une condition
//      de fin est mal définie.
public class CalculsChemins
{

    // Calcule la distance entre deux cases.
    // Renvoie le chemin à suivre et l'effort calculé dans un tableau : d'abord le chemin (une liste de cases)
    //   puis la "distance" (effort) (un double).
    // Si l'effort maximal est dépassé et qu'on n'a pas trouvé de chemin, renvoie un chemin null et
    //   Double.POSITIVE_INFINITY comme effort
    public static Object[] chercherCourtChemin (Partie partie, Joueur joueur, CalculEffortPriseCase calculEffort,
                                                Case caseDepart, Case caseDestination, double effortMaximal)
    {
        return chercherChemin (partie, joueur, calculEffort,
                               Arrays.asList (new Case[]{caseDepart}), caseDestination, effortMaximal) ;
    }
    // (pareil mais ne renvoie que la distance ; Double.POSITIVE_INFINITY si pas de chemin trouvé)
    public static double distanceChemin (Partie partie, Joueur joueur, CalculEffortPriseCase calculEffort,
                                         Case caseDepart, Case caseDestination, double effortMaximal)
    {
        return (Double) chercherCourtChemin (partie, joueur, calculEffort, caseDepart, caseDestination, effortMaximal)[1] ;
    }

    // Calcule la distance entre un ensemble de cases (au chois) et une case destination.
    // CODE Eviter de répéter tous ces commentaires. Les indiquer une fois pour toute dans les commentaires
    //      de la classe pour indiquer comment fonctionnent les méthodes.
    // Renvoie le chemin à suivre et l'effort calculé dans un tableau : d'abord le chemin (une liste de cases)
    //   puis la "distance" (effort) (un double).
    // Si l'effort maximal est dépassé et qu'on n'a pas trouvé de chemin, renvoie un chemin null et
    //   Double.POSITIVE_INFINITY comme effort
    public static Object[] chercherCourtChemin (Partie partie, Joueur joueur, CalculEffortPriseCase calculEffort,
                                                List<Case> casesDepart, Case caseDestination, double effortMaximal)
    {
        return chercherChemin (partie, joueur, calculEffort, casesDepart, caseDestination, effortMaximal) ;
    }
    // (pareil mais ne renvoie que la distance ; Double.POSITIVE_INFINITY si pas de chemin trouvé)
    public static double distanceChemin (Partie partie, Joueur joueur, CalculEffortPriseCase calculEffort,
                                         List<Case> casesDepart, Case caseDestination, double effortMaximal)
    {
        return (Double) chercherCourtChemin (partie, joueur, calculEffort, casesDepart, caseDestination, effortMaximal)[1] ;
    }

    
    // Calcule la distance entre un ensemble de cases de départ et une case cible.
    // Renvoie le chemin à suivre et l'effort calculé dans un tableau : d'abord le chemin (une liste de cases)
    //   puis la "distance" (effort) (un double).
    // Si l'effort maximal est dépassé et qu'on n'a pas trouvé de chemin, renvoie un chemin null et
    //   Double.POSITIVE_INFINITY comme effort
    // TODO C'est ennuyeux ces nombres flottants : si on teste l'égalité par faible différence on risque de
    //      se retrouver avec des valeurs qui dépassent le seuil, sinon on risque de manquer des valeurs
    //      à cause d'arrondis pourris (trop hauts). Décider quelle solution privilégier et en tenir
    //      compte partout (ne serait-ce que le noter et vérifier que ça ne gêne pas).
    // NOTE On ne veut se déplacer qu'en territoire ennemi (pas la peine de considérer le territoire
    //      qui nous appartient, on est censé partir d'une case à côté de l'ennemi, sinon on va se retrouver
    //      avec toutes les cases qui nous appartiennent avec un effort de 0, pour pas grand chose (quoique...
    //      on ne devrait pas explorer ces cases puisqu'on n'a pas de plus court chemin à proposer pour y
    //      aller ; ben non, si elles ne sont pas marquées comme cases de départ). Reste à voir ce qu'on veut
    //      faire de ça : en général c'est pour trouver un chemin à travers un territoire ennemi, donc on peut
    //      se contenter de considérer les cases ennemies, juste pour simplifier l'ensemble de cases à
    //      consdérer.
    private static Object[] chercherChemin (Partie partie, Joueur joueur, CalculEffortPriseCase calculEffort,
                                            List<Case> casesDepart, Case caseDestination, double effortMaximal)
    {
        Terrain                    terrain ;            // Terrain sur lequel se déroule la partie
        TreeMap<String,CaseChemin> file ;               // File à priorité (algo classique de recherche de plus court chemin) [PriorityQueue ne convient pas à cause de la réorganisation régulière des priorités]
        Map<Case,CaseChemin>       casesExplorees ;     // Cases auxquelles on a déjà attribué une valeur (correspondance Case -> CaseChemin, pour les retrouver)
        
//System.out.println ("Chercher un chemin entre une liste de " + casesDepart.size() + " cases et la case " + caseDestination.x() + "," + caseDestination.y()) ;
        // Initialisations
        terrain        = partie.terrain() ;
        file           = new TreeMap() ;
        casesExplorees = new HashMap() ;
        

        // Placer les cases de départ dans la file
        for (Case c : casesDepart)
        {
            CaseChemin caseChemin = new CaseChemin (c, 0) ;
            file.put           (caseChemin.genCle(), caseChemin) ;
            casesExplorees.put (c,                   caseChemin) ;
        }
        
        // Tant que le travail n'est pas terminé
        while (true)
        {
            // Lire la prochaine case
            Entry<String,CaseChemin> infosCaseConsideree = file.pollFirstEntry() ;
            CaseChemin caseConsideree = (infosCaseConsideree == null ? null : infosCaseConsideree.getValue()) ;
            
            // Conditions de fin de l'algorithme
            if (caseConsideree == null)                             // Case destination non atteignable ou effort trop élevé
                return new Object[] {null, Double.POSITIVE_INFINITY} ;
            if (caseConsideree.effort > effortMaximal)              // Effort trop élevé
                return new Object[] {null, Double.POSITIVE_INFINITY} ;
            if (caseConsideree.caseCourante == caseDestination)     // Case destination atteinte
                return new Object[] {retrouverChemin (caseConsideree), caseConsideree.effort} ;
            
            // Lister les cases voisines
            List<Case> voisines = nAppartenantPasA (joueur, terrain.casesAccessiblesDepuis (caseConsideree.caseCourante, partie.invasionsNavalesPermises())) ;
            for (Case voisine : voisines)
            {
                // Evaluer l'effort à faire pour y aller
                double effortVersVoisine = calculEffort.effortCaptureCase (partie, joueur, voisine) ;
                double effortTotal       = caseConsideree.effort + effortVersVoisine ;

                // Si l'effort total ne dépasse pas l'effort maximal
                if (effortTotal <= effortMaximal)
                {
                    // Si la case est inconnue, la créer
                    CaseChemin caseVoisine = casesExplorees.get (voisine) ;
                    if (caseVoisine == null)
                    {
                        caseVoisine = new CaseChemin (voisine, effortTotal) ;
                        caseVoisine.caseChemSource = caseConsideree ;
                        file.put           (caseVoisine.genCle(), caseVoisine) ;
                        casesExplorees.put (voisine,              caseVoisine) ;
                    }
                    // Sinon mettre à jour les informations sur le plus court chemin pour y arriver
                    else
                    {
                        if (effortTotal < caseVoisine.effort)
                        {
                            file.remove (caseVoisine.genCle()) ;
                            caseVoisine.effort         = effortTotal ;
                            caseVoisine.caseChemSource = caseConsideree ;
                            file.put    (caseVoisine.genCle(), caseVoisine) ;
                        }
                    }
                }
            }

        }
        
    }
    
    
    // Renvoie le chemin suivi en le remontant à partir de la case destination (emballée).
    // Renvoie la liste des cases constituant le chemin, dans l'ordre depuis la case suivant la case
    //  d'origine jusqu'à la case destination.
    private static List<Case> retrouverChemin (CaseChemin caseDest)
    {
        List<Case> chemin = new LinkedList() ;
        
        // Remonter le chemin
        CaseChemin c = caseDest ;
        while (c.caseChemSource != null)
        {
            chemin.add (0, c.caseCourante) ;
            c = c.caseChemSource ;
        }
        
        // Renvoyer le chemin
        return chemin ;
    }

}



// Représente une case sur les chemins en cours de calcul.
class CaseChemin
{
    
    public Case       caseCourante ;        // Case courante
    public double     effort ;              // Effort estimé pour arriver à cette case (Double.POSITIVE_INFINITY si on n'est pas arrivé jusqu'à cette case)
    public CaseChemin caseChemSource ;      // Case par laquelle on est arrivée à celle-ci (null si aucune)
    

    
    // Constructeurs
    public CaseChemin (Case caseCourante)
    {
        this (caseCourante, Double.POSITIVE_INFINITY) ;
    }
    public CaseChemin (Case caseCourante, double effort)
    {
        this (caseCourante, effort, null) ;
    }
    public CaseChemin (Case caseCourante, double effort, CaseChemin caseChemSource)
    {
        this.caseCourante = caseCourante ;
        this.effort       = effort ;
        this.caseChemSource   = caseChemSource ;
    }
    
    

    // Renvoie la clé de cette case (clé unique pour les tris/hachages)
    public String genCle ()
    {
        return "" + this.effort + this.caseCourante.x() + "-" + this.caseCourante.y() ;
    }
    
}
