/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia.chemins;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.traitements.Regles ;



// Effort pour prendre une case en fonction de la défense de la case.
// Cette méthode prend en compte la défense de la case, sans chercher à déterminer si en arrivant à cette
//   case on aura pris le contrôle des cases voisine. La méthode considère une force d'attaque standard,
//   qui ne dépend pas de la situation.
// Cette méthode peut être utilisée pour faire une estimation sans avoir besoin de faire évoluer le terrain
//   jusqu'à la situation considérée : pas la peine de calculer le scénario qui mène à cet assaut, on se base
//   sur une estimation de défense et d'attaque.
// TODO Faire une méthode de calcul d'effort basée sur la défense et l'attaque réelle, mais qui ne pourra
//      servir que si on a un scénario permettant d'arriver à la situation (et donc avoir une estimation
//      des forces réelles à ce moment).
public class CalculEffortApproximatif implements CalculEffortPriseCase
{

    // Estime l'effort à faire pour prendre la case considérée.
    // Renvoie Double.POSITIVE_INFINITY si la capture n'est pas possible.
    public double effortCaptureCase (Partie partie, Joueur joueur, Case caseConsideree)
    {
        Regles regles = partie.regles() ;

        // Estimer l'attaque et la défense
        double valAttaque = Regles.VALEUR_ATTAQUE_TROUPE + Regles.BONUS_ATTAQUE_TROUPE_ADJACENTE ;
        Double valDefense = regles.forceDefense (caseConsideree.x(), caseConsideree.y()) ;

        // Calculer la probabilité de réussite de l'attaque et l'effort à faire
        // TODO Préciser un peu ça : il faudrait tenir compte du bonus d'assauts successifs
        // TODO On ne tient pas compte d'un éventuel bonus d'attaque en cours, puisqu'on n'évalue pas
        //      spécialement la situation actuelle. Ca veut dire que si on fait un plan d'attaque d'une case
        //      proche on ne va pas privilégier la case déjà en cours d'assaut.
        Double probaReussiteAttaque = partie.regles().probaReussiteAttaque (valAttaque, valDefense) ;
        return (probaReussiteAttaque == null || probaReussiteAttaque == 0 ?
                  Double.POSITIVE_INFINITY :
                  1.0 / probaReussiteAttaque) ;
    }
    
}
