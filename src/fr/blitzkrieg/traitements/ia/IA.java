/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;



// Interface spécifiant les méthodes des classes d'IA
public interface IA
{

    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour () ;
    
    // Méthode appelée pour décider ce que doit faire un joueur informatique
    // La méthode renvoie l'action à mener (null si l'IA ne propose aucune action à mener)
    // CODE Reporter ces commentaires sur les classes filles une fois stabilisés ? Ou juste une référence à
    //      cette méthode plutôt ?
    public ActionIA choisirAction () ;
    
}
