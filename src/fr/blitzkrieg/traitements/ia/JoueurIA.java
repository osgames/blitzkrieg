/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.igraphique.actions.ActionClicTerrain ;
import fr.blitzkrieg.igraphique.actions.ActionFinTourJoueur ;



// Fil d'exécution d'un joueur IA
// DOC Schémas de classes et d'interactions pour le déroulement de la partie avec des joueurs IA et humains
//     et l'intégration de l'IA là-dedans
public class JoueurIA implements Runnable
{

    private static final int INTERVALLE_OBSERVATION =  500 ;        // Durée entre deux observations de la partie par le joueur (en ms)
    private static final int INTERVALLE_ACTION      = 1000 ;        // Durée entre deux actions du joueur (en ms)

    private Joueur joueur ;             // Joueur représenté par ce fil d'exécution
    private Partie partie ;             // Partie à laquelle joue le joueur
    private IA     ia ;                 // IA associée au joueur (pour le calcul du jeu)

    
    
    // Constructeur
    public JoueurIA (Joueur joueur, Partie partie, IA ia)
    {
        this.joueur = joueur ;
        this.partie = partie ;
        this.ia     = ia ;
    }
    
    
    
    // Méthode principale du joueur IA
    public void lancer ()
    {
//        long dateDerniereAction ;       // Date à laquelle le joueur a observé la partie ou agi pour la dernière fois
//                                              => NON : on veut attendre un ceratin temps après la fin de l'action
        
        
        // Initialisations
//        dateDerniereAction = System.currentTimeMillis() ;
System.out.println (this.joueur.nom() + " : Bonjour [" + this.ia.getClass().getSimpleName() + "]") ;

        
        // Jouer autant qu'on peut
        while (true)
        {
            // Attendre une demi-seconde
//            long dateProchaineAction = dateDerniereAction + INTERVALLE_OBSERVATION ;
//            long dureeAttente = dateProchaineAction - dateDerniereAction ;
//            if (dureeAttente >= 0)
//            {
            try
            {
                Thread.sleep (INTERVALLE_OBSERVATION) ;
            }
            catch (InterruptedException e)
            {
                System.out.println ("Le fil d'exécution du joueur " + this.joueur.nom() + " a été interrompu.") ;
                return ;
            }
//            }
    
            // Si la partie est terminée, arrêter de jouer
            if (this.partie.terminee())
                break ;
    
            // Si c'est à ce joueur de jouer et que la partie n'est pas suspendue
            if (this.joueur.estActif() && this.joueur.equals (this.partie.joueurCourant()) &&
                this.partie.enCours() && ! this.partie.observationSuspendue())
            {
                ActionIA actionIA ;
                
System.out.println (this.joueur.nom() + " : Je joue...") ;
                do
                {
                    // NOTE Tout ce qui suit se trouve dans la classe d'IA en principe
                    //          => Le mieux serait de découper les actions de l'IA en plusieurs parties :
                    //             réflexion et action ; d'ailleurs c'est plutôt le joueur qui devrait effectuer
                    //             les actions, les classes d'IA se contenant de décider de ce qu'il faut faire
                    //
                    // Etudier la situation et prendre une décision
                    actionIA = this.ia.choisirAction() ;
System.out.println (this.joueur.nom() + " : " + actionIA) ;
                    // Exécuter l'action
                    if (actionIA != null)
                    {
                        boolean actionExecutee ;
                        if      (ActionIA.RETIRER.equals  (actionIA.code())) actionExecutee = new ActionClicTerrain().executerActionRetirer  (this.joueur, actionIA.caseConcernee()) ;
                        else if (ActionIA.DEPOSER.equals  (actionIA.code())) actionExecutee = new ActionClicTerrain().executerActionDeposer  (this.joueur, actionIA.caseConcernee()) ;
                        else if (ActionIA.ATTAQUER.equals (actionIA.code())) actionExecutee = new ActionClicTerrain().executerActionAttaquer (this.joueur, actionIA.caseConcernee()) ;
                        else throw new IllegalStateException ("Code d'action non reconnu : " + actionIA.code()) ;
                        // (si l'action n'a pas pu être exécutée, on a un problème ; éviter quand même de geler
                        //  le jeu en demandant une nouvelle action qui pourrait être la même et à nouveau
                        //  impossible ; signaler l'erreur au joueur ?
                        //      TODO => Par le système de commentaires de l'IA, c'est le plus simple (et
                        //              il sert au moins à ça ; quoique... en fait ce sera le système
                        //              d'affichage des messages, il ne servira pas forcément qu'aux IA)
                        if (! actionExecutee)
                        {
                            System.err.println (this.joueur.nom() + " : Erreur - impossible d'exécuter l'action " + actionIA.code() + " en " + actionIA.caseConcernee().x() + ", " + actionIA.caseConcernee().y()) ;
                            actionIA = null ;
                        }
                    }

                    // Attendre un peu qu'on puisse suivre les actions
                    try
                    {
// ENCOURS A réactiver (mais pas pour le tour d'initialisation)
Thread.sleep (0) ;
//                        if (! this.partie.tourInitialisation ())
//                            Thread.sleep (INTERVALLE_ACTION) ;
                    }
                    catch (InterruptedException e)
                    {
                        System.out.println ("Le fil d'exécution du joueur " + this.joueur.nom() + " a été interrompu.") ;
                        return ;
                    }
                }
                while (actionIA != null) ;
    
                // Lancer l'action de fin de tour (comme le fait un joueur humain quand il utilise un bouton)
                // TODO Système d'accès centralisé aux actions
System.out.println (this.joueur.nom() + " : Au suivant") ;
                new ActionFinTourJoueur().actionPerformed (null) ;
    
//                // Mettre à jour la date de dernière action
//                // (pour attendre une demi-seconde à partir de maintenant)
//                dateDerniereAction = System.currentTimeMillis() ;
            }
        }
System.out.println (this.joueur.nom() + " : Au revoir") ;

    }
    
    
    
    // Implémentation de Runnable
    public void run ()
    {
        this.lancer() ;
    }
    
}
