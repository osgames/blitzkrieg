/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;



// IA très simple, pour tester l'organisation du jeu intégrant les IA
public class IAPrimitive implements IA
{

    private Joueur joueur ;             // Joueur associé à cette IA
    private Partie partie ;             // Partie à laquelle joue le joueur
    
    
    
    // Constructeur
    public IAPrimitive (Joueur joueur, Partie partie)
    {
        this.joueur = joueur ;
        this.partie = partie ;
    }
    
    
    
    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour ()
    {
        // (ne rien faire)
    }
    
    
    // Méthode appelée pour jouer le tour de l'IA
    public ActionIA choisirAction ()
    {
        // Si on est au tour d'initialisation, ne pas proposer d'action
        if (this.partie.tourInitialisation())
            return null ;
        
        // S'il n'y a plus d'actions, ne pas proposer d'action
        if (this.joueur.nbActions() <= 0)
            return null ;
        
        // S'il y a des troupes en réserve, chercher une case à attaquer et proposer cette case
        if (this.joueur.nbTroupesReserve() > 0)
        {
            Terrain terrain = this.partie.terrain() ;
            for (int y = 0 ; y < terrain.hauteur() ; y++)
            for (int x = 0 ; x < terrain.largeur() ; x++)
            {
                Case   caseCourante = terrain.caseEn (x, y) ;
                Joueur proprietaire = caseCourante.proprietaire() ;
                if (proprietaire != null && ! proprietaire.equals (this.joueur))
                {
                    if (this.partie.regles().attaquePossible (this.joueur, x, y))
                        return new ActionIA (ActionIA.ATTAQUER, caseCourante) ;
                }
            }
            // Si aucune attaque n'est possible, ne pas proposer d'action
            return null ;
        }
        // S'il n'y a pas de troupe en réserve mais qu'il y en a sur le terrain, chercher une troupe à
        //   récupérer et proposer cette action
        else if (this.joueur.nbTroupesTerrain() > 0)
        {
            Terrain terrain = this.partie.terrain() ;
            for (int y = 0 ; y < terrain.hauteur() ; y++)
            for (int x = 0 ; x < terrain.largeur() ; x++)
            {
                Case   caseCourante = terrain.caseEn (x, y) ;
                Joueur proprietaire = caseCourante.proprietaire() ;
                if (this.joueur.equals (proprietaire) && caseCourante.troupePresente())
                    return new ActionIA (ActionIA.RETIRER, caseCourante) ;
            }
            // Si aucune troupe disponible (par exemple si elles sont sur des cases mortes), ne pas proposer d'action
            return null ;
        }
        // Sinon ne pas proposer d'action
        else
        {
            return null ;
        }
    }

}
