/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;



// IA qui ne fait rien
public class IAInactive implements IA
{

    // Méthode appelée au début du tour du joueur, qui permet à l'IA de réinitialiser certains éléments
    public void debutTour ()
    {
        // (ne rien faire)
    }
    
    
    // Méthode appelée pour jouer le tour de l'IA
    public ActionIA choisirAction ()
    {
        // (ne rien faire)
        return null ;
    }

}
