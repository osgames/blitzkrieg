/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements.ia;

import static fr.blitzkrieg.donnees.Terrain.appartenantA ;
import static fr.blitzkrieg.donnees.Terrain.nAppartenantPasA ;
import static fr.blitzkrieg.donnees.Terrain.vivantes ;

import java.util.ArrayList ;
import java.util.List ;
import java.util.Map ;
import java.util.Map.Entry ;
import java.util.Random ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.traitements.Regles ;



// Fonctions utiles pour l'IA.
// TODO Voir si ça ne peut pas se mettre ailleurs, certaines fonctions pourraient servir à autre chose qu'à
//      l'IA
public class UtilesIA
{
    
    // Renvoie les cases possédées par un adversaire du joueur donné et dont l'attaque permet d'augmenter
    //   la zone visible par le joueur en paramètre.
    // Renvoie une liste vide si aucune case trouvée.
    // NOTE Le fait que les cases soient attaquables présente plusieurs intérêts :
    //        - Elles appartiennent à un autre joueur (en cas de problème / règle particulière
    //          qui ferait qu'on a une case au joueur courant qui a ds vosines inexplorées, il
    //          vaut mieux éviter les ennuis)
    //        - Il ne s'agit pas de n'importe quelle case au milieu du territoire ennemi ou
    //          accessible à travers la mer mais que la règle d'attaque navale soit désactivée
    public static List<Case> casesExplorables (Partie partie, Joueur joueur)
    {
        Terrain    terrain            = partie.terrain() ;
        List<Case> casesSelectionnees = new ArrayList() ;
        
        
        // Cases attaquables
        List<Case> casesAttaquables = casesAttaquables (partie, joueur, null, false) ;
        
        // Cases explorables
        for (Case c : casesAttaquables)
        {
            if (! c.decouverte (joueur) || terrain.existeVoisineNonDecouvertePar (c.x(), c.y(), joueur))
                casesSelectionnees.add (c) ;
        }
        
        // Renvoyer les cases trouvées
        return casesSelectionnees ;
    }
    

    // Renvoie les cases attaquables par le joueur donné respectant les critères en paramètre
    // TODO Mettre des fonctions en commun pour les IA (ces fonctions ne concernent pas que le terrain
    //      mais aussi les règles et la partie en cours)
    // TODO Spécifier plus précisément les valeurs qui peuvent être passées en paramètre
    public static List<Case> casesAttaquables (Partie partie, Joueur joueur,
                                               TypeCase typeCase, boolean terreUniquement)
    {
        Terrain    terrain            = partie.terrain() ;
        Regles     regles             = partie.regles() ;
        List<Case> casesSelectionnees = new ArrayList() ;


        // Chercher les cases attaquables
        for (int y = 0 ; y < terrain.hauteur() ; y++)
        for (int x = 0 ; x < terrain.largeur() ; x++)
        {
            Case caseCourante = terrain.caseEn (x, y) ;
            if (typeCase == null || typeCase.equals (caseCourante.type()))
            {
                if (terreUniquement && regles.attaqueTerrestrePossible (joueur, x, y) ||
                                       regles.attaquePossible          (joueur, x, y))
                {
                    casesSelectionnees.add (caseCourante) ;
                }
            }
        }
        
        // Renvoyer les cases attaquables
        return casesSelectionnees ;
    }
    
    
    // Renvoie la liste des cases de front d'un joueur (les cases vivantes lui appartenant et qui
    //   sont à côté d'une case ennemie.
    // TODO Prendre en compte les ports et découper en deux : les cases de front à partir desquelles on
    //      peut attaquer (certains ports compris) et les cases de front qui peuvent être attaquées
    //      (certaines cases de côtes comprises)
    public static List<Case> casesFront (Partie partie, Joueur joueur)
    {
        // Initialisations
        Terrain terrain = partie.terrain() ;
        
        // Chercher les cases de front
        List<Case> casesVivantesJoueur = vivantes (appartenantA (joueur, terrain.toutesLesCases())) ;
        List<Case> casesFront = new ArrayList() ;
        for (Case c : casesVivantesJoueur)
        {
            if (! nAppartenantPasA (joueur, terrain.casesVoisines (c)).isEmpty())
                casesFront.add (c) ;
        }
        
        // Renvoyer les cases trouvées
        return casesFront ;
    }
    
    
    // Calcule la distance entre cette case et le front (une case ennemie).
    // Une distance de 1 signifie qu'une case voisine est possédée par un ennemi.
    // Renvoie Integer.MAX_VALUE si ausune case ennemie n'est trouvée.
    // TODO A améliorer : il faudrait prendre en compte les futures alliances et les capacités d'action
    //      des joueurs (par exemple dans le scénario aléatoire le joueur "territoire inexploré" ne
    //      représente pas une menace)
    // TODO Tenir compte de la spécificité des déplacements par mer (possibles ou non, raccourcis par
    //      rapport à la distance réelle ; regarder ce qui existe déjà dans les informations sur les
    //      cases desservies par tel ou tel port).
    //          => Il vaudrait mieux créer une grille et la remplir avec la distance minimale pour atteindre
    //             chaque case ; en plus ça servira pour d'autres calculs d'IA.
    public static int distanceFront (Partie partie, Case caseConsideree)
    {
        Joueur joueur         = caseConsideree.proprietaire() ;
        Terrain terrain       = partie.terrain() ;
        Integer[][] distances = terrain.grilleDistancesDepuis (caseConsideree) ;
        
        
        // Chercher la case ennemie la plus proche
        int distMin = Integer.MAX_VALUE ;
        for (int x = 0 ; x < distances.length    ; x++)
        for (int y = 0 ; y < distances[x].length ; y++)
        {
            Case c = terrain.caseEn(x,y) ;
            if (distances[x][y] != null && distances[x][y] < distMin &&
                ! joueur.equals (c.proprietaire()) && c.type() != TypeCase.CASE_MER)
            {
                distMin = distances[x][y] ;
            }
        }
        
        // Renvoyer la distance à la case la plus proche
        return distMin ;
    }
    
    
    // Calcule l'effort maximal réalisable en attaque par un joueur.
    // Il s'agit du nombre maximal de cases qu'il peut attaquer, en tenant compte du nombre d'actions,
    //   du nombre de troupes disponibles en réserve et sur le terrain.
    // Cette valeur peut être utilisée par les IA pour limiter leurs ambitions quand elles cherchent
    //   des plans à exécuter.
    public static int effortMaximalAttaque (Joueur joueur)
    {
        return (joueur.nbTroupesReserve() >= joueur.nbActions() ?
                  joueur.nbActions() :
                  joueur.nbTroupesReserve() + (joueur.nbActions() - joueur.nbTroupesReserve()) / 2) ;
    }
    
    
    // Sélectionne une case parmi celles de la table en paramètre.
    // Sélectionne la case de plus haute valeur (une au hasard ayant la plus grande valeur en cas d'égalité).
    // Renvoie null s'il n'y a pas de cases dans la table en paramètre.
    public static Case meilleureCase (Map<Case,Integer> valsCases, Random genAlea)
    {
        // Chercher la plus haute valeur
        int valMax = 0 ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue() > valMax)
                valMax = valeur.getValue() ;
        }
        
        // Récupérer les cases ayant la meilleure valeur
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue().intValue() == valMax)
                casesSelectionnees.add (valeur.getKey()) ;
        }
        
        // Renvoyer une case parmi les candidates
        return (casesSelectionnees.isEmpty() ?
                  null :
                  casesSelectionnees.get (genAlea.nextInt (casesSelectionnees.size()))) ;
    }
    
    
    // Sélectionne une case parmi celles de la table en paramètre.
    // Sélectionne la case de plus basse valeur (une au hasard ayant la plus petite valeur en cas d'égalité).
    // Renvoie null s'il n'y a pas de cases dans la table en paramètre.
    public static Case plusMauvaiseCase (Map<Case,Integer> valsCases, Random genAlea)
    {
        // Cas où on n'a aucune case en entrée
        if (valsCases.isEmpty())
            return null ;
        
        // Chercher la plus basse valeur
        int valMin = valsCases.values().iterator().next() ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue() < valMin)
                valMin = valeur.getValue() ;
        }

        // Récupérer les cases ayant la plus petite valeur
        List<Case> casesSelectionnees = new ArrayList() ;
        for (Entry<Case,Integer> valeur : valsCases.entrySet())
        {
            if (valeur.getValue().intValue() == valMin)
                casesSelectionnees.add (valeur.getKey()) ;
        }

        // Renvoyer une case parmi les candidates
        return (casesSelectionnees.isEmpty() ?
                  null :
                  casesSelectionnees.get (genAlea.nextInt (casesSelectionnees.size()))) ;
    }
    
}
