/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements;

import java.util.HashMap ;
import java.util.List ;
import java.util.Map ;
import java.util.Random ;
import java.util.Map.Entry ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;



// Classe chargée d'appliquer les règles du jeu
// NOTE On rassemble ici les règles le plus possible, donc par exemple le rechargement des actions
//      et troupes au début du tour du joueur doit être calculé ici (à partir des paramètres stockés
//      dans le joueur) plutôt que dans des méthodes du joueur (auxquelles il faudrait passer des
//      infos supplémentaires comme le nombre de villes qu'il contrôle, ... ; et pour d'autres méthodes
//      il faudrait éventuellement passer des options pour effectuer les traitements)
//          => Les classes de données sont assez simples et possèdent des méthodes permettant de traiter
//             leurs données de la manière dont elles sont manipulées dans le programme (pas avec des
//             accesseurs/modificateurs qui ne font rien sur tous les attributs), mais elles n'appliquent
//             pas les règles, qui doivent être rassemblées au plus dans cette classe
//              => A la fin, relire les règles et vérifier que peut être lu dans cette classe (ce qui
//                 permet de se lire l'ensemble des règles au même endroit)
//                  ! Il reste toujours des choses truobles : par exemple la limite de troupes dans la
//                    réserve du joueur : actuellement c'est le joueur qui se charge de respecter
//                    la limite et c'est assez logique qu'il s'en soucie, mais ça veut dire qu'un détail
//                    des règles s'est échappé et se trouve dans une autre classe...
// NOTE * Pas de vérification pour déposer/retirer des troupes sur une case invisible : ça ne doit
//        simplement pas arriver à cause de la manière dont sont calculées les cases visibles pour un
//        joueur
//      * Pas de problème pour attaquer dans le noir : pour les attaques terrestres ça ne devrait pas
//        arriver à cause de la manière dont est calculée la visibilité d'un joueur ; pour les attaques
//        navales c'est autorisé
public class Regles
{
    
    // Constantes
    // ! Eviter de les utiliser depuis l'extérieur, mais parfois utile pour des estimations approximatives
    //   de l'IA.
    // (attaque)
    public static final int VALEUR_ATTAQUE_TROUPE           = 10 ;      // Attaque de la troupe envoyée
    public static final int BONUS_ATTAQUE_TROUPE_ADJACENTE  =  5 ;      // Troupe sur une case adjacente
    public static final int BONUS_ATTAQUE_VILLE_ADJACENTE   =  3 ;      // Ville  sur une case adjacente
    public static final int VALEUR_ATTAQUE_NAVALE           =  2 ;      // Valeur d'une attaque navale
    // (défense)
    public static final int VALEUR_DEFENSE_CASE_MORTE       =  0 ;      // Case morte (aucun bonus possible)
    public static final int VALEUR_DEFENSE_TERRE            =  2 ;      // Case vide
    public static final int VALEUR_DEFENSE_VILLE            = 25 ;      // Case contenant une ville
    public static final int BONUS_DEFENSE_TROUPE            = 13 ;      // Troupe sur la case
    public static final int BONUS_DEFENSE_TROUPE_ADJACENTE  =  5 ;      // Troupe sur une case adjacente
    public static final int BONUS_DEFENSE_VILLE_ADJACENTE   =  5 ;      // Ville  sur une case adjacente
    
    
    // Données
    private Partie partie ;             // Partie sur laquelle s'appliquent les règles (pour ne pas avoir à la passer à chaque appel d'une méthode)
    
    // TODO Est-ce qu'on devrait stocker ça dans la partie plutôt ?
    //          => Oui, pour que ce soit sauvegarde avec le reste ; sinon ce sera perdu après un
    //             rechargement (ça risque d'arriver assez peu, mais mieux vaut faire ça correctement)
    // TODO ? Renommer ça en attaque, pour uniformiser (mieux : garder "assut en cours", qui correspond à l'action globale, et nbAttaquesEchouees, qui indique les attaques individuelles)
    private Case caseAssautEnCours ;        // Case sur laquelle vient de se dérouler un assaut du joueur courant (assaut qui a échoué)
    private int  nbAttaquesEchouees ;       // Nombre d'attaques déjà menées au cours de cet assaut sans réussir à prendre la case

    
    
    // Constructeur
    public Regles ()
    {
    }
    
    
    // Renvoie la case sur laquelle un assaut est en cours (attaques menées par le joueur courant durant
    //   ce tour.
    // Renvoie null s'il n'y a pas de case attaquée en ce moment.
    // NOTE Utile pour l'IA par exemple, au lieu de dédoubler la mémorisation de cette information
    public Case caseAssautEnCours ()
    {
        return this.caseAssautEnCours ;
    }
    
    
//    // Crée une nouvelle partie associée à ces règles
//    // La partie est associée aux règles (tous les liens sont créés correctement).
//    // La partie est renvoyée pour la stocker ailleurs
//    // NOTE Pas moyen de s'assurer que les règles ont forcément une partie en cours parce qu'il faut
//    //      aussi faire le lien dans l'autre sens lors de la construction de la partie. Et à la limite
//    //      les règles peuvent changer de parties alors que l'inverse n'est pas possible.
//    //      (l'alternative c'est de passer la partie en paramètre à chaque méthode des règles, pour
//    //       avoir un objet représentant vraiment les règles, qui s'appliquent sur la partie founie ;
//    //       l'inconvénient c'est que c'est plus lourd et tant qu'à avoir un objet, autant qu'il serve,
//    //       on peut toujours créer d'autres objets pour d'autres parties par exemple)
//    public Partie creerNouvellePartie ()
//    {
////        this.partie = new Partie (this) ;
//        this.partie = new GenerateurCarteAleatoire().genererCarte (this, Arrays.asList ("Joueur 1", "Joueur2")) ;
////this.partie = new GenerateurCarteAleatoire().genererScenarioFrontRusse() ;
////try
////{
////    new StockagePartie().sauvegarder (this.partie, "front-russe.scn", false) ;
////}
////catch (IOException e)
////{
////    e.printStackTrace() ;
////}
//        return this.partie ;
//    }
    // Associe une partie aux règles (notamment quand on charge une partie)
    //  => Revoir cette question, avec les notes plus haut
    public void associerPartie (Partie partie)
    {
        this.partie = partie ;
    }
    
    
    
    // TODO Organiser toutes ces méthodes (+ commenter)
    private void reinitialiserAssautEnCours ()
    {
        this.caseAssautEnCours  = null ;
        this.nbAttaquesEchouees = 0 ;
    }
    // TODO Renommer, c'est pas très beau
    private void modifAssautEnCours (Case caseAttaquee)
    {
        if (caseAttaquee.equals (this.caseAssautEnCours))
        {
            this.nbAttaquesEchouees++ ;
        }
        else
        {
            this.caseAssautEnCours  = caseAttaquee ;
            this.nbAttaquesEchouees = 1 ;
        }
    }
    
    
    // Effectue les actions de début du tour du joueur donné
    public void debutTourJoueur (Joueur joueur)
    {
        
// NOTE Pas possible d'avoir 0 villes, on ne traite ici que les joueurs actifs
//          => Eh merde, c'est embêtant pour la consommation des cases mortes : il faudrait exécuter les
//             actions de fin de tour des joueurs inactifs quand on les passe

        // Annuler l'éventuel assaut en cours
        // TODO Ce serait plus rigoureux à la fin du tour du joueur (première action qui devrait être
        //      déclenchée par le clic sur le bouton "Fin de tour")
        // TODO A bien tester
        // TODO Mis en dehors du test du premier tour pour être sûr que ce soit fait quand on gère une nouvelle
        //      partie ; à vérifier, pour l'instant on n'a rien lors de l'initialisation de la partie,
        //      ce qui suppose qu'on recrée l'objet de règles à chaque partie : à précisier et spécifier
        reinitialiserAssautEnCours() ;
            
        // Etapes à effectuer aux tours "normaux"
        if (! this.partie.tourInitialisation())
        {
            // Ajouter les nouvelles actions et troupes
            if (! this.partie.tourInitialisation())
            {
                int nbVilles = this.partie.terrain().nbVillesControleesPar (joueur) ;
                joueur.ajouterActionsReserve  (joueur.nbActionsParTour() + nbVilles * joueur.nbActionsParVille()) ;
                joueur.ajouterTroupesReserves (joueur.nbTroupesParTour() + nbVilles * joueur.nbTroupesParVille()) ;
            }
        }
    }
    
    
    // Effectue les actions de fin du tour du joueur donné
    public void finTourJoueur (Joueur joueur)
    {
        Terrain terrain = this.partie.terrain() ;
        
        
        // Etapes à effectuer aux tours "normaux"
        if (! this.partie.tourInitialisation())
        {
            // Calculer et marquer les cases mortes sur le terrain
            // TODO A vérifier : pour les attaques navales, si on ne capture pas une ville dans le tour les
            //      cases sont immédiatement perdues (ce qui correspond bien à cette gestion des cases mortes)
            //          ! Pas possible à vérifier dans l'ancienne version de Pendulous (du moins sans payer
            //            pour avoir l'éditeur de scénarios)
            terrain.mettreAJourCasesMortes() ;
            
            // Les cases mortes du joueur proches d'un territoire vivant sont transférées à ce territoire
            // Les troupes sur les cases mortes (non-ravitaillées) sont retirées, que la case soit prise
            //   ou non
            Map<Case,Joueur> transferts = new HashMap() ;
            // TODO Le terrain devrait pouvoir fournir les cases mortes d'un joueur donné
            for (Case caseMorte : terrain.casesMortes())
            {
                if (joueur.equals (caseMorte.proprietaire()))
                {
                    // Lister les propriétaires des cases voisines vivantes
                    // (si on a plusieurs fois le même, on garde les différentes occurrences pour lui donner
                    //  plus de chances de prendre la case)
                    List<Joueur> pretendants = terrain.proprietairesCasesVoisinesVivantes (caseMorte.x(), caseMorte.y()) ;
                    
                    // Choisir un nouveau propriétaire
                    if (!pretendants.isEmpty())
                    {
                        Joueur nouvProprietaire = pretendants.get (Blitzkrieg.instance().genAlea().nextInt (pretendants.size())) ;
                        transferts.put (caseMorte, nouvProprietaire) ;
                    }
                    
                    // Dans tous les cas, retirer les troupes du terrain
                    if (caseMorte.troupePresente())
                    {
                        // TODO Des méthodes dans Regles pour ce genre d'actions qui doivent toujours
                        //      se dérouler ensemble ? Ca évite d'avoir la possibilité de se tromper
                        joueur.retirerTroupeTerrain() ;
                        caseMorte.retirerTroupe() ;
                    }
                }
            }
            
            // Effectuer les transferts de cases
            for (Entry<Case,Joueur> transfert : transferts.entrySet())
            {
                Case   caseMorte        = transfert.getKey() ;
                Joueur nouvProprietaire = transfert.getValue() ;
                
                // Prendre le territoire
                caseMorte.capturerCase (nouvProprietaire, false) ;
                terrain.regarder (nouvProprietaire, caseMorte.x(), caseMorte.y()) ;
            }
        
            
            // Calculer les points de victoire gagnés
            // CODE Séparer les fonctions entre celles qui correspondent à des étapes de la partie
            //      (début tour, fin de tour, ...) et celles qui font des calculs particuliers (compter
            //       les points, ...)
            int nbPointsGagnes = terrain.nbTerresVivantesControleesPar (joueur) * this.partie.nbPointsCase() +
                                 terrain.nbVillesControleesPar         (joueur) * this.partie.nbPointsVille() ;
            joueur.ajouterPointsVictoire (nbPointsGagnes) ;
        }
    }
    
    
    // Dépose une troupe sur un territoire appartenant déjà au joueur
    // Renvoie vrai si l'action a été autorisée (et donc effectuée)
    // TODO Est-ce qu'il ne faudrait pas vérifier que la partie est en cours (et non suspendue) ? Ca pourrait
    //      permettre de détecter des problèmes, notamment avec les IA
    public boolean deposer (Joueur joueur, int x, int y)
    {
        Case caseCible ;                // Case du terrains sur laquelle s'effectue l'action
        boolean actionAutorisee ;       // Indique si l'action est autorisée
        
        
        // Récupérer la case concernée
        caseCible = this.partie.terrain().caseEn (x, y) ;
        
        // Déterminer si l'action est possible
        actionAutorisee = (this.partie.tourInitialisation() || joueur.nbActions() > 0) &&
                          joueur.nbTroupesReserve() > 0                                 &&
                          caseCible.proprietaire().equals (joueur)                      &&
                          caseCible.type() == TypeCase.CASE_TERRE                       &&
                          caseCible.estVivante()                                        &&
                          ! caseCible.troupePresente() ;
                          
        // Exécuter l'action
        if (actionAutorisee)
        {
            if (! this.partie.tourInitialisation())
                joueur.consommerAction() ;
            joueur.transfererDeReserveVersTerrain() ;
            caseCible.deposerTroupe() ;
        }
        
        // Renvoyer l'autorisation
        return actionAutorisee ;
    }
    
    
    // Retire une troupe du joueur du terrain
    // Renvoie vrai si l'action a été autorisée (et donc effectuée)
    public boolean retirer (Joueur joueur, int x, int y)
    {
        Case caseCible ;                // Case du terrains sur laquelle s'effectue l'action
        boolean actionAutorisee ;       // Indique si l'action est autorisée
        
        
        // Récupérer la case concernée
        caseCible = this.partie.terrain().caseEn (x, y) ;
        
        // Déterminer si l'action est possible
        actionAutorisee = (this.partie.tourInitialisation() || joueur.nbActions() > 0) &&
                          caseCible.proprietaire().equals (joueur)                      &&
                          caseCible.troupePresente()                                    &&
                          caseCible.estVivante() ;
        
        // Exécuter l'action
        if (actionAutorisee)
        {
            if (! this.partie.tourInitialisation())
                joueur.consommerAction() ;
            joueur.transfererDeTerrainVersReserve() ;
            caseCible.retirerTroupe() ;
        }
        
        // Renvoyer l'autorisation
        return actionAutorisee ;
    }
    
    
    // Attaque une case ennemie
    // Renvoie vrai si l'action a été autorisée (que l'attaque ait réussi ou non)
    public boolean attaquer (Joueur joueur, int x, int y)
    {
        Case caseCible ;                // Case du terrains sur laquelle s'effectue l'action
        boolean actionAutorisee ;       // Indique si l'action est autorisée
        
        
        // Récupérer la case concernée
        caseCible = this.partie.terrain().caseEn (x, y) ;
        
        // Déterminer si l'action est possible
        // (pas de vérification du type de la case : on peut prendre tout ce qui est à quelqu'un)
        actionAutorisee = ! this.partie.tourInitialisation() &&
                          joueur.nbActions()        > 0      &&
                          joueur.nbTroupesReserve() > 0      &&
                          attaquePossible (joueur, x, y) ;

        // Exécuter l'action
        if (actionAutorisee)
        {
            Joueur attaquant ;
            Joueur defenseur ;
            double  probaReussite ;
            boolean attaqueReussie ;
            Random  genAlea ;

            // Calculer le résultat du combat
            // TODO Regarder s'il ne faut pas tester le résultat de la fonction de calcul de proba. je
            //      ne crois pas, il me semble qu'on a déjà vérifier que l'attaque et la défense étaient
            //      possibles
            attaquant = joueur ;
            defenseur = caseCible.proprietaire() ;
            probaReussite  = probaReussiteAttaque (attaquant, x, y) ;
            genAlea        = Blitzkrieg.instance().genAlea() ;
            attaqueReussie = genAlea.nextDouble() < probaReussite ;
            
            // Appliquer le résultat de l'attaque
            attaquant.consommerAction() ;
            // (la case devient automatiquement visible [cas des cases attaquées par mer])
            caseCible.decouvrir (attaquant) ;
            if (attaqueReussie)
            {
                reinitialiserAssautEnCours() ;
                if (caseCible.troupePresente() && defenseur != null)     // TODO Arranger ce code et bien vérifier que les troupes sont bien comptées partout ; il devrait y avoir forcément un propriétaire, non ?
                    defenseur.retirerTroupeTerrain() ;
                if (caseCible.type() == TypeCase.CASE_TERRE)
                    attaquant.transfererDeReserveVersTerrain() ;
                else if (caseCible.type() == TypeCase.CASE_VILLE)
                    attaquant.retirerTroupeReserve() ;
                else
                    throw new IllegalStateException ("Type de case invalide : " + caseCible.type()) ;
                caseCible.capturerCase (joueur, true) ;
                this.partie.terrain().mettreAJourVisibiliteProprietaire (x, y) ;
            }
            else
            {
                // NOTE A bien faire avant les actions qui modifient les données (-> réaffichage correct
                //      quand le panneau reçoit la notification de modification)
                modifAssautEnCours (caseCible) ;
                attaquant.retirerTroupeReserve() ;
            }
            
            // Mettre à jour les statistiques de combat
            attaquant.modifStatsAttaques (probaReussite,       attaqueReussie) ;
            defenseur.modifStatsDefenses (1.0 - probaReussite, ! attaqueReussie) ;
        }
        
        // Renvoyer l'autorisation
        return actionAutorisee ;
    }
    
    
    // Calcule la force d'attaque d'un joueur donné sur une case
    // Renvoie null si l'attaque n'est pas possible
    // TODO Ajouter le multiplicateur pour les attaques navales (à déterminer automatiquement :
    //      voir attaquePossible)
    public Double forceAttaque (Joueur joueur, int x, int y)
    {
        Terrain terrain ;
        Case    caseCible ;
        Joueur  attaquant ;
        

        // Initialisations
        terrain   = this.partie.terrain() ;
        caseCible = terrain.caseEn (x, y) ;
        attaquant = joueur ;
        
        // Vérifier que l'attaque est possible
        // TODO Il vraudrait mieux récupérer les deux résultats pour les attaques terrestre et navale
        //      et utiliser les deux pour cette condition ; ça éviterait de refaire les calculs pour
        //      déterminer si l'attaque est terrestre ou navale ; ou alors une fonction qui renvoie un
        //      résultat parmi 3 : non, terrestre ou navale
        if (! attaquePossible (attaquant, x, y))
            return null ;
            
        // Déterminer s'il s'agit d'une attaque terrestre ou navale
        boolean attaqueNavale = ! attaqueTerrestrePossible (joueur, x, y) ;
        
        // Calcul de la force d'attaque
        // TODO Ajouter le bonus d'attaques successives : a priori le bonus est de
        //      nbAttaques échouées * 25% attaque de base
        double valAttaque ;
        if (attaqueNavale)
        {
            valAttaque = VALEUR_ATTAQUE_NAVALE ;
        }
        else
        {
            valAttaque  = VALEUR_ATTAQUE_TROUPE ;
            valAttaque += terrain.nbTroupesVoisinesVivantesAppartenantA (x, y, attaquant) * BONUS_ATTAQUE_TROUPE_ADJACENTE ;
            valAttaque += terrain.nbVillesVoisinesAppartenantA          (x, y, attaquant) * BONUS_ATTAQUE_VILLE_ADJACENTE ;
            valAttaque *= attaquant.efficacite() ;
            valAttaque *= (caseCible.equals (this.caseAssautEnCours) ? 1 + 0.25 * this.nbAttaquesEchouees : 1) ;
        }
        
        // Renvoyer la force d'attaque
        return valAttaque ;
    }
    
    
    // Calcule la force de la défense d'une case
    // Renvoie null si cette information n'a pas de sens (cases de mer)
    public Double forceDefense (int x, int y)
    {
        Terrain terrain ;
        Case    caseCible ;
        Joueur  defenseur ;
        

        // Initialisations
        terrain   = this.partie.terrain() ;
        caseCible = terrain.caseEn (x, y) ;
        defenseur = caseCible.proprietaire() ;
        
        
        // Cas particulier des cases de mer
        if (caseCible.type() == TypeCase.CASE_MER)
        {
            return null ;
        }
        // Cas particulier des cases mortes
        else if (caseCible.estMorte())
        {
            return (double) VALEUR_DEFENSE_CASE_MORTE ;
        }
        // Cas normal
        else
        {
            // Calcul de la force de défense
            double valDefense ;
            valDefense  = (caseCible.type() == TypeCase.CASE_VILLE ? VALEUR_DEFENSE_VILLE : VALEUR_DEFENSE_TERRE) ;
            valDefense += (caseCible.troupePresente()              ? BONUS_DEFENSE_TROUPE : 0) ;
            valDefense += terrain.nbTroupesVoisinesVivantesAppartenantA (x, y, defenseur) * BONUS_DEFENSE_TROUPE_ADJACENTE ;
            valDefense += terrain.nbVillesVoisinesAppartenantA          (x, y, defenseur) * BONUS_DEFENSE_VILLE_ADJACENTE ;
            valDefense *= defenseur.efficacite() ;
            
            // Renvoyer la force de défense
            return valDefense ;
        }

    }
    
    
    // Calcule la probabilité de réussite d'une attaque d'un joueur donné sur la case indiquée
    // Renvoie une probabilité en 0 et 1 s'il y a une chance de prendre la case
    // Renvoie null si l'attaque n'est pas possible (pour simplement savoir si une attaque est possible
    //   ou non, utiliser plutôt la méthode consacrée)
    // NOTE L'efficacité est appliqué au score global d'attaque ou de défense, pas seulement aux troupes ;
    //      à vérifier [-> Front russe]
    public Double probaReussiteAttaque (Joueur joueur, int x, int y)
    {
        return probaReussiteAttaque (forceAttaque (joueur, x, y), forceDefense (x, y)) ;
    }
    
    // Calcule la probabilité de réussite d'une attaque en fonction des valeurs d'attaque et de défense
    // Renvoie une probabilité en 0 et 1 s'il y a une chance de prendre la case
    // Renvoie null si l'attaque n'est pas possible
    public Double probaReussiteAttaque (Double valAttaque, Double valDefense)
    {
        if (valAttaque == null || valDefense == null)
            return null ;
        else
            return valAttaque / (valAttaque + valDefense) ;
    }
    
    
    // Indique si une attaque est possible
    public boolean attaquePossible (Joueur joueur, int x, int y)
    {
        return attaqueTerrestrePossible (joueur, x, y) ||
               attaqueNavalePossible    (joueur, x, y) ;
    }
    
    // Indique si une attaque terrestre est possible
    public boolean attaqueTerrestrePossible (Joueur joueur, int x, int y)
    {
        Terrain terrain   = this.partie.terrain() ;
        Case    caseCible = terrain.caseEn (x, y) ;
        return ! this.partie.tourInitialisation()         &&
               caseCible.proprietaire() != null           &&
               ! caseCible.proprietaire().equals (joueur) &&
               terrain.existeVoisineVivanteAppartenantA (x, y, joueur) ;
    }
    
    // Indique si une attaque navale est possible
    public boolean attaqueNavalePossible (Joueur joueur, int x, int y)
    {
        Terrain terrain   = this.partie.terrain() ;
        Case    caseCible = terrain.caseEn (x, y) ;
        return this.partie.invasionsNavalesPermises()     &&
               ! this.partie.tourInitialisation()         &&
               caseCible.proprietaire() != null           &&
               ! caseCible.proprietaire().equals (joueur) &&
               terrain.merBordant (x, y) != null          &&
               terrain.merBordant(x,y).portAppartenantA (joueur) ;
    }

}
