/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.traitements;

import java.awt.Color ;
import java.util.ArrayList ;
import java.util.Collections ;
import java.util.HashMap ;
import java.util.List ;
import java.util.Map ;
import java.util.Random ;
import java.util.Map.Entry ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Mer ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.utiles.collections.Listes ;



// Outil de génération de cartes aléatoires
// TODO Arranger plus tard : il faudrait déjà séparer la génération de la carte et la création
//      du scénario d'affrontement
public class GenerateurCarteAleatoire
{

    // Génère une carte aléatoire
    // TODO Si on ne veut pas spécifier une graine particulière, il suffit de passer System.currentTimeMillis()
    //          ! Il vaudrait mieux quand même faire une fonction sans, ce serait plus clair
    //            (ouais enfin là elle ne sert pas)
    public Terrain genererCarte (List<Joueur> joueurs, int largeurCarte, int hauteurCarte, long graineGenAlea)
    {
        // TODO A arranger pour être sûr que tous les endroits sont accessibles (avec la gestion des attaques
        //      navales)
        // CODE Arranger le code
        Case[][] cases = new Case[largeurCarte][hauteurCarte] ;

        // Déposer les graines pour la génération du terrain
        // TODO Permettre de régler ces valeurs quand on génère un scénario aléatoire
        double proportionMers   = 0.01 ;
//double proportionMers = 0.1 ;
        double proportionTerres = 0.04 ;
        double proportionVilles = 0.01 ;
        int nbGrainesMers   = (int) Math.ceil (proportionMers   * largeurCarte * hauteurCarte) ;
        int nbGrainesTerres = (int) Math.ceil (proportionTerres * largeurCarte * hauteurCarte) ;
        // (déposer les graines au hasard)
        Random genAlea = new Random (graineGenAlea) ;
        for (int i = 0 ; i < nbGrainesMers ; i++)
        {
            int x = genAlea.nextInt (largeurCarte) ;
            int y = genAlea.nextInt (largeurCarte) ;
            cases[x][y] = new Case (x, y, TypeCase.CASE_MER, null, false) ;
        }
        for (int i = 0 ; i < nbGrainesTerres ; i++)
        {
            int x = genAlea.nextInt (largeurCarte) ;
            int y = genAlea.nextInt (largeurCarte) ;
            cases[x][y] = new Case (x, y, TypeCase.CASE_TERRE, null, false) ;
        }
        
        // Faire croître le terrain autour des graines
        // TODO Décider à quel moment le terrain est créé et ne plus manipuler les cases depuis l'extérieur
        //      ensuite (? : besoin pour leur donner un type)
        //          => A arranger ; fonctions du générateur qui récupèrent les cases voisines dans un
        //             tableau de cases par exemple ?
        // CODE Est-ce qu'on peut faire quelque chose pour factoriser tout ce code qui consiste à répandre
        //      les propriétés de certaines cases ? Peut-être une fonction qui renvoie toutes les cases
        //      à traiter ? Ou carrément une fonction qui modifie les attributs des objets directement ?
        //      (bof ; il vaudrait mieux appeler les méthodes appropriées ; et il faut encore exprimer a
        //      condition de choix des cases... beuh... ça va être chiant en Java)
        boolean modifieType ;
        do
        {
            // Déterminer le type des cases inexistantes
            modifieType = false ;
            List<Case> nouvCases = new ArrayList() ;
            for (int x = 0 ; x < largeurCarte ; x++)
            for (int y = 0 ; y < hauteurCarte ; y++)
            {
                Case caseCourante = cases[x][y] ;
                if (caseCourante == null)
                {
                    // Choisir un type parmi celui des cases voisines
                    // TODO Et si on prenait le type le plus représenté ? Ca éviterait peut-être que les
                    //      mers soient trop éfilochées par la propagation aléatoire
                    List<Case> casesVoisines = casesVoisines (cases, x, y, largeurCarte, hauteurCarte, null) ;
                    if (! casesVoisines.isEmpty())
                    {
                        TypeCase typeCase = casesVoisines.get (genAlea.nextInt (casesVoisines.size())).type() ;
                        nouvCases.add (new Case (x, y, typeCase, null, false)) ;
                        modifieType = true ;
                    }
                }
            }
    
            // Ajouter les nouvelles cases au terrain
            for (Case nouvCase : nouvCases)
                cases[nouvCase.x()][nouvCase.y()] = nouvCase ;

        } while (modifieType) ;
        
        // (placer les villes)
        int nbVillesAPlacer = (int) Math.ceil (proportionVilles * largeurCarte * hauteurCarte) ;
        for (int i = 0 ; i < nbVillesAPlacer ; i++)
        {
            int x = genAlea.nextInt (largeurCarte) ;
            int y = genAlea.nextInt (largeurCarte) ;
            if (cases[x][y].type() == TypeCase.CASE_TERRE)
                cases[x][y] = new Case (x, y, TypeCase.CASE_VILLE, null, false) ;
        }

        // Récupérer les mers dans la situation actuelle
        Terrain terrainTmp = new Terrain(largeurCarte,hauteurCarte,cases) ;
        List<Mer> mers = terrainTmp.mers() ;
        
        // Pour chaque mer, ajouter les ports nécessaires à la connexion de tous les territoires
        // TODO Il faudrait intégrer ça aussi dans le terrain, pour pouvoir valider les cartes au besoin
        // TODO Il faudrait éventuellement tester les cas extrêmes de cartes : que de la mer, ...
        //          => En fait il faudrait refuser ces cartes (=> liste de critères pour que la carte soit
        //             valide)
        for (Mer mer : mers)
        {
            // Si la mer possède plus d'une côte, il faut vérifier que chaque côte contient un port au moins
            if (mer.cotesParTerre().size() > 1)
            {
                for (List<Case> cote : mer.cotesParTerre())
                {
                    // Regarder s'il y a au moins un port
                    boolean portTrouve = false ;
                    for (Case caseCote : cote)
                    {
                        portTrouve = caseCote.type() == TypeCase.CASE_VILLE ;
                        if (portTrouve)
                            break ;
                    }
                    // S'il n'y a pas de port, une case au hasard devient un port
                    // TODO En principe il faudrait revenir à la construction du terrain et tout
                    //      reprendre, parce que le port posé peut servir à deux mers ; mais bon,
                    //      cette solution risque juste d'ajouter quelques ports en plus dans
                    //      des cas très rares, ce n'est pas grave
                    if (!portTrouve)
                    {
                        Case casePort = cote.get (genAlea.nextInt (cote.size())) ;
                        casePort = new Case (casePort.x(), casePort.y(), TypeCase.CASE_VILLE, null, false) ;
                        cases[casePort.x()][casePort.y()] = casePort ;
                    }
                }
            }
        }
        
        
        // Créer l'objet représentant le terrain
        Terrain terrain = new Terrain (largeurCarte, hauteurCarte, cases) ;
        
//        // Créer le terrain
//        Random genAlea = new Random (graineGenAlea) ;
//        int probaVilles =  1 ;
//        int probaMers   = 15 ;
//        for (int x = 0 ; x < largeurCarte ; x++)
//        for (int y = 0 ; y < hauteurCarte ; y++)
//        {
//            // (type de terrain)
//            TypeCase type ;
//            int nbAlea = genAlea.nextInt (100) ;
//            if (nbAlea < probaMers)
//                type = TypeCase.CASE_MER ;
//            else if (nbAlea < probaMers + probaVilles)
//                type = TypeCase.CASE_VILLE ;
//            else
//                type = TypeCase.CASE_TERRE ;
//            cases[x][y] = new Case (x, y, type, null, false) ;
//        }
//        Terrain terrain = new Terrain (largeurCarte, hauteurCarte, cases) ;
        
        // Attribuer les villes à des joueurs
        // NOTE Il faudrait un autre système pour placer les villes ou alors une vérification, parce que
        //      là on n'est pas strictement sûr qu'il y aura assez de villes
        // (créer une liste des numéros des villes et les mélanger)
        int nbVilles = terrain.nbVilles() ;
        List<Integer> numsVilles = Listes.creerSequenceNombres (nbVilles) ;
        Collections.shuffle (numsVilles, genAlea) ;
        // (attribuer les villes aux joueurs)
// TODO Ca a planté sur une carte de 10x10 : il faudrait planter proprement et/ou gérer les cas particuliers
//      quand la carte à générer est trop petite
        int NB_VILLES_INITIAL = 2 ;
        List<Integer> numsVillesJoueur1 = numsVilles.subList (0,                 NB_VILLES_INITIAL) ;
// TODO Plantage sur la carte suivante apparu un jour comme ça (ça arrive encore parfois) :
//Exception in thread "main" java.lang.IndexOutOfBoundsException: toIndex = 4
//    at java.util.SubList.<init>(AbstractList.java:602)
//    at java.util.RandomAccessSubList.<init>(AbstractList.java:758)
//    at java.util.AbstractList.subList(AbstractList.java:468)
//    at fr.blitzkrieg.traitements.GenerateurCarteAleatoire.genererCarte(GenerateurCarteAleatoire.java:208)
//    at fr.blitzkrieg.stockage.StockagePartie.charger(StockagePartie.java:254)
//    at fr.blitzkrieg.Blitzkrieg.creerPartieScenario(Blitzkrieg.java:102)
//    at fr.blitzkrieg.Blitzkrieg.main(Blitzkrieg.java:170)
        List<Integer> numsVillesJoueur2 = numsVilles.subList (NB_VILLES_INITIAL, 2 * NB_VILLES_INITIAL) ;
        
        // Donner effectivement les villes aux joueurs
        int cptVille = 0 ;
        for (int x = 0 ; x < largeurCarte ; x++)
        for (int y = 0 ; y < hauteurCarte ; y++)
        {
            if (cases[x][y].type() == TypeCase.CASE_VILLE)
            {
                cptVille++ ;
                if (numsVillesJoueur1.contains (cptVille))
                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (0)) ;
                else if (numsVillesJoueur2.contains (cptVille))
                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (1)) ;
                else
                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (2)) ;
            }
        }
        
        // Etendre le terrain des joueurs autour de leurs villes
        boolean modifieProprietaire ;
        do
        {
            // Attribuer à quelqu'un les cases de terre sans propriétaire
            modifieProprietaire = false ;
            Map<Case,Joueur> attributions = new HashMap() ;
            for (int x = 0 ; x < largeurCarte ; x++)
            for (int y = 0 ; y < hauteurCarte ; y++)
            {
                Case caseCourante = terrain.caseEn (x, y) ;
                if (caseCourante.type() == TypeCase.CASE_TERRE && caseCourante.proprietaire() == null)
                {
                    // Choisir un propriétaire parmi les voisins
                    List<Joueur> voisins = terrain.proprietairesCasesVoisinesVivantes (x, y) ;
                    if (! voisins.isEmpty())
                    {
                        Joueur nouvProprietaire = voisins.get (genAlea.nextInt (voisins.size())) ;
                        attributions.put (caseCourante, nouvProprietaire) ;
                        modifieProprietaire = true ;
                    }
                }
            }
            
            // Appliquer les attributions
            for (Entry<Case,Joueur> attribution : attributions.entrySet())
            {
                Case   caseCourante     = attribution.getKey() ;
                Joueur nouvProprietaire = attribution.getValue() ;
                caseCourante.choisirProprietaireInitial (nouvProprietaire) ;
            }
            
        } while (modifieProprietaire) ;
        

        // Renvoyer le terrain créé
        return terrain ;
    }
    
    
//    // Génère une partie
//    public Partie genererCarte (Regles regles, List<String> nomsJoueurs)
//    {
//        return genererCarte (regles, nomsJoueurs, System.currentTimeMillis()) ;
//    }
//    public Partie genererCarte (Regles regles, List<String> nomsJoueurs, long graineGenAlea)
//    {
//        // TODO A arranger pour être sûr que tous les endroits sont accessibles (avec la gestion des attaques
//        //      navales)
//        // TODO Génération des plans d'eau à partir de graines
//        // CODE Arranger le code
//        
//// TODO A organiser mieux, mais pour l'instatn ce n'est prévue que pour 2 joueurs + le joueur neutre
//if (nomsJoueurs.size() != 2)
//    throw new IllegalArgumentException ("Il devrait y avoir deux noms de joueurs et non " + nomsJoueurs.size()) ;
//        
//        int largeurCarte = 50 ;
//        int hauteurCarte = 50 ;
//        Case[][] cases = new Case[largeurCarte][hauteurCarte] ;
//        
//        // Créer les joueurs
//        List<Joueur> joueurs = new ArrayList() ;
//        List<Color> couleurs = Arrays.asList (Color.RED, Color.GRAY, Color.CYAN, new Color (0x008000)) ;       // TODO Gérer quelque part les limites et gérer la liste de couleurs ailleurs (voire permettre aux joueurs de choisir leur couleur plus librement)
//        for (int i = 0 ; i < nomsJoueurs.size() ; i++)
//        {
//            String nom    = nomsJoueurs.get (i) ;
//            Color couleur = couleurs.get    (i) ;
//            // TODO Mettre les valeurs dans des variables pour plus de clarté
//            joueurs.add (new Joueur (nom, couleur, true, 1.0, 50, 1, 1, 3, 2, 0, 4)) ;
//        }
//        joueurs.add (new Joueur ("Territoire inexploré", Color.CYAN, false, 1.0, 0, 0, 0, 0, 0, 0, 0)) ;
//        
//        // Déposer les graines pour la génération du terrain
//        // TODO Permettre de régler ces valeurs quand on génère un scénario aléatoire
//        double proportionMers   = 0.01 ;
////double proportionMers = 0.1 ;
//        double proportionTerres = 0.04 ;
//        double proportionVilles = 0.01 ;
//        int nbGrainesMers   = (int) Math.ceil (proportionMers   * largeurCarte * hauteurCarte) ;
//        int nbGrainesTerres = (int) Math.ceil (proportionTerres * largeurCarte * hauteurCarte) ;
//        // (déposer les graines au hasard)
//        Random genAlea = new Random (graineGenAlea) ;
//        for (int i = 0 ; i < nbGrainesMers ; i++)
//        {
//            int x = genAlea.nextInt (largeurCarte) ;
//            int y = genAlea.nextInt (largeurCarte) ;
//            cases[x][y] = new Case (x, y, TypeCase.CASE_MER, null, false) ;
//        }
//        for (int i = 0 ; i < nbGrainesTerres ; i++)
//        {
//            int x = genAlea.nextInt (largeurCarte) ;
//            int y = genAlea.nextInt (largeurCarte) ;
//            cases[x][y] = new Case (x, y, TypeCase.CASE_TERRE, null, false) ;
//        }
//        
//        // Faire croître le terrain autour des graines
//        // TODO Décider à quel moment le terrain est créé et ne plus manipuler les cases depuis l'extérieur
//        //      ensuite (? : besoin pour leur donner un type)
//        //          => A arranger ; fonctions du générateur qui récupèrent les cases voisines dans un
//        //             tableau de cases par exemple ?
//        // CODE Est-ce qu'on peut faire quelque chose pour factoriser tout ce code qui consiste à répandre
//        //      les propriétés de certaines cases ? Peut-être une fonction qui renvoie toutes les cases
//        //      à traiter ? Ou carrément une fonction qui modifie les attributs des objets directement ?
//        //      (bof ; il vaudrait mieux appeler les méthodes appropriées ; et il faut encore exprimer a
//        //      condition de choix des cases... beuh... ça va être chiant en Java)
//        boolean modifieType ;
//        do
//        {
//            // Déterminer le type des cases inexistantes
//            modifieType = false ;
//            List<Case> nouvCases = new ArrayList() ;
//            for (int x = 0 ; x < largeurCarte ; x++)
//            for (int y = 0 ; y < hauteurCarte ; y++)
//            {
//                Case caseCourante = cases[x][y] ;
//                if (caseCourante == null)
//                {
//                    // Choisir un type parmi celui des cases voisines
//                    // TODO Et si on prenait le type le plus représenté ? Ca éviterait peut-être que les
//                    //      mers soient trop éfilochées par la propagation aléatoire
//                    List<Case> casesVoisines = casesVoisines (cases, x, y, largeurCarte, hauteurCarte, null) ;
//                    if (! casesVoisines.isEmpty())
//                    {
//                        TypeCase typeCase = casesVoisines.get (genAlea.nextInt (casesVoisines.size())).type() ;
//                        nouvCases.add (new Case (x, y, typeCase, null, false)) ;
//                        modifieType = true ;
//                    }
//                }
//            }
//    
//            // Ajouter les nouvelles cases au terrain
//            for (Case nouvCase : nouvCases)
//                cases[nouvCase.x()][nouvCase.y()] = nouvCase ;
//
//        } while (modifieType) ;
//        
//        // (placer les villes)
//        int nbVillesAPlacer = (int) Math.ceil (proportionVilles * largeurCarte * hauteurCarte) ;
//        for (int i = 0 ; i < nbVillesAPlacer ; i++)
//        {
//            int x = genAlea.nextInt (largeurCarte) ;
//            int y = genAlea.nextInt (largeurCarte) ;
//            if (cases[x][y].type() == TypeCase.CASE_TERRE)
//                cases[x][y] = new Case (x, y, TypeCase.CASE_VILLE, null, false) ;
//        }
//
//        // Récupérer les mers dans la situation actuelle
//        Terrain terrainTmp = new Terrain(largeurCarte,hauteurCarte,cases) ;
//        List<Mer> mers = terrainTmp.mers() ;
//        
//        // Pour chaque mer, ajouter les ports nécessaires à la connexion de tous les territoires
//        // TODO Il faudrait intégrer ça aussi dans le terrain, pour pouvoir valider les cartes au besoin
//        // TODO Il faudrait éventuellement tester les cas extrêmes de cartes : que de la mer, ...
//        //          => En fait il faudrait refuser ces cartes (=> liste de critères pour que la carte soit
//        //             valide)
//        for (Mer mer : mers)
//        {
//            // Si la mer possède plus d'une côte, il faut vérifier que chaque côte contient un port au moins
//            if (mer.cotesParTerre().size() > 1)
//            {
//                for (List<Case> cote : mer.cotesParTerre())
//                {
//                    // Regarder s'il y a au moins un port
//                    boolean portTrouve = false ;
//                    for (Case caseCote : cote)
//                    {
//                        portTrouve = caseCote.type() == TypeCase.CASE_VILLE ;
//                        if (portTrouve)
//                            break ;
//                    }
//                    // S'il n'y a pas de port, une case au hasard devient un port
//                    // TODO En principe il faudrait revenir à la construction du terrain et tout
//                    //      reprendre, parce que le port posé peut servir à deux mers ; mais bon,
//                    //      cette solution risque juste d'ajouter quelques ports en plus dans
//                    //      des cas très rares, ce n'est pas grave
//                    if (!portTrouve)
//                    {
//                        Case casePort = cote.get (genAlea.nextInt (cote.size())) ;
//                        casePort = new Case (casePort.x(), casePort.y(), TypeCase.CASE_VILLE, null, false) ;
//                        cases[casePort.x()][casePort.y()] = casePort ;
//                    }
//                }
//            }
//        }
//        
//        
//        // Créer l'objet représentant le terrain
//        Terrain terrain = new Terrain (largeurCarte, hauteurCarte, cases) ;
//        
////        // Créer le terrain
////        Random genAlea = new Random (graineGenAlea) ;
////        int probaVilles =  1 ;
////        int probaMers   = 15 ;
////        for (int x = 0 ; x < largeurCarte ; x++)
////        for (int y = 0 ; y < hauteurCarte ; y++)
////        {
////            // (type de terrain)
////            TypeCase type ;
////            int nbAlea = genAlea.nextInt (100) ;
////            if (nbAlea < probaMers)
////                type = TypeCase.CASE_MER ;
////            else if (nbAlea < probaMers + probaVilles)
////                type = TypeCase.CASE_VILLE ;
////            else
////                type = TypeCase.CASE_TERRE ;
////            cases[x][y] = new Case (x, y, type, null, false) ;
////        }
////        Terrain terrain = new Terrain (largeurCarte, hauteurCarte, cases) ;
//        
////// ENCOURS A supprimer
////// Terrain visible pour tout le monde
////terrain.rendreVisiblePourTous (joueurs) ;
//        
//        // Attribuer les villes à des joueurs
//        // NOTE Il faudrait un autre système pour placer les villes ou alors une vérification, parce que
//        //      là on n'est pas strictement sûr qu'il y aura assez de villes
//        // (créer une liste des numéros des villes et les mélanger)
//        int nbVilles = terrain.nbVilles() ;
//        List<Integer> numsVilles = Listes.creerSequenceNombres (nbVilles) ;
//        Collections.shuffle (numsVilles) ;
//        // (attribuer les villes aux joueurs)
//        int NB_VILLES_INITIAL = 2 ;
//        List<Integer> numsVillesJoueur1 = numsVilles.subList (0,                 NB_VILLES_INITIAL) ;
//        List<Integer> numsVillesJoueur2 = numsVilles.subList (NB_VILLES_INITIAL, 2 * NB_VILLES_INITIAL) ;
//        
//        // Donner effectivement les villes aux joueurs
//        int cptVille = 0 ;
//        for (int x = 0 ; x < largeurCarte ; x++)
//        for (int y = 0 ; y < hauteurCarte ; y++)
//        {
//            if (cases[x][y].type() == TypeCase.CASE_VILLE)
//            {
//                cptVille++ ;
//                if (numsVillesJoueur1.contains (cptVille))
//                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (0)) ;
//                else if (numsVillesJoueur2.contains (cptVille))
//                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (1)) ;
//                else
//                    terrain.caseEn(x,y).choisirProprietaireInitial (joueurs.get (2)) ;
//            }
//        }
//        
//        // Etendre le terrain des joueurs autour de leurs villes
//        boolean modifieProprietaire ;
//        do
//        {
//            // Attribuer à quelqu'un les cases de terre sans propriétaire
//            modifieProprietaire = false ;
//            Map<Case,Joueur> attributions = new HashMap() ;
//            for (int x = 0 ; x < largeurCarte ; x++)
//            for (int y = 0 ; y < hauteurCarte ; y++)
//            {
//                Case caseCourante = terrain.caseEn (x, y) ;
//                if (caseCourante.type() == TypeCase.CASE_TERRE && caseCourante.proprietaire() == null)
//                {
//                    // Choisir un propriétaire parmi les voisins
//                    List<Joueur> voisins = terrain.proprietairesCasesVoisinesVivantes (x, y) ;
//                    if (! voisins.isEmpty())
//                    {
//                        Joueur nouvProprietaire = voisins.get (genAlea.nextInt (voisins.size())) ;
//                        attributions.put (caseCourante, nouvProprietaire) ;
//                        modifieProprietaire = true ;
//                    }
//                }
//            }
//            
//            // Appliquer les attributions
//            for (Entry<Case,Joueur> attribution : attributions.entrySet())
//            {
//                Case   caseCourante     = attribution.getKey() ;
//                Joueur nouvProprietaire = attribution.getValue() ;
//                caseCourante.choisirProprietaireInitial (nouvProprietaire) ;
//            }
//            
//        } while (modifieProprietaire) ;
//        
//        // Créer la partie
//        return new Partie (regles, "Partie générée aléatoirement", "", 100, 1, 10, true, joueurs, terrain) ;
//    }
    
    
    // Renvoie les cases voisines d'un case donnée
    // Filtre sur type case si non null
    private List<Case> casesVoisines (Case[][] cases, int x, int y, int largeurCarte, int hauteurCarte, TypeCase type)
    {
        List<Case> casesVoisines = new ArrayList() ;
        
        int xDeb = Math.max (x - 1, 0) ;
        int xFin = Math.min (x + 1, largeurCarte - 1) ;
        int yDeb = Math.max (y - 1, 0) ;
        int yFin = Math.min (y + 1 , hauteurCarte - 1) ;
        for (int xVoisines = xDeb ; xVoisines <= xFin ; xVoisines++)
        for (int yVoisines = yDeb ; yVoisines <= yFin ; yVoisines++)
        {
            if (xVoisines != x || yVoisines != y)
            {
                Case caseCourante = cases[xVoisines][yVoisines] ;
                if (caseCourante != null && (type == null || type == caseCourante.type()))
                    casesVoisines.add (caseCourante) ;
            }
        }
        return casesVoisines ;
    }
    
    
    // TODO Bon là ça n'a carrément plus rien à faire ici, mais il fallait mettre quelque part cette
    //      méthode (temporaire) de création du scénario "Front Russe"
    //          ! Scénario Front Russe de l'ancienne version de Pendulous (sans doute le même que la
    //            nouvelle version ?)
    public Partie genererScenarioFrontRusse ()
    {
        
        int largeurCarte = 21 ;
        int hauteurCarte = 28 ;
        Case[][] cases = new Case[largeurCarte][hauteurCarte] ;
        
        // Créer les joueurs
        List<Joueur> joueurs = new ArrayList() ;
        joueurs.add (new Joueur ("Allemagne", Color.RED,  true, 1.0,  50, 6, 0, 15, 0, 14, 27)) ;
        joueurs.add (new Joueur ("Russie",    Color.GRAY, true, 0.75, 50, 7, 0, 10, 0,  1, 20)) ;
        
        // Créer le terrain
        for (int x = 0 ; x < largeurCarte ; x++)
        for (int y = 0 ; y < hauteurCarte ; y++)
        {
            // (type de terrain)
            // NOTE J'aurais mieux fait de faire un tableau de chaînes représentant les types et de l'interpréteer ensuite...
            TypeCase type ;
            if ((y ==  0 && ((x >= 1 && x <= 9) || (x >= 19 && x <= 20))) ||
                (y ==  1 && ((x >= 2 && x <= 12) || (x >= 15 && x <= 16) || x == 20)) ||
                (y ==  2 && ((x >= 3 && x <= 6) || x == 8 || (x >= 12 && x <= 13) || (x >= 15 && x <= 16) || x == 18)) ||
                (y ==  3 && ((x >= 4 && x <= 6) || x == 10)) ||
                (y ==  4 && ((x >= 4 && x <= 5) || x == 10 || x == 18)) ||
                (y ==  5 && x == 13) ||
                (y ==  6 && x == 17) ||
                (y ==  7 && x == 17) ||
                (y == 20 && (x >= 3 && x <= 4)) ||
                (y == 21 && ((x >= 2 && x <= 4) || (x >= 6 && x <= 7))) ||
                (y == 22 && ((x >= 1 && x <= 3) || (x >= 7 && x <= 10))) ||
                (y == 23 && ((x >= 0 && x <= 3) || (x >= 8 && x <= 9))) ||
                (y == 24 && (x >= 0 && x <= 7)) ||
                (y == 25 && (x >= 0 && x <= 8)) ||
                (y == 26 && (x >= 0 && x <= 8)) ||
                (y == 27 && ((x >= 0 && x <= 9) || (x >= 17 && x <= 20))))
            {
                type = TypeCase.CASE_MER ;
            }
            else if (y ==  0 && x == 10 ||
                     y ==  2 && x == 14 ||
                     y ==  3 && x ==  0 ||
                     y ==  7 && x ==  3 ||
                     y ==  8 && x ==  8 ||
                     y == 10 && x == 15 ||
                     y == 14 && x ==  8 ||
                     y == 18 && x ==  0 ||
                     y == 19 && x ==  5 ||
                     y == 22 && x == 16 ||
                     y == 23 && x == 4)
            {
                type = TypeCase.CASE_VILLE ;
            }
            else
            {
                type = TypeCase.CASE_TERRE ;
            }
            cases[x][y] = new Case (x, y, type, null, false) ;
            
            // Attribuer la case à un joueur
            if (type != TypeCase.CASE_MER)
            {
                if (x >= 15 ||
                    x >= 8 && y >=  1 ||
                    x >= 7 && y >=  5 ||
                    x >= 6 && y >=  8 ||
                    x >= 5 && y >= 10 ||
                    x == 4 && (y == 12 || y == 22 || y == 23))
                {
                    cases[x][y].choisirProprietaireInitial (joueurs.get (1)) ;
                }
                else
                {
                    cases[x][y].choisirProprietaireInitial (joueurs.get (0)) ;
                }
            }
        }
        Terrain terrain = new Terrain (largeurCarte, hauteurCarte, cases) ;
        
        // Tout le terrain est visible par tous les joueurs
        terrain.rendreVisiblePourTous (joueurs) ;
//        for (int x = 0 ; x < largeurCarte ; x++)
//        for (int y = 0 ; y < hauteurCarte ; y++)
//        {
//            for (Joueur joueur : joueurs)
//                terrain.caseEn(x,y).decouvrir (joueur) ;
//        }
        
        // Créer la partie
        Regles regles = new Regles() ;
        Partie partie = new Partie (regles, "Front Russe", "", 15, 1, 10, false, joueurs, terrain) ;
        regles.associerPartie (partie) ;
        return partie ;
    }
    
}
