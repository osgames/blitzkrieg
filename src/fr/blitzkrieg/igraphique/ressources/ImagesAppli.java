/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.ressources;

import java.awt.Color ;
import java.awt.Image ;
import java.awt.image.BufferedImage ;
import java.io.IOException ;
import java.util.HashMap ;
import java.util.Map ;

import fr.blitzkrieg.utiles.igraphique.dessins.Images ;



// Accès aux images de l'application
// La classe s'occupe de rassembler les chemins contenant les images, de charger les images quand c'est
//   nécessaire, de gérer le cache, ...
public class ImagesAppli
{

    // Chemins
    // TODO Quelque chose de plus complexe pour gérer les thèmes
    //      (d'autant qu'il faudra aussi gérer des sons dans les mêmes répertoires)
    private static final String CHEM_REP_IMAGES = "donnees/themes/defaut/" ;
    
    // Noms des fichiers
    public static final String IMG_DRAPEAU   = "drapeau.png" ;
    public static final String IMG_TROUPE    = "troupe.png" ;
    public static final String IMG_VILLE     = "ville.png" ;
    public static final String IMG_EXPLOSION = "explosion.png" ;
    public static final String IMG_COTE      = "cote.png" ;
    
    // Cache des images
    private static Map<String,Image>                  cacheImages         = new HashMap() ;
    private static Map<CleImageColoree,BufferedImage> cacheImagesColorees = new HashMap() ;
    
    
    
    // Renvoie l'image indiquée en paramètre
    // En cas d'erreur, lève une exception non déclarée, pour éviter de traiter ces exceptions dans les
    //   panneaux qui les affichent.
    // TODO Prévoir une image de remplacement pour les images qui n'ont pas pu être chargées
    // TODO Si on se met à utiliser cette méthode depuis l'extérieur, attention : il n'y a pas de cache
    //      dessus (il faudrait le faire, bien sûr)
    public synchronized static Image charger (String nomImage)
    {
        Image image ;
        
        
        // Chercher l'image dans le cache
        image = cacheImages.get (nomImage) ;
        if (image != null)
            return image ;
        
        // Si elle ne se trouve pas dans le cache, charger l'image depuis le fichier
        try
        {
            image = Images.charger (CHEM_REP_IMAGES + nomImage) ;
        }
        catch (IOException e)
        {
            e.printStackTrace() ;
            throw new RuntimeException ("Erreur lors du chargement de l'image \"" + nomImage + "\"", e) ;
        }
        
        // Ajouter l'image au cache
        cacheImages.put (nomImage, image) ;

        // Renvoyer l'image
        return image ;
    }
    
    
    // Renvoie l'image indiquée en paramètre et le peint de la couleur demandée.
    // Tous les points rouges sont peint de la couleur demandée si une couleur est fournie.
    // TODO Revoir ici aussi la gestion des exceptions
    public synchronized static BufferedImage charger (String nomImage, Color couleur)
    {
        CleImageColoree cleImage ;
        BufferedImage   image ;
        
        
        // Chercher l'image dans le cache
        cleImage = new CleImageColoree (nomImage, couleur) ;
        image = cacheImagesColorees.get (cleImage) ;
        if (image != null)
            return image ;

        // Charger une copie de l'image
        image = Images.copie (Images.imageTampon (charger (nomImage))) ;
        
        // Peindre l'image de la bonne couleur
        if (couleur != null)
        {
            for (int y = 0 ; y < image.getHeight() ; y++)
            for (int x = 0 ; x < image.getWidth() ; x++)
            {
                Color coul = new Color (image.getRGB (x, y), true) ;
//                if (coul.getAlpha() != 0 && ! coul.equals (Color.BLACK))
                if (coul.equals (Color.RED))
                    image.setRGB (x, y, couleur.getRGB()) ;
            }
        }
        
        // Ajouter l'image au cache
        cacheImagesColorees.put (cleImage, image) ;
        
        // Renvoyer l'image
        return image ;
    }
    
}


// Clé représentant une image
// TODO Il faudra penser à ajouter le thème
//          => Ben là le problème se posera sans doute aussi pour le chargement des images et il faudra
//             une clé pour ça
class CleImageColoree
{
    private String nomImage  ;
    private Color  couleur ;
    
    
    // Constructeur
    public CleImageColoree (String nomImage, Color couleur)
    {
        this.nomImage = nomImage ;
        this.couleur  = couleur ;
    }
    
    // Egalité
    public boolean equals (Object o)
    {
        // Cas particuliers
        if (o == null)
            return false ;
        if (o == this)
            return true ;
        if (o.getClass() != this.getClass())
            return false ;
        
        // Comparer les champs
        CleImageColoree c1 = (CleImageColoree) o ;
        CleImageColoree c2 = this ;
        return c1.nomImage.equals (c2.nomImage) &&
               c1.couleur.equals  (c2.couleur) ;
    }
    
    // Code de hachage
    public int hashCode ()
    {
        // NOTE Je ne sais pas si c'est terrible... m'enfin bon, l'important c'est de ne pas charger les
        //      images depuis le fichier à chaque fois...
        return this.nomImage.hashCode() ^ this.couleur.hashCode() ;
    }
    
}
