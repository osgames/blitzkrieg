/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.Color ;
import java.awt.Dimension ;
import java.awt.Graphics ;
import java.awt.Image ;
import java.awt.Rectangle ;
import java.awt.event.ActionEvent ;
import java.awt.event.ActionListener ;
import java.awt.image.BufferedImage ;
import java.util.HashMap ;
import java.util.Map ;
import java.util.Observable ;
import java.util.Observer ;

import javax.swing.JPanel ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Mer ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;
import fr.blitzkrieg.igraphique.actions.ActionClicTerrain ;
import fr.blitzkrieg.igraphique.animations.Animation ;
import fr.blitzkrieg.igraphique.ressources.ImagesAppli ;
import fr.blitzkrieg.utiles.igraphique.dessins.Images ;



// Panneau affichant la carte principale
// TODO Il faudrait mettre les ascenseurs dans cette classe et un panneau interne pour le dessin ; sinon
//      quand la partie se termine on met à jour seulement le panneau de dessin et sa taille diminue
//      puisqu'il n'y a plus rien à afficher, mais les ascenseurs restent présents parce que le composant
//      de défilement n'a pas été mis à jour ; donc il faudrait tout intégrer dans ce panneau, qui
//      gèrerait tout d'un coup et dessinerait dans un panneau interne
public class PanTerrain extends JPanel implements Observer, ActionListener
{

    // Constantes
    // TODO Rassembler tout ça ailleurs, au niveau de l'appli
    public static final Color COUL_NON_DECOUVERTE = Color.BLACK ;           // Case non découverte
    public static final Color COUL_MERS           = Color.BLUE ;
    public static final Color COUL_TERRES         = Color.GREEN ;
    public static final Color COUL_FOND_CARTE     = Color.DARK_GRAY ;
    public static final int   TAILLE_CASE         = 32 ;            // Taille d'une case  NOTE Attention : ça doit correspondre à la taille des images (un avertissement sinon ? notamment pour les thèmes supplémentaires) [ben en attendant c'est public]
    
    
    private Partie          partie ;                // Partie à afficher
    
    private Map<CleImgCase,BufferedImage> cacheCases = new HashMap() ;      // Dessins des cases
    private BufferedImage                 cacheImgExplosion ;               // Dessin de l'explosion (animations)
    


    // Constructeur
    public PanTerrain ()
    {
        // Ajouter l'écouteur de clics
        this.addMouseListener (new ActionClicTerrain()) ;
    }
    
    
    // Indique au panneau la partie à représenter
    public synchronized void modifPartieAAfficher (Partie partie)
    {
        // Regarder la nouvelle partie
        // CODE Code à arranger
        if (this.partie != null)
            this.partie.deleteObserver (this) ;
        this.partie = partie ;
        if (this.partie != null)
            this.partie.addObserver (this) ;
        
        // Adapter la taille du panneau à la taille du terrain à représenter
        int largeurTerrain = (this.partie == null ? 0 : partie.terrain().largeur()) ;
        int hauteurTerrain = (this.partie == null ? 0 : partie.terrain().hauteur()) ;
        this.setMinimumSize   (new Dimension (largeurTerrain * TAILLE_CASE, hauteurTerrain * TAILLE_CASE)) ;
        this.setPreferredSize (this.getMinimumSize()) ;
        
        // Afficher les nouvelles informations
        mettreAJourDessinTerrain() ;
    }
    
    
    // Met à jour l'affichage du terrain
    private void mettreAJourDessinTerrain ()
    {
        // Demander le réaffichage du panneau
        this.repaint() ;
    }
    
    
    // Traite les événements "action"
    public void actionPerformed (ActionEvent evt)
    {
        // (pour l'instant, il s'agit forcément d'une animation)
        // Arrêter les animations terminées (pas attendre la méthode de dessin, pour être sûr que le chrono
        //   de ces animations ne va pas continuer à fournir des tics parce que le fil de dessin est en
        //   retard [sinon ça génèrerait encore des dessins supplémentaires pour rien])
        Blitzkrieg.instance().animations().arreterAnimationsTerminees() ;
        
        // Demander le réaffichage de la vue
        this.repaint() ;
    }
    
    
    // Afffiche la carte
    // TODO Revoir les couleurs
    // TODO Trouver une image de fond plutôt que peindre uniformément ?
    // CODE Arranger le code
// ENCOURS Essai de ne pas recréer le tampon image à chaque fois parce que c'est très lent
//          => Non, ce n'était pas ça qui était très lent, mais c'était très gênant quand même
//          => Il faudrait quand même vérifier qu'il a la bonne taille (ça change quand on crée un)
//          => Calculer sa taille en mémoire : à cause du temps de gestion de tout ça et de la taille mémoire,
//             il faudra sans doute gérer le défilement à la main et ne tracer qu'une partie du panneau
//             (sur un tampon plus petit)
// TODO Vérifier que ces accès à la partie un peu partout dans le programme ne nécessitent pas plus de
//      synchronisation ; sans doute, quand la partie est modifiée alors qu'on est en train de réafficher
//      quelque chose ; l'état affiché peut être incohérent ou l'affichage pourrait même planter... Il
//      faudrait un ticket pour afficher la partie (utilisé dans l'affichage) ou la modifier (utilisé
//      dans les règles), pour être sûr.
// PERF Vérifier qu'on ne redessine que la partie "salie" du panneau d'affichage et pas tout la partie
//      affichée
private BufferedImage tampon = null ;
    protected synchronized void paintComponent (Graphics g)
    {
        Blitzkrieg application ;
        
        
        // Initialisations
        application = Blitzkrieg.instance ();
        
        
long dateDeb = System.currentTimeMillis() ;
        // Peindre l'image hors écran
// CODE Code à arranger  (notamment le rangement de la variable)
if (this.tampon == null || this.tampon.getWidth() != this.getWidth() || this.tampon.getHeight() != this.getHeight())
    this.tampon = new BufferedImage (this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB) ;
//        BufferedImage image = new BufferedImage (this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB) ;
BufferedImage image = this.tampon ;
        Graphics gImg = image.getGraphics() ;
long dateFinCreationImage = System.currentTimeMillis() ;
        // Peindre le fond de la carte
        gImg.setColor (COUL_FOND_CARTE) ;
//        gImg.fillRect (0, 0, this.getWidth(), this.getHeight()) ;
Rectangle r = g.getClipBounds() ;       // CODE A renommer


// Tracer la grille
long dateDebCreationImgCase = System.currentTimeMillis() ;
long dateFinCreationImgCase = System.currentTimeMillis() ;
long dateDebCalculsFiltrage = System.currentTimeMillis() ;
long dateFinCalculsFiltrage = System.currentTimeMillis() ;
long dureeDessinCases = 0 ;
        if (this.partie == null || ! this.partie.commencee())
        {
            // TODO Il faut aussi faire ça quand la taille du rectangle à tracer est inférieure à la
            //      taille du panneau (pour de petits terrains et de grands écrans)
            gImg.fillRect ((int)r.getMinX(), (int)r.getMinY(), (int)r.getWidth(), (int)r.getHeight()) ;
        }
        else if (this.partie.observationSuspendue())
        {
            gImg.setColor (COUL_NON_DECOUVERTE) ;
            gImg.fillRect ((int)r.getMinX(), (int)r.getMinY(), (int)r.getWidth(), (int)r.getHeight()) ;
        }
        else
        {
dateDebCreationImgCase = System.currentTimeMillis() ;
            // PERF Regarder la mémoire consommée un peu partout (tracer avec un profileur), ça évite
            //             d'avoir besoin de plus de mémoire pour faire fonctionner le jeu et et devoir
            //             récupérer la mémoire régulièrement
dateFinCreationImgCase = System.currentTimeMillis() ;
           
            Terrain terrain = this.partie.terrain() ;
            int largeurTerrain = terrain.largeur() ;
            int hauteurTerrain = terrain.hauteur() ;
dateDebCalculsFiltrage = System.currentTimeMillis() ;
double xMin = r.getMinX() ;
double yMin = r.getMinY() ;
double xMax = r.getMaxX() ;
double yMax = r.getMaxY() ;
int xCaseDeb = Math.max (0,                 (int) Math.floor (xMin / TAILLE_CASE) - 1) ;
int yCaseDeb = Math.max (0,                 (int) Math.floor (yMin / TAILLE_CASE) - 1) ;
int xCaseFin = Math.min (largeurTerrain -1, (int) Math.floor (xMax / TAILLE_CASE) + 1) ;
int yCaseFin = Math.min (hauteurTerrain -1, (int) Math.floor (yMax / TAILLE_CASE) + 1) ;
dateFinCalculsFiltrage = System.currentTimeMillis() ;
            for (int x = xCaseDeb ; x <= xCaseFin ; x++)
            for (int y = yCaseDeb ; y <= yCaseFin ; y++)
            {
long dateDebDessinCases     = System.currentTimeMillis() ;
                // Informations sur la case
                // (initialisations)
                Mer merBordantLaCase = terrain.merBordant (x, y) ;
                // (récupération des informations)
                Joueur joueurCourant = this.partie.joueurCourant() ;
                Case   caseCourante  = terrain.caseEn (x, y) ;
                boolean  decouverte     = caseCourante.decouverte (joueurCourant) ;
                // TODO A voir : est-ce qu'on ne peut pas se baser sur la méthode des règles qui indique
                //      si une attaque navale est possible ici ?
                boolean  accesParMer    = this.partie.invasionsNavalesPermises() && merBordantLaCase != null && merBordantLaCase.portAppartenantA (joueurCourant) ;
                TypeCase typeCase       = caseCourante.type() ;
                Joueur   proprietaire   = caseCourante.proprietaire() ;
                boolean  troupePresente = caseCourante.troupePresente() ;
                boolean  caseVivante    = caseCourante.estVivante() ;
                
                // Chercher la première animation sur la case
                Animation animationCase = null ;
                for (Animation anim : application.animations().animationsEnCours())
                {
                    if (anim.x == x && anim.y == y && anim.estEnCours())
                    {
                        animationCase = anim ;
                        break ;
                    }
                }

                // Déterminer le type de case à peindre
                BufferedImage imgCase ;
                // (animation en phase explosion)
                if (animationCase != null && animationCase.estEnPhaseExplosion())
                    imgCase = imageExplosion() ;
                // (animation en phase dessin : dessiner la case avant l'assaut)
                else if (animationCase != null && ! animationCase.estEnPhaseExplosion())
                    imgCase = imageCase (decouverte, accesParMer, typeCase, animationCase.proprietaire, animationCase.troupePresente, animationCase.caseVivante) ;
                // (pas d'animation)
                else
                    imgCase = imageCase (decouverte, accesParMer, typeCase, proprietaire, troupePresente, caseVivante) ;
                    
long dateFinDessinCases = System.currentTimeMillis() ;
dureeDessinCases += dateFinDessinCases - dateDebDessinCases ;
                
                // Peindre la case sur le panneau
int xDebCase = x * TAILLE_CASE ;
int yDebCase = y * TAILLE_CASE ;
                gImg.drawImage (imgCase, xDebCase, yDebCase, this) ;
            }
        }
        
//        // Supprimer les animations qui sont terminées
//        for (Animation anim : application.animations().animationsEnCours())
//        {
//            if (! anime.estE)
//        }
//        Iterator<Animation> itAnim = this.animationsEnCours.iterator() ;
//        while (itAnim.hasNext())
//        {
//            Animation animation = itAnim.next() ;
//            if (! animation.estEnCours())
//                itAnim.remove() ;
//        }

        // Peindre l'image sur le panneau
        g.drawImage (image, 0, 0, this) ;
// PERF A utiliser pour surveiller/améliorer les performances, mais en attendant ça gêne la lecture des
//      traces de l'IA
long dateFinDessin = System.currentTimeMillis() ;
//if (dateFinDessin - dateDeb > 100)
//{
//System.out.println ("Dessin terminé en " + (dateFinDessin - dateDeb) + "ms") ;
//System.out.println ("  Dont le dessin lui-même : " + (dateFinDessin - dateFinCreationImage) + "ms") ;
//System.out.println ("    Dont la création de l'image sur laquelle dessiner : " + (dateFinCreationImgCase - dateDebCreationImgCase) + "ms") ;
//System.out.println ("    Dont les calculs de filtrage : " + (dateFinCalculsFiltrage - dateDebCalculsFiltrage) + "ms") ;
//System.out.println ("    Dont le dessin des cases : " + dureeDessinCases + "ms") ;
//}
    }
    
    
    // Mise à jour du panneau quand la partie observée est modifiée
    public void update (Observable o, Object arg)
    {
//        if (this.partie.terminee())
//            modifPartieAAfficher (null) ;
//        else
//            mettreAJourDessinTerrain() ;
        
        // (le panneau peut continuer à afficher une partie terminée tant qu'on ne lui a pas dit d'arrêter)
        mettreAJourDessinTerrain() ;
    }
    
    
    // Renvoie la case correspondant aux critères en paramètres
    // PERF Calculer l'accès par mer ça doit être un peu long pour retracer l'image à chaque fois... ; voir
    //      comment on peut faire sans oublier de refaire le calcul quand c'est nécessaire (et vérifier si
    //      c'est réellement ça qui prend du temps)
    private BufferedImage imageCase (boolean decouverte, boolean accesParMer, TypeCase typeCase, Joueur proprietaire, boolean troupePresente, boolean caseVivante)
    {
        CleImgCase    cle ;             // Clé associée à l'image
        BufferedImage imgCase ;         // Image à renvoyer
        
        
        // Construire la clé
        Color coulProprietaire = (proprietaire == null ? null : proprietaire.couleur()) ;
        cle = new CleImgCase (decouverte, accesParMer, typeCase, coulProprietaire, troupePresente, caseVivante) ;
        
        // Récupérer l'image dans le cache
        imgCase = this.cacheCases.get (cle) ;
        
        // Créer l'image si elle ne se trouve pas dans le cache
        if (imgCase == null)
        {
            // Créer l'image
            imgCase = new BufferedImage (TAILLE_CASE, TAILLE_CASE, BufferedImage.TYPE_INT_ARGB) ;
            Graphics gImgCase = imgCase.getGraphics() ;
            
            // Si la case n'est pas visible pour le joueur courant, elle est masquée
            if (!decouverte)
            {
                // Remplir la case
                gImgCase.setColor (COUL_NON_DECOUVERTE) ;
                gImgCase.fillRect (0, 0, TAILLE_CASE, TAILLE_CASE) ;
                
                // Marqueur de côte si besoin
                // TODO Il ne faudrait pas l'indiquer si les attaques navales ne sont pas autorisées, si ?
                //          => Dans ce cas renommer le paramètre et calculer sa valeur correcte en dehors
                //             de cette fonction
                if (accesParMer)
                {
                    Image imgCote = ImagesAppli.charger (ImagesAppli.IMG_COTE, proprietaire.couleur()) ;
                    gImgCase.drawImage (imgCote, 0, 0, this) ;
                }
            }
            else
            {
                // Remplir la case
                gImgCase.setColor (typeCase == TypeCase.CASE_MER ? COUL_MERS : COUL_TERRES) ;
                gImgCase.fillRect (0, 0, TAILLE_CASE, TAILLE_CASE) ;

                // Si la case appartient à quelqu'un, mettre sa couleur dessus
                if (proprietaire != null)
                {
                    // S'il y a une ville, dessiner la ville
                    if (typeCase == TypeCase.CASE_VILLE)
                    {
                        Image imgVille = ImagesAppli.charger (ImagesAppli.IMG_VILLE, proprietaire.couleur()) ;
                        gImgCase.drawImage (imgVille, 0, 0, this) ;
                    }
                    // S'il y a une troupe, dessiner la troupe
                    else if (troupePresente)
                    {
                        Image imgTroupe = ImagesAppli.charger (ImagesAppli.IMG_TROUPE, proprietaire.couleur()) ;
                        gImgCase.drawImage (imgTroupe, 0, 0, this) ;
                    }
                    // Sinon simplement afficher le marqueur de propriété
                    else
                    {
                        // TODO A ajuster tout ça
                        int xCentre = TAILLE_CASE / 2 ;
                        int yCentre = TAILLE_CASE / 2 ;
                        gImgCase.setColor (proprietaire.couleur()) ;
                        gImgCase.fillRect (xCentre-1, yCentre-1, 2, 2) ;
                    }
                }
            
                // Si la case est morte, modifier les couleurs
                if (! caseVivante)
                    Images.assombrie (imgCase, 0.5) ;
            }
            
            // Stocker l'image dans le cache
            this.cacheCases.put (cle, imgCase) ;
        }
        
        // Renvoyer l'image
        return imgCase ;
    }
    
    
    // Renvoie l'image de l'explosion pour les animations
    // TODO Système pas très développé pour gérer les animations, mais suffisant pour l'instant
    //          => A voir ; par exemple ça va tant qu'on ne veut pas dessiner la ville sous l'explosion
    private BufferedImage imageExplosion ()
    {
        BufferedImage imgCase ;
        
        
        // Regarder si l'image est dans le cache
        if (this.cacheImgExplosion != null)
        {
            imgCase = this.cacheImgExplosion ;
        }
        else
        {
            // Créer l'image
            imgCase = new BufferedImage (TAILLE_CASE, TAILLE_CASE, BufferedImage.TYPE_INT_ARGB) ;
            Graphics gImgCase = imgCase.getGraphics() ;
    
            // Peindre le fond de l'image
            gImgCase.setColor (COUL_TERRES) ;
            gImgCase.fillRect (0, 0, TAILLE_CASE, TAILLE_CASE) ;
    
            // Ajouter l'image de l'explosion
            Image imgExplosion = ImagesAppli.charger (ImagesAppli.IMG_EXPLOSION) ;
            gImgCase.drawImage (imgExplosion, 0, 0, this) ;
    
            // Ajouter l'image au cache
            this.cacheImgExplosion = imgCase ;
        }
        
        // Renvoyer l'image
        return imgCase ;
    }
    
}



// Clé identifiant une image de case
class CleImgCase
{
    private boolean  decouverte ;
    private boolean  accesParMer ;          // Case accessible par la mer ?
    private TypeCase typeCase ;
    private Color    coulProprietaire ;
    private boolean  troupePresente ;
    private boolean  caseVivante ;
    
    
    
    // Constructeur
    public CleImgCase (boolean decouverte, boolean cote, TypeCase typeCase, Color coulProprietaire, boolean troupePresente, boolean caseVivante)
    {
        this.decouverte       = decouverte ;
        this.accesParMer      = cote ;
        this.typeCase         = typeCase ;
        this.coulProprietaire = coulProprietaire ;
        this.troupePresente   = troupePresente ;
        this.caseVivante      = caseVivante ;
    }
    
    
    // Egalité
    public boolean equals (Object o)
    {
        if (o == this)
            return true ;
        if (o == null)
            return false ;
        if (o.getClass() != this.getClass())
            return false ;
        
        // Comparer les attributs
        CleImgCase c1 = this ;
        CleImgCase c2 = (CleImgCase) o ;
        return c1.decouverte     == c2.decouverte &&
               c1.accesParMer    == c2.accesParMer &&
               c1.troupePresente == c2.troupePresente &&
               c1.caseVivante    == c2.caseVivante &&
               c1.typeCase       == c2.typeCase   &&
               (c1.coulProprietaire == null && c2.coulProprietaire == null ||
                c1.coulProprietaire != null && c1.coulProprietaire.equals (c2.coulProprietaire)) ;
    }
    
    
    // Code de hachage
    public int hashCode ()
    {
        // PERF Pas génial mais rapide à calculer (enfin j'espère)
        //          => A vérifier, si ça se trouve ça irait plus vite de comparer les cases une par
        //             une dans une liste ; ou alors il faudrait faire un tableau à 5 dimensions
        //             contenant les images (et convertir les valeurs en coordonnées, ce qui n'ira
        //             sans doute pas tellement plus vite que faire la recherche dans la liste)
        // TODO A bien tester si on le garde
        int code = 0 ;
        code = (this.decouverte     ? (code << 1) | 0x1 : (code << 1)) ;
        code = (this.accesParMer    ? (code << 1) | 0x1 : (code << 1)) ;
        code = (this.troupePresente ? (code << 1) | 0x1 : (code << 1)) ;
        code = (this.caseVivante    ? (code << 1) | 0x1 : (code << 1)) ;
        code ^= this.typeCase.hashCode() ;
        if (this.coulProprietaire != null)
            code ^= this.coulProprietaire.hashCode() ;
        
        // Renvoyer le code calculé
        return code ;
    }
    
}
