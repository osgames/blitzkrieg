/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.Color ;
import java.awt.Dimension ;
import java.awt.event.WindowEvent ;
import java.awt.event.WindowListener ;

import javax.swing.BoxLayout ;
import javax.swing.JButton ;
import javax.swing.JFileChooser ;
import javax.swing.JFrame ;
import javax.swing.JMenu ;
import javax.swing.JMenuBar ;
import javax.swing.JMenuItem ;
import javax.swing.JPanel ;
import javax.swing.JScrollPane ;
import javax.swing.border.EtchedBorder ;

import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.igraphique.actions.ActionAfficherInfosCombats ;
import fr.blitzkrieg.igraphique.actions.ActionAfficherInfosJoueurs ;
import fr.blitzkrieg.igraphique.actions.ActionAfficherInfosPartie ;
import fr.blitzkrieg.igraphique.actions.ActionChargerPartie ;
import fr.blitzkrieg.igraphique.actions.ActionCreerPartie ;
import fr.blitzkrieg.igraphique.actions.ActionFinPartie ;
import fr.blitzkrieg.igraphique.actions.ActionFinTourJoueur ;
import fr.blitzkrieg.igraphique.actions.ActionQuitterApplication ;
import fr.blitzkrieg.igraphique.actions.ActionSauvegarderPartie ;
import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;
import fr.blitzkrieg.utiles.igraphique.swing.gestplacement.PlaceurListe ;



// Fenêtre principale
// TODO Limiter la mémoire de la JVM à juste ce qui est nécessaire, après tests : le jeu devrait pouvoir
//      fonctionner sur une machine lente, pas besoin de beaucoup de puissance de calcul (enfin ça dépend
//      de l'IA...), donc si ça pouvait ne pas consommer des masses énormes de mémoire ce serait bien. Et
//      d'ailleurs c'est toujours bien si ça ne goinfre pas de la mémoire comme un cochon (! les options de
//      la JVM risquent d'être spécifiques à la JVM de Sun) ; attention à la consommation mémoire de l'IA
//      également ; on peut peut-être faire ça plus simplement en appelant régulièrement le collecteur de
//      mémoire pour éviter que trop de mémoire ne soit utilisée alors que ce n'est pas nécessaire (la JVM
//      n'alloue pas énormément de mémoire au début)
// DOC Faire une mini doc de conception
public class FenPrincipale extends JFrame implements WindowListener
{

    // Composants graphiques de la fenêtre
    private JMenuBar               bm_menus ;
    private PanInfosJoueur         p_infosJoueur ;
    private PanInfosAttaqueDefense p_infosAttDef ;
    private PanMiniCarte           p_miniCarte ;
    private PanTerrain             p_terrain ;
    
    // Autres objets graphiques
    private DialPoints        dialPoints ;          // Boîte de dialogue affichant les points de victoire des joueurs
    private DialInfosJoueurs  dialInfosJoueurs ;    // Boîte de dialogue affichant les informations sur les joueurs
    private DialInfosPartie   dialInfosPartie ;     // Boîte de dialogue affichant les informations sur la partie
    private DialInfosCombats  dialInfosCombats ;    // Boîte de dialogue affichant les statistiques sur les combats
    private DialChoixScenario dialScenarios ;       // Boîte de dialogue permettant de choisir un scénario
//    private FenMiniCarte      fenMiniCarte ;        // Fenêtre affichant la mini-carte
    private JFileChooser      selectFichier ;       // Sélecteur de fichier
    
    
    
    // Constructeur
    public FenPrincipale ()
    {
        super ("Blitzkrieg") ;
        
        // Menus
        // TODO Touches de raccourci pour les menus et leurs éléments
        // TODO Une fois les couleurs choisies, intégrer la barre de menus aux couleurs du reste de la fenêtre
        // TODO Menu "A propos"
        this.bm_menus = new JMenuBar() ;
        this.setJMenuBar (this.bm_menus) ;
        JMenu menuFichier = new JMenu ("Fichier") ;
        this.bm_menus.add (menuFichier) ;
            menuFichier.add (new JMenuItem (new ActionCreerPartie())) ;
            menuFichier.add (new JMenuItem (new ActionChargerPartie())) ;
            menuFichier.add (new JMenuItem (new ActionSauvegarderPartie())) ;
            menuFichier.add (new JMenuItem (new ActionFinPartie())) ;       // TODO Renommer les Fins* en Terminer* ?
            menuFichier.add (new JMenuItem (new ActionQuitterApplication())) ;
        JMenu menuTour = new JMenu ("Tour") ;
        this.bm_menus.add (menuTour) ;
            menuTour.add    (new JMenuItem (new ActionFinTourJoueur())) ;
        JMenu menuRapports = new JMenu ("Rapports") ;
        this.bm_menus.add (menuRapports) ;
            menuRapports.add (new JMenuItem (new ActionAfficherInfosJoueurs())) ;
            menuRapports.add (new JMenuItem (new ActionAfficherInfosPartie())) ;
            menuRapports.add (new JMenuItem (new ActionAfficherInfosCombats())) ;
        
        // Panneau d'information sur le joueur courant
        this.p_infosJoueur = new PanInfosJoueur() ;
        // TODO Taille à déterminer, suivant ce qu'il y aura à afficher
        this.p_infosJoueur.setPreferredSize (new Dimension (200, 200)) ;
        this.p_infosJoueur.setMinimumSize   (this.p_infosJoueur.getPreferredSize()) ;
        this.p_infosJoueur.setMaximumSize   (this.p_infosJoueur.getPreferredSize()) ;
        
        // Panneau d'information sur les attaques/défenses
        this.p_infosAttDef = new PanInfosAttaqueDefense() ;
        // TODO Taille à déterminer, en fonction du panneau précédent
        this.p_infosAttDef.setPreferredSize (new Dimension (200, 200)) ;
        this.p_infosAttDef.setMinimumSize   (this.p_infosAttDef.getPreferredSize()) ;
        this.p_infosAttDef.setMaximumSize   (this.p_infosAttDef.getPreferredSize()) ;
        
        // Panneau de la mini-carte
        // TODO Est-ce qu'on pourrait dimensionner la taille des "cases" sur la mini-carte en fonction
        //      de la taille ? S'il y a assez de place pour qu'elles soient plus grosses, les agrandir.
        //          => A voir, surtout pour donner la taille initiale des cases (pour une nouvelle
        //             partie), après une fonction de loupe +/- doit permettre d'augmenter/diminuer la
        //             taille des cases, les barres de défilement apparaissant si besoin
        this.p_miniCarte = new PanMiniCarte() ;
//        JScrollPane defil_minicarte = new JScrollPane (this.p_miniCarte) ;
        
        // Panneau affichant la carte principale 
        this.p_terrain = new PanTerrain() ;
        JScrollPane sp_terrain = new JScrollPane (this.p_terrain) ;
// TODO Est-ce qu'on veut l'afficher cette bordure (par défaut elle s'affiche) ?
sp_terrain.setBorder (null) ;
        this.p_terrain.addMouseListener       (this.p_infosAttDef) ;
        this.p_terrain.addMouseMotionListener (this.p_infosAttDef) ;
        
        // Assemblage de la fenêtre
        JPanel p_gauche = new JPanel (new PlaceurListe (PlaceurListe.COLONNE)) ;
        p_gauche.add (this.p_infosJoueur) ;
        p_gauche.add (this.p_infosAttDef) ;
        p_gauche.add (this.p_miniCarte) ;
        p_gauche.setBackground (Color.DARK_GRAY) ;
        p_gauche.setBorder (new EtchedBorder()) ;
        this.setLayout (new BorderLayout()) ;
        this.getContentPane().add (p_gauche,   BorderLayout.WEST) ;
        this.getContentPane().add (sp_terrain, BorderLayout.CENTER) ;
// TODO A supprimer : pour les tests de l'interface
JPanel p_bas = new JPanel() ;
p_bas.setLayout (new BoxLayout (p_bas, BoxLayout.Y_AXIS)) ;
p_bas.add (new JButton (new ActionFinTourJoueur())) ;
//p_bas.add (new JButton (new ActionFinPartie())) ;
//p_bas.add (new JButton (new ActionSauvegarderPartie())) ;
//p_bas.add (new JButton (new ActionChargerPartie())) ;
this.getContentPane().add (p_bas, BorderLayout.SOUTH) ;
        // Paramétrage de la fenêtre
//        this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE) ;        // NOTE Géré à la main
        this.setDefaultCloseOperation (JFrame.DO_NOTHING_ON_CLOSE) ;
        this.addWindowListener (this) ;
        
        // Afficher la fenêtre
        this.setSize (600, 400) ;       // TODO Choisir une taille pour la fenêtre (en fonction de la résolution de l'écran ?)
        Fenetres.centrer (this) ;
        this.setVisible (true) ;
        
//this.fenMiniCarte().setVisible (true) ;
    }
    
    
    // Initialise les objets graphiques pour une nouvelle partie
    public void initialiserObjetsGraphiquesPartie (Partie partie)
    {
        // Composants de la fenêtre
        this.p_infosJoueur.modifPartieAAfficher (partie) ;
        this.p_infosAttDef.modifPartieAAfficher (partie) ;
        this.p_terrain.modifPartieAAfficher     (partie) ;
        this.p_miniCarte.modifPartieAAfficher   (partie) ;
        
        // Autres objets graphiques
        this.dialPoints       = new DialPoints       (this, partie) ;
        this.dialInfosJoueurs = new DialInfosJoueurs (this, partie) ;
        this.dialInfosPartie  = new DialInfosPartie  (this, partie) ;
        this.dialInfosCombats = new DialInfosCombats (this, partie) ;
//        this.fenMiniCarte.modifPartieAAfficher (partie) ;

        
        // Demander le réaffichage des objets
        // (note : notamment le redimensionnement du panneau affichant le terrain, pas bien pris en compte)
        this.p_terrain.updateUI() ;
    }
    
    
    // Finalise les objets graphiques lorsqu'une partie est terminée
    public void finaliserObjetsGraphiquesPartie ()
    {
        // Composants de la fenêtre
        // TODO Vérifier que sans ça les composants continent à afficher la partie
        this.p_infosJoueur.modifPartieAAfficher (null) ;
        this.p_infosAttDef.modifPartieAAfficher (null) ;
        this.p_terrain.modifPartieAAfficher     (null) ;
        this.p_miniCarte.modifPartieAAfficher   (null) ;
        
        // Autres objets graphiques
        this.dialPoints       = null ;
        this.dialInfosJoueurs = null ;
        this.dialInfosPartie  = null ;
        this.dialInfosCombats = null ;
//        this.fenMiniCarte.modifPartieAAfficher (null) ;
    }
    
    
    // Renovie le panneau dessinant le terrain
    public PanTerrain panTerrain ()
    {
        return this.p_terrain ;
    }
    
    
    // Renvoie la boîte de dialogue d'affichage des points de victoire
    public DialPoints dialPoints ()
    {
        return this.dialPoints ;
    }
    
    // Renvoie la boîte de dialogue d'affichage des informations sur les joueurs
    public DialInfosJoueurs dialInfosJoueurs ()
    {
        return this.dialInfosJoueurs ;
    }
    
    // Renvoie la boîte de dialogue d'affichage des informations sur la partie
    public DialInfosPartie dialInfosPartie ()
    {
        return this.dialInfosPartie ;
    }
    
    // Renvoie la boîte de dialogue d'affichage des statistiques sur les combats
    public DialInfosCombats dialInfosCombats ()
    {
        return this.dialInfosCombats ;
    }
    
    // Renvoie la boîte de dialogue de choix d'un scénario
    public DialChoixScenario dialChoixScenario ()
    {
        if (this.dialScenarios == null)
            this.dialScenarios = new DialChoixScenario (this) ;
        return this.dialScenarios ;
    }
    
//    // Renvoie la fenêtre de la mini-carte
//    public FenMiniCarte fenMiniCarte ()
//    {
//        if (this.fenMiniCarte == null)
//            this.fenMiniCarte = new FenMiniCarte() ;
//        return this.fenMiniCarte ;
//    }
    
    // Renvoie le sélectionneur de fichiers du jeu
    public JFileChooser selectFichier ()
    {
        if (this.selectFichier == null)
            this.selectFichier = new JFileChooser() ;
        return this.selectFichier ;
    }
    

    // Détection de la fermeture de la fenêtre
    public void windowClosing (WindowEvent e)
    {
        // CODE Voir aux endroits où on utilise actionPeformed directement : il ne faudrait pas appeler
        //      une autre méthode plutôt ?
        // TODO Centraliser les actions ?
        new ActionQuitterApplication().actionPerformed (null) ;
    }
    

    // Méthodes non implémentées de WindowListener
    public void windowClosed      (WindowEvent e) {}
    public void windowActivated   (WindowEvent e) {}
    public void windowDeactivated (WindowEvent e) {}
    public void windowDeiconified (WindowEvent e) {}
    public void windowIconified   (WindowEvent e) {}
    public void windowOpened      (WindowEvent e) {}

}
