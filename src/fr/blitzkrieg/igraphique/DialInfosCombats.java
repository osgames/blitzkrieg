/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.Frame ;
import java.awt.GridLayout ;
import java.util.List ;

import javax.swing.JComponent ;
import javax.swing.JDialog ;
import javax.swing.JLabel ;
import javax.swing.JPanel ;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;




// Boît de dialogue affichant les statistiques des combats
public class DialInfosCombats extends JDialog
{

    private Partie partie ;                             // Partie concernée

    private InfosCombatsJoueur[] infosCombatsJoueurs ;  // Informations sur les combats des joueurs

    
    
    // Constructeur
    // TODO S'appliquer sur les noms des boîtes de dialogue
    public DialInfosCombats (Frame parent, Partie partie)
    {
        super (parent, "Rapport sur les combats", true) ;
        
        
        // Stocker la partie
        this.partie = partie ;
        
        
        // Créer les objets pour l'affichage des informations
        // TODO Arranger la présentation
        JComponent[][] composants = new JComponent[7][this.partie.nbJoueurs() + 1] ;
        // (en-têtes des lignes)
        int y = 0 ;
        composants[y++][0] = new JLabel() ;
        composants[y++][0] = new JLabel ("Nombre d'attaques") ;
        composants[y++][0] = new JLabel ("Nombre de défenses") ;
        composants[y++][0] = new JLabel ("Réussite des attaques") ;
        composants[y++][0] = new JLabel ("Réussite des défenses") ;
        composants[y++][0] = new JLabel ("Probabilité moyenne de réussite des attaques") ;
        composants[y++][0] = new JLabel ("Probabilité moyenne de réussite des défenses") ;
        // (colonnes des joueurs)
        this.infosCombatsJoueurs = new InfosCombatsJoueur[this.partie.nbJoueurs()] ;
        for (int i = 0 ; i < this.partie.nbJoueurs() ; i++)
        {
            y = 0 ;
            // TODO Ajouter le drapeau
            InfosCombatsJoueur infosCombatsJoueur = new InfosCombatsJoueur() ;
            this.infosCombatsJoueurs[i] = infosCombatsJoueur ;
            composants[y++][i+1] = infosCombatsJoueur.txt_nomJoueur ;
            composants[y++][i+1] = infosCombatsJoueur.txt_nbAttaques ;
            composants[y++][i+1] = infosCombatsJoueur.txt_nbDefenses ;
            composants[y++][i+1] = infosCombatsJoueur.txt_reussiteAttaques ;
            composants[y++][i+1] = infosCombatsJoueur.txt_reussiteDefenses ;
            composants[y++][i+1] = infosCombatsJoueur.txt_probaReussiteMoyenneAttaques ;
            composants[y++][i+1] = infosCombatsJoueur.txt_probaReussiteMoyenneDefenses ;
        }
        
        // Ranger les objets dans leur panneau
        JPanel p_infos = new JPanel (new GridLayout (composants.length, composants[0].length)) ;
        for (int i = 0 ; i < composants.length    ; i++)
        for (int j = 0 ; j < composants[i].length ; j++)
        {
            p_infos.add (composants[i][j]) ;
        }

        // Assembler les composants
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (p_infos,                   BorderLayout.CENTER) ;
        this.getContentPane().add (Fenetres.panneauOk (this), BorderLayout.SOUTH) ;
        
        // Autres actions sur la fenêtre
        Fenetres.ajouterEcouteurEchapPourMasquer  (this) ;
        Fenetres.ajouterEcouteurEntreePourMasquer (this) ;

        
        // Dimensionner et positionner la fenêtre
        // TODO Déterminer la taille, éventuellement en donnant une limite par rapport aux noms
        //      des joueurs et en réservant de la place pour les scores
        //          ! Ici on peut peut-être garder this.pack() (si les composants sont bien dimensionnés) ?
        // TODO Afficher au milieu de l'écran ; ce n'est pas censé s'afficher au milieu de la fenêtre
        //      mère et sinon au milieu de l'écran par défaut ?
        this.pack() ;
        Fenetres.centrer (this, parent) ;
    }
    
    
    // Affiche la boîte de dialogue
    // Les informations affichées sont mises à jour
    public void setVisible (boolean visible)
    {
        // Remplir les infos sur les joueurs quand on affiche la boîte de dialogue
        if (visible)
        {
            List<Joueur> joueurs = this.partie.joueurs() ;
            for (int i = 0 ; i < joueurs.size() ; i++)
            {
                Joueur joueur = joueurs.get (i) ;
                InfosCombatsJoueur infosCombatsJoueur = this.infosCombatsJoueurs[i] ;
                infosCombatsJoueur.txt_nomJoueur.setText                    (joueur.nom()) ;
                infosCombatsJoueur.txt_nbAttaques.setText                   (""+joueur.nbAttaquesLancees()) ;
                infosCombatsJoueur.txt_nbDefenses.setText                   (""+joueur.nbDefenses()) ;
                infosCombatsJoueur.txt_reussiteAttaques.setText             (""+joueur.proportionAttaquesReussies()*100 + "%") ;
                infosCombatsJoueur.txt_reussiteDefenses.setText             (""+joueur.proportionDefensesReussies()*100 + "%") ;
                infosCombatsJoueur.txt_probaReussiteMoyenneAttaques.setText (""+joueur.probaReussiteAttMoyenne()*100 + "%") ;
                infosCombatsJoueur.txt_probaReussiteMoyenneDefenses.setText (""+joueur.probaReussiteDefMoyenne()*100 + "%") ;
            }
        }
        
        // Appeler la méthode mère
        super.setVisible (visible) ;
    }

}



// Classe rassemblant les textes à afficher pour un joueur
class InfosCombatsJoueur
{
    public JLabel txt_nomJoueur                    = new JLabel("*") ;
    public JLabel txt_nbAttaques                   = new JLabel("*") ;
    public JLabel txt_nbDefenses                   = new JLabel("*") ;
    public JLabel txt_reussiteAttaques             = new JLabel("*") ;
    public JLabel txt_reussiteDefenses             = new JLabel("*") ;
    public JLabel txt_probaReussiteMoyenneAttaques = new JLabel("*") ;
    public JLabel txt_probaReussiteMoyenneDefenses = new JLabel("*") ;
}
