/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.Frame ;
import java.awt.GridLayout ;
import java.util.List ;

import javax.swing.JComponent ;
import javax.swing.JDialog ;
import javax.swing.JLabel ;
import javax.swing.JPanel ;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;



// Boîte de dialogue affichant les paramètres des joueurs
// TODO Comme pour les interfaces web, ce genre de choses pourrait être simplifié en Lisp/LSU : pas à
//      construire/mémoriser les objets d'affichage des infos, il suffit de dire où ils sont et ce qu'ils
//      sont censés afficher et la classe est générée tout seule [même sans LSU d'ailleurs : on pourrait
//      nommer les infos en décrivant la fenêtre et dans la méthode de mise à jour de l'affichage on
//      derait référence à ces noms]
public class DialInfosJoueurs extends JDialog
{

    private Partie partie ;                     // Partie concernée
    
    private InfosJoueur[] infosJoueurs ;        // Informations sur les joueurs

    
    
    // Constructeur
    public DialInfosJoueurs (Frame parent, Partie partie)
    {
        super (parent, "Paramètres des joueurs", true) ;
        
        
        // Stocker la partie
        this.partie = partie ;
        
        
        // Créer les objets pour l'affichage des informations
        // TODO Arranger la présentation
        JComponent[][] composants = new JComponent[10][this.partie.nbJoueurs() + 1] ;
        // (en-têtes des lignes)
        int y = 0 ;
        composants[y++][0] = new JLabel () ;
        composants[y++][0] = new JLabel ("Troupes par tour") ;
        composants[y++][0] = new JLabel ("Troupes par ville") ;
        composants[y++][0] = new JLabel ("Actions par tour") ;
        composants[y++][0] = new JLabel ("Actions par ville") ;
        composants[y++][0] = new JLabel ("Troupes maximum") ;
        composants[y++][0] = new JLabel ("Actions disponibles") ;
        composants[y++][0] = new JLabel ("Troupes en réserve") ;
        composants[y++][0] = new JLabel ("Points de victoire") ;
        composants[y++][0] = new JLabel ("Efficacité de l'armée") ;
        // (colonnes des joueurs)
        this.infosJoueurs = new InfosJoueur[this.partie.nbJoueurs()] ;
        for (int i = 0 ; i < this.partie.nbJoueurs() ; i++)
        {
            y = 0 ;
            // TODO Ajouter le drapeau
            InfosJoueur infosJoueur = new InfosJoueur() ;
            this.infosJoueurs[i] = infosJoueur ;
            composants[y++][i+1] = infosJoueur.txt_nomJoueur ;
            composants[y++][i+1] = infosJoueur.txt_troupesParTour ;
            composants[y++][i+1] = infosJoueur.txt_troupesParVille ;
            composants[y++][i+1] = infosJoueur.txt_actionsParTour ;
            composants[y++][i+1] = infosJoueur.txt_actionsParVille ;
            composants[y++][i+1] = infosJoueur.txt_troupesMaximum ;
            composants[y++][i+1] = infosJoueur.txt_actionsDisponibles ;
            composants[y++][i+1] = infosJoueur.txt_troupesReserve ;
            composants[y++][i+1] = infosJoueur.txt_pointsVictoire ;
            composants[y++][i+1] = infosJoueur.txt_efficaciteArmee ;
        }

        // Ranger les objets dans leur panneau
        JPanel p_infos = new JPanel (new GridLayout (composants.length, composants[0].length)) ;
        for (int i = 0 ; i < composants.length    ; i++)
        for (int j = 0 ; j < composants[i].length ; j++)
        {
            p_infos.add (composants[i][j]) ;
        }

        
        // Assembler les composants
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (p_infos,                   BorderLayout.CENTER) ;
        this.getContentPane().add (Fenetres.panneauOk (this), BorderLayout.SOUTH) ;
        
        // Autres actions sur la fenêtre
        Fenetres.ajouterEcouteurEchapPourMasquer  (this) ;
        Fenetres.ajouterEcouteurEntreePourMasquer (this) ;


        // Dimensionner et positionner la fenêtre
        // TODO Déterminer la taille, éventuellement en donnant une limite par rapport aux noms
        //      des joueurs et en réservant de la place pour les scores
        //          ! Ici on peut peut-être garder this.pack() (si les composants sont bien dimensionnés) ?
        // TODO Afficher au milieu de l'écran ; ce n'est pas censé s'afficher au milieu de la fenêtre
        //      mère et sinon au milieu de l'écran par défaut ?
        this.pack() ;
        Fenetres.centrer (this, parent) ;
    }
    
    
    // Affiche la boîte de dialogue
    // Les informations affichées sont mises à jour
    public void setVisible (boolean visible)
    {
        // Remplir les infos sur les joueurs quand on affiche la boîte de dialogue
        if (visible)
        {
            List<Joueur> joueurs = this.partie.joueurs() ;
            for (int i = 0 ; i < joueurs.size() ; i++)
            {
                Joueur joueur = joueurs.get (i) ;
                InfosJoueur infosJoueur = this.infosJoueurs[i] ;
                infosJoueur.txt_nomJoueur.setText          (joueur.nom()) ;
                infosJoueur.txt_troupesParTour.setText     (""+joueur.nbTroupesParTour()) ;
                infosJoueur.txt_troupesParVille.setText    (""+joueur.nbTroupesParVille()) ;
                infosJoueur.txt_actionsParTour.setText     (""+joueur.nbActionsParTour()) ;
                infosJoueur.txt_actionsParVille.setText    (""+joueur.nbActionsParVille()) ;
                infosJoueur.txt_troupesMaximum.setText     (""+joueur.nbTroupesMax()) ;
                infosJoueur.txt_actionsDisponibles.setText (""+joueur.nbActions()) ;
                infosJoueur.txt_troupesReserve.setText     (""+joueur.nbTroupesReserve()) ;
                infosJoueur.txt_pointsVictoire.setText     (""+joueur.nbPointsVictoire()) ;
                infosJoueur.txt_efficaciteArmee.setText    (""+joueur.efficacite()) ;
            }
        }
        
        // Appeler la méthode mère
        super.setVisible (visible) ;
    }

}



// Classe rassemblant les textes à afficher pour un joueur
class InfosJoueur
{
    public JLabel txt_nomJoueur          = new JLabel() ;
    public JLabel txt_troupesParTour     = new JLabel() ;
    public JLabel txt_troupesParVille    = new JLabel() ;
    public JLabel txt_actionsParTour     = new JLabel() ;
    public JLabel txt_actionsParVille    = new JLabel() ;
    public JLabel txt_troupesMaximum     = new JLabel() ;
    public JLabel txt_actionsDisponibles = new JLabel() ;
    public JLabel txt_troupesReserve     = new JLabel() ;
    public JLabel txt_pointsVictoire     = new JLabel() ;
    public JLabel txt_efficaciteArmee    = new JLabel() ;
}
