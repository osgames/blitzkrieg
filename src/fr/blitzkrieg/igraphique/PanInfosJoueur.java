/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.Color ;
import java.awt.Dimension ;
import java.util.Observable ;
import java.util.Observer ;

import javax.swing.JLabel ;
import javax.swing.JPanel ;
import javax.swing.border.EtchedBorder ;

import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.igraphique.ressources.ImagesAppli ;
import fr.blitzkrieg.utiles.igraphique.swing.JImage ;
import fr.blitzkrieg.utiles.igraphique.swing.Textes ;
import fr.blitzkrieg.utiles.igraphique.swing.gestplacement.PlaceurListe ;



// Panneau affichant les informations sur le joueur courant
public class PanInfosJoueur extends JPanel implements Observer
{

    private Partie partie ;                 // Partie à afficher
    
    private JImage img_drapeau ;            // Informations à afficher sur le joueur 
    private JLabel l_tour ;
    private JLabel l_nomJoueur ;
    private JLabel l_nbTroupesReserve ;
    private JLabel l_nbActionsReserve ;
    
    
    
    // Constructeur
    public PanInfosJoueur ()
    {
        // Paramétrer le panneau
        this.setBackground (Color.DARK_GRAY) ;
        
        // Créer les composants
        // (indiquer la dimension du panneau pour ne pas tout déplacer quand on n'affiche aucun drapeau)
        this.img_drapeau        = new JImage (Color.DARK_GRAY, null, new Dimension (32, 32)) ;        // TODO Constantes pour la couleur de fond (la même que le panneau Penterrain) et la taille du composant (la même taille que les images, y compris quand il n'y a pas d'image) : tout ça à ranger quelque part, ces infos sont utilisées aileurs
//        this.img_drapeau        = new JImage (Color.DARK_GRAY, null, null) ;
        this.img_drapeau.setBorder (null) ;
        this.l_tour             = Textes.creerTexte (" ", new Color (0xF0E68C)) ;        // TODO Constante
        this.l_nomJoueur        = Textes.creerTexte (" ", new Color (0xF0E68C)) ;
        this.l_nbTroupesReserve = Textes.creerTexte ("", new Color (0xF0E68C)) ;
        this.l_nbActionsReserve = Textes.creerTexte ("", new Color (0xF0E68C)) ;
        
        // Assembler le panneau
        JPanel p_tour             = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        JPanel p_nbTroupesReserve = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        JPanel p_nbActionsReserve = new JPanel (new PlaceurListe (PlaceurListe.LIGNE, PlaceurListe.ALIGN_BAS)) ;
        p_tour.setBackground             (Color.DARK_GRAY) ;
        p_nbTroupesReserve.setBackground (Color.DARK_GRAY) ;
        p_nbActionsReserve.setBackground (Color.DARK_GRAY) ;
        p_tour.add             (Textes.creerTexte ("Tour : ",  new Color (0xF0E68C))) ;
        p_nbTroupesReserve.add (Textes.creerTexte ("Réserve : ",  new Color (0xF0E68C))) ;
        p_nbActionsReserve.add (Textes.creerTexte ("Actions : ",  new Color (0xF0E68C))) ;
        p_tour.add (this.l_tour) ;
        p_nbTroupesReserve.add (this.l_nbTroupesReserve) ;
        p_nbActionsReserve.add (this.l_nbActionsReserve) ;
        
        this.setLayout (new PlaceurListe (PlaceurListe.COLONNE)) ;
//        this.setBorder (new EtchedBorder()) ;
        this.add (this.img_drapeau) ;
        this.add (this.l_nomJoueur, PlaceurListe.ALIGN_CENTRE) ;
        this.add (p_nbTroupesReserve) ;
        this.add (p_nbActionsReserve) ;
        this.add (p_tour) ;         // TODO Voir où on affiche cette information ; dans la version d'origine, ça n'apparaît que dans la barre de titre ; mieux dans un panneau, non ? laisser dans ce panneau ou mettre ailleurs ?
    }
    
    
    
    // Modifie la parie à afficher
    public void modifPartieAAfficher (Partie partie)
    {
        // Regarder la nouvelle partie
        if (this.partie != null)
            this.partie.deleteObserver (this) ;
        this.partie = partie ;
        if (this.partie != null)
            this.partie.addObserver (this) ;
        
        // Afficher les nouvelles informations
        mettreAJourInfosJoueur() ;
    }
    
    
    // Met à jour les informations sur le joueur
    private void mettreAJourInfosJoueur ()
    {
        // Mettre à jour les composants
        if (this.partie != null && this.partie.enCours() && ! this.partie.observationSuspendue() &&
             this.partie.joueurCourant() != null)
        {
            // TODO Aligner un peu les valeurs
            Joueur joueur = this.partie.joueurCourant() ;
            this.img_drapeau.modifImage     (ImagesAppli.charger (ImagesAppli.IMG_DRAPEAU, joueur.couleur())) ;
            this.img_drapeau.setBorder      (new EtchedBorder()) ;
            this.l_tour.setText             (this.partie.tourInitialisation() ? "déploiement" : "" + this.partie.numTour() + "/" + this.partie.nbTours()) ;
            this.l_nomJoueur.setText        (joueur.nom()) ;
            this.l_nbTroupesReserve.setText ("" + joueur.nbTroupesReserve()) ;
            this.l_nbActionsReserve.setText (this.partie.tourInitialisation() ? "(déploiement)" : "" + joueur.nbActions()) ;
        }
        else
        {
            this.img_drapeau.modifImage     (null) ;
            this.img_drapeau.setBorder      (null) ;
            this.l_tour.setText             ("") ;
            this.l_nomJoueur.setText        (" ") ;     // TODO ? Constante pour les deux endroits où on utilise cet espace dans cette classe ? Le but est de ne pas diminuer la hauteur du texte quand le nom est vidé (sinon on décale les informations en dessous)
            this.l_nbTroupesReserve.setText ("") ;
            this.l_nbActionsReserve.setText ("") ;
        }
        
        // Demander le réaffichage du panneau
        this.repaint() ;
    }


    // Mise à jour du panneau quand la partie observée est modifiée
    // TODO Faire un truc plus précis pour ne pas faire ça à chaque action sur la partie
    //          => Bof : même si la partie indique ce qui est modifié, la plupart du temps c'est le
    //             joueur (poser/retirer une troupe) et il faut mettre à jour le panneau
    // NOTE Avec plein de notifications successives, on ne refait pas plein de fois le dessin (sans doute
    //      que dans le traitements des événements Swing les appels successifs à repaint s'annulent dans
    //      la file d'attente)
    public void update (Observable o, Object arg)
    {
        if (this.partie.terminee())
            modifPartieAAfficher (null) ;
        else
            mettreAJourInfosJoueur() ;
    }
    
}
