/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import java.awt.BorderLayout ;
import java.awt.Frame ;
import java.awt.GridLayout ;

import javax.swing.JDialog ;
import javax.swing.JLabel ;
import javax.swing.JPanel ;

import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.utiles.igraphique.swing.Fenetres ;



// Boîte de dialogue affichant les paramètres de la partie
public class DialInfosPartie extends JDialog
{

    private Partie partie ;                 // Partie concernée
    
    private JLabel txt_nbTours ;            // Textes à afficher
    private JLabel txt_ptsVictoireTerre ;
    private JLabel txt_ptsVictoireVille ;
    private JLabel txt_invasionsNavales ;

    
    
    // Constructeur
    public DialInfosPartie (Frame parent, Partie partie)
    {
        super (parent, "Paramètres de la partie", true) ;
        

        // Stocker la partie
        this.partie = partie ;
        
        // Créer les objets pour l'affichage des informations
        JPanel p_infos = new JPanel (new GridLayout (4, 2)) ;
        p_infos.add (new JLabel ("Nombre de tours")) ;            p_infos.add (this.txt_nbTours          = new JLabel()) ;
        p_infos.add (new JLabel ("Points de victoire : terre")) ; p_infos.add (this.txt_ptsVictoireTerre = new JLabel()) ;
        p_infos.add (new JLabel ("Points de victoire : ville")) ; p_infos.add (this.txt_ptsVictoireVille = new JLabel()) ;
        p_infos.add (new JLabel ("Invasions navales")) ;          p_infos.add (this.txt_invasionsNavales = new JLabel()) ;
        
        // Assembler les composants
        this.getContentPane().setLayout (new BorderLayout()) ;
        this.getContentPane().add (p_infos,                   BorderLayout.CENTER) ;
        this.getContentPane().add (Fenetres.panneauOk (this), BorderLayout.SOUTH) ;
        
        // Autres actions sur la fenêtre
        Fenetres.ajouterEcouteurEchapPourMasquer  (this) ;
        Fenetres.ajouterEcouteurEntreePourMasquer (this) ;
        
        // Dimensionner et positionner la fenêtre
        this.pack() ;
        Fenetres.centrer (this, parent) ;
    }
    
    
    // Affiche la boîte de dialogue
    // Les informations affichées sont mises à jour
    public void setVisible (boolean visible)
    {
        // Mettre à jour les informations quand on affiche la boîte de dialogue
        if (visible)
        {
            this.txt_nbTours.setText          (""+this.partie.nbTours()) ;
            this.txt_ptsVictoireTerre.setText (""+this.partie.nbPointsCase()) ;
            this.txt_ptsVictoireVille.setText (""+this.partie.nbPointsVille()) ;
            this.txt_invasionsNavales.setText ((this.partie.invasionsNavalesPermises() ? "autorisées" : "non autorisées")) ;
        }
        
        // Appeler la méthode mère
        super.setVisible (visible) ;
    }
    
}
