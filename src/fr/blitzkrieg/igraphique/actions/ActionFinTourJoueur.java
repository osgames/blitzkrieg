/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.traitements.GestDeroulementPartie ;



// Fin du tour d'un joueur
public class ActionFinTourJoueur extends AbstractAction
{

    // Constructeur
    public ActionFinTourJoueur ()
    {
        super ("Fin du tour") ;
    }

    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        Blitzkrieg            application ;     // Instance de l'application
        GestDeroulementPartie gestPartie ;      // Gestionnaire de la partie courante
        Partie                partie ;          // Partie courante
        boolean listejoueursFinie ;             // Indique si on a fait le tour de la liste des joueurs (ou si la partie est terminée parce qu'il n'y a plus de joueurs)
        boolean partieTerminee ;                // Indique si la partie est terminée
        
        
        // Initialisations
        application = Blitzkrieg.instance() ;
        gestPartie  = application.gestPartieCourante() ;
        partie      = application.partieCourante() ;
        
        
        // Passer au joueur suivant
        // TODO Vérifier dans toutes les actions que la partie courante est en cours, au cas où des
        //      boutons soient restés affichés après la fin de partie
        if (partie != null)
        {
            listejoueursFinie = gestPartie.passerAuJoueurSuivant() ;
            partieTerminee    = partie.terminee() ;
        
            // Si la partie est terminée
            if (partieTerminee)
            {
// TODO Eventuellement s'appuyer une une méthode de ActionFinPartie (sans la demande de
//      confirmation, mais en mettant à jour l'instance de l'application par exemple)
                application.fenPrincipale().dialPoints().afficherScoresEtVainqueur() ;
                application.terminerPartie() ;
            }
            else
            {
                // Si un tour de jeu est terminé
                if (listejoueursFinie)
                {
                    // (affichage uniquement après le tour de déploiement)
                    if (partie.numTour() > 1)
                    {
                        partie.suspendreObservation() ;
                        application.fenPrincipale().dialPoints().afficherScoresCourants() ;
                        partie.permettreObservation() ;
                    }
                }
                
//                // Si le joueur courant est une IA
//                // TODO Intégrer autrement dans le code ? comme l'enchaînement des tours est essentiellement
//                //      géré par des événements, c'est plus simple de mettre ça là-dedans, en faisant jouer
//                //      les joueurs informatiques et en mimant une nouvelle fin de tour
//                //          !!! Euh... et si une IA joue en premier, Comment on fait ?
//                //              ? Sortir le mécanisme de gestion des tours des joueurs de ce contrôleur
//                //                et simplent l'appeler d'ici ?
//                //              ! Les actions des IA doivent s'afficher normalement dans l'interface
//                if (! partie.joueurCourant().estHumain())
//                {
//                    // Effectuer le tour de l'IA
//                    // (pour l'instant la seule IA disponible est le joueur "Territoire inexploré", qui ne
//                    //  fait rien)
//                    //      => IA inactive, il s'agit simplement d'un comportement particulier
//                    
//                    // Fin du tour
//                    // TODO Pour l'instant, simulation de l'exécution de l'action par l'IA : faire mieux ?
//                    this.actionPerformed (evt) ;
//                }
            }
        }
    }
    
}
