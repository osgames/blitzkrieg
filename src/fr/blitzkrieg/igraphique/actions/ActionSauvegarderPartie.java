/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;
import java.io.File ;

import javax.swing.AbstractAction ;
import javax.swing.JFileChooser ;
import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.stockage.StockagePartie ;



// Sauvegarde de la partie en cours
public class ActionSauvegarderPartie extends AbstractAction
{
    
    // Constructeur
    public ActionSauvegarderPartie ()
    {
        super ("Sauvegarder la partie") ;
    }
    
    
//    // Indique si l'action est possible ou non
//    // TODO Vérifier que ça marche dans un menu et implémenter pour les autres actions
//          => Ben ça marche pas : le bouton (pas dans un menu) est toujours désactivé et la méthode ne
//             semble pas appelée (?)
//    public boolean isEnabled ()
//    {
//System.out.println ("Partie en cours ? " + Blitzkrieg.instance().partieCourante() != null) ;
//        return Blitzkrieg.instance().partieCourante() != null ;
//    }
    
    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        Blitzkrieg application ;                // Instance de l'application
        Partie     partie ;                     // Partie courante

        
        // Initialisations
        application = Blitzkrieg.instance() ;
        partie = application.partieCourante() ;
        if (partie != null)
        {
            // Choisir un fichier
            // NOTE Tous les cas d'erreur devraient de toutes façons donner lieu à un message,
            //      y compris le cas resChoixFic == ERROR...
            //          ! Pas de confirmation de la sauvegarde, donc tous les autres cas doivent être
            //            (cas d'erreur) doivent être signalés
            // TODO On devrait peut-être avoir une classe séparée pour l'accès aux éléments graphiques
            //      de l'interface (attention, certains doivent être initialisés par la fenêtre principale)
            // TODO Demander à l'application de fournir l'outil de stockage ?
            JFileChooser selectFic = application.fenPrincipale().selectFichier() ;
            int resChoixFic = selectFic.showSaveDialog (application.fenPrincipale()) ;
            if (resChoixFic == JFileChooser.APPROVE_OPTION)
            {
                try
                {
                    // CODE Arranger et commenter
                    File ficChoisi = selectFic.getSelectedFile() ;
                    String chemFic = ficChoisi.getAbsolutePath() ;
                    chemFic += (chemFic.endsWith(".sauv") ? "" : ".sauv") ;     // NOTE A faire avant le test d'existence
                    ficChoisi = new File (chemFic) ;
                    
                    if (! ficChoisi.exists() ||
                        JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "Le fichier existe déjà. Ecraser ?", "Sauvegarde de la partie", JOptionPane.OK_CANCEL_OPTION))
                    {
//                        new StockagePartie().sauvegarder (partie, "test-sauv-partie.sauv", true) ;
                        new StockagePartie().sauvegarder (partie, ficChoisi.getAbsolutePath(), true) ;
                    }
                }
                catch (Throwable e)
                {
                    // TODO Essayer de déterminer la cause plus précisément (impossible de créer le fichier parce que le répertoire n'existe pas, pas de droits, plus de place, ... ; au moins séparer IOException du reste, mais si possible faire plus)
                    e.printStackTrace() ;
                    JOptionPane.showMessageDialog (application.fenPrincipale(), "Erreur lors de la sauvegarde de la partie.", "Erreur de sauvegarde", JOptionPane.ERROR_MESSAGE) ;
                }

            }
            else if (resChoixFic == JFileChooser.ERROR_OPTION)
            {
                JOptionPane.showMessageDialog (application.fenPrincipale(), "Erreur lors du choix du fichier.", "Erreur de choix de fichier", JOptionPane.ERROR_MESSAGE) ;
            }
        }

    }
    
}
