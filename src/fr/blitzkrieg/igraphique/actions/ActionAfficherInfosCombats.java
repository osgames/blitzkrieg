/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.igraphique.DialInfosCombats ;




// Affichage les statistiques sur les combats
public class ActionAfficherInfosCombats extends AbstractAction
{
    
    // Constructeur
    public ActionAfficherInfosCombats ()
    {
        super ("Rapport de combats") ;
    }
    
    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        DialInfosCombats dialInfosCombats = Blitzkrieg.instance().fenPrincipale().dialInfosCombats() ;
        if (dialInfosCombats != null)
            dialInfosCombats.setVisible (true) ;
    }

}
