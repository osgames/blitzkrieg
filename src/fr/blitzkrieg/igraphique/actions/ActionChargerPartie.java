/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;
import java.io.File ;

import javax.swing.AbstractAction ;
import javax.swing.JFileChooser ;
import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.stockage.StockagePartie ;



// Chargement d'une partie ou d'un scénario
public class ActionChargerPartie extends AbstractAction
{

    // Constructeur
    public ActionChargerPartie ()
    {
        super ("Charger une partie") ;
    }
    
    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        Blitzkrieg application ;                // Instance de l'application
        Partie     partie ;                     // Partie courante

        
        // Initialisations
        application = Blitzkrieg.instance() ;
        partie = application.partieCourante() ;

        // Demander confirmation si une partie est en cours
        // CODE Arranger le code
        if (partie == null ||
            JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "La partie en cours sera perdue. Continuer ?", "Chargement de la partie", JOptionPane.YES_NO_OPTION))
        {
            // Choisir un fichier
            JFileChooser selectFic = application.fenPrincipale().selectFichier() ;
            int resChoixFic = selectFic.showOpenDialog (application.fenPrincipale()) ;
            if (resChoixFic == JFileChooser.APPROVE_OPTION)
            {
                File ficChoisi = selectFic.getSelectedFile() ;
                
                // Vérifier que le fichier existe
                if (! ficChoisi.exists())
                {
                    JOptionPane.showMessageDialog (application.fenPrincipale(), "Impossible de charger la partie : le fichier n'existe pas.", "Erreur de chargement", JOptionPane.ERROR_MESSAGE) ;
                }
                else
                {
                    // Terminer la partie en cours
                    if (partie != null)
                        application.terminerPartie() ;
                    
                    // Charger la nouvelle partie
                    try
                    {
//                        partie = new StockagePartie().charger ("test-sauv-partie.sauv") ;
//                        application.creerPartie (partie) ;
                        partie = application.creerPartie (ficChoisi) ;
                    }
                    catch (Throwable e)
                    {
                        // TODO Essayer de déterminer la cause plus précisément (droits de lecture, format invalide, ... : faire des tests pour lever des exceptions plus précises si besoin et afficher une erreur la plus explicite possible ; fusionner avec le test actuel qui vérifie l'existence du fichier)
                        // TODO Afficher les piles d'appel comme "détails" dans la boîte de dialogue ? mais ça nécessiterait la création de boîtes de dialogue perso
                        e.printStackTrace() ;
                        JOptionPane.showMessageDialog (application.fenPrincipale(), "Impossible de charger la partie.", "Erreur de chargement", JOptionPane.ERROR_MESSAGE) ;
                    }
                }
            }
        }

    }

}
