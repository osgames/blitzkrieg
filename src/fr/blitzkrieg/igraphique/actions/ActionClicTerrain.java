/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.MouseEvent ;
import java.awt.event.MouseListener ;

import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.igraphique.PanTerrain ;
import fr.blitzkrieg.igraphique.animations.Animation ;
import fr.blitzkrieg.traitements.Regles ;



// Clic sur le terrain (la carte principale)
// NOTE La classe s'appelle ActionXXX mais elle n'implémente pas Action (actionPerformed ne convient pas
//      à ce type d'événement)
public class ActionClicTerrain implements MouseListener
{

    // Exécuter l'action
    public void mousePressed (MouseEvent evt)
    {
        Partie partie ;             // Partie courante ;
        Regles regles ;             // Règles de la partie courante
        
        
        // Initialisations
        partie = Blitzkrieg.instance().partieCourante() ;
        regles = Blitzkrieg.instance().reglesCourantes() ;
        
        // Vérifier que la partie est bien en cours
        if (partie != null && partie.joueurCourant() != null)
        {
            Blitzkrieg application = Blitzkrieg.instance() ;
            Terrain terrain        = partie.terrain() ;
            Joueur  joueurCourant  = partie.joueurCourant() ;
            
            // Calculer les coordonnées de la case sur laquelle on a cliqué
            // TODO Réfléchir où mettre toutes ces constantes
            // TODO Unifier avec les autres endroits où on fait ce calcul/test (dans PanInfosAttaqueDefense)
            //          => Fonction du panneau du terrain qui renvoie la case affichée en (x,y) (null si pas de case)
            int xCase = (int) Math.floor ((double) evt.getX() / PanTerrain.TAILLE_CASE) ;
            int yCase = (int) Math.floor ((double) evt.getY() / PanTerrain.TAILLE_CASE) ;
            if (xCase < terrain.largeur() && yCase < terrain.hauteur())
            {
                Case caseCliquee = terrain.caseEn (xCase, yCase) ;
                
                // Retirer une troupe
                // NOTE Noter quelque part : à plein d'endroits il faut faire attention aux tests sur le
                //      propriétaire : il n'y a pas forcément de propriétaire à une case, donc il faut
                //      souvent faire le teste d'égalité dans l'autre sens ou faire un premier teste qui
                //      écarte la possibilité de l'absence de propriétaire
                //          => Bien tester tout ça dans les tests automatisés
                // NOTE Oui, on peut attaquer les mers, mais ce sont les méthodes dans les règles qui
                //      sont chargées de ces vérifications ; ici on ne fait que le minimum pour
                //      choisir quelle action a été tentée et exécuter la bonne action (qui peut
                //      s'annuler si elle n'est pas autorisée)
                boolean actionExecutee = false ;
                if (joueurCourant.equals (caseCliquee.proprietaire()) &&
                    caseCliquee.troupePresente())
                {
                    actionExecutee = regles.retirer (joueurCourant, xCase, yCase) ;
                }
                // Déposer une troupe en territoire ami
                else if (joueurCourant.equals (caseCliquee.proprietaire()) &&
                         ! caseCliquee.troupePresente())
                {
                    actionExecutee = regles.deposer (joueurCourant, xCase, yCase) ;
                }
                // Attaquer une case ennemie
                else if (! joueurCourant.equals (caseCliquee.proprietaire()))
                {
                    actionExecutee = executerActionAttaquer (joueurCourant, caseCliquee) ;
                }
                
                // Vérifier s'il est temps de proposer la fin du tour
                if (actionExecutee)
                {
                    // TODO Il faudrait noter quelque part quand on l'a déjà fait, pour ne pas reposer
                    //      la question au même tour (à annuler dans l'action de fin de tour)
                    if (! partie.tourInitialisation() &&
                        (joueurCourant.nbActions() == 0 ||
                         joueurCourant.nbActions() == 1 && joueurCourant.nbTroupesReserve() == 0))
                    {
try
{
    Thread.sleep (1000) ;       // TODO Quelque chose un peu mieux qu'attendre un certain temps ? Et interdire de lancer des actions avant que les animations soient terminées, pour éviter que n'importe quoi se passe au même moment ?
                                //          => Oui, ça ne va pas : l'attente se fait dans le fil de l'interface graphique, donc rien ne s'affiche pendant qu'on attend : travailler sur l'enchaînement des actions et affichage pour éviter que ça se repasse
}
catch (InterruptedException e)
{
}
                        // TODO Pas terrible de recréer l'action comme ça ; il faudrait plutôt pouvoir
                        //      y accéder quelque part
                        //          => Un endroit centralisé pour accéder  aux actions, comme pour les
                        //             composants graphiques
                        if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "Fin du tour ?", "Fin du tour ?", JOptionPane.YES_NO_OPTION))
                            new ActionFinTourJoueur().actionPerformed (null) ;
                    }
                }
            }
        }
    }
    
    
    // TODO A organiser autrement ? L'IA doit pouvoir déclencher ces actions
    // TODO ? Revérifier que la partie est en cours, ... ?
    //          => A réfléchir avec les problèmes de synchronisation entre l'interface et l'IA notamment
    // CODE Commenter
    public boolean executerActionRetirer (Joueur joueur, Case caseConcernee)
    {
        Blitzkrieg application = Blitzkrieg.instance() ;
        Regles regles          = application.reglesCourantes() ;
        
        return regles.retirer (joueur, caseConcernee.x(), caseConcernee.y()) ;
    }
    public boolean executerActionDeposer (Joueur joueur, Case caseConcernee)
    {
        Blitzkrieg application = Blitzkrieg.instance() ;
        Regles regles          = application.reglesCourantes() ;
        
        return regles.deposer (joueur, caseConcernee.x(), caseConcernee.y()) ;
    }
    public boolean executerActionAttaquer (Joueur joueur, Case caseConcernee)
    {
        Blitzkrieg application = Blitzkrieg.instance() ;
        Regles regles          = application.reglesCourantes() ;
        boolean actionExecutee ;
        
        
        // Exécuter l'action
        Animation animation = application.animations().creerAnimationAttaque (joueur, caseConcernee) ;
        actionExecutee = regles.attaquer (joueur, caseConcernee.x(), caseConcernee.y()) ;
        // (animation)
        // NOTE L'autre solution (peut-être meilleure, c'est que dans les événements que
        //      reçoit le panneau de dessin du terrain il y ait un paramètre indiquant
        //      quand il s'agit d'une attaque (avec les informations sur la case concernée
        //      avant l'attaque) [nécessite une méthode particulière pour les attaques dans
        //      le terrain, ce qui est peut-être moyen (bof, ça veut dire que le terrain ne
        //      subit pas forcément la même chose quand il est attaqué que quand on dépose
        //      simplement une troupe dessus)])
        if (actionExecutee)
            application.animations().lancerAnimation (animation) ;

        // Renvoyer le résultat de l'action
        return actionExecutee ;
    }


    // Méthodes non implémentées de MouseListener
    public void mouseClicked  (MouseEvent e) {}
    public void mouseEntered  (MouseEvent e) {}
    public void mouseExited   (MouseEvent e) {}
    public void mouseReleased (MouseEvent e) {}
    
}
