/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;
import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;



// Fin du la partie courante
public class ActionFinPartie extends AbstractAction
{

    // Constructeur
    public ActionFinPartie ()
    {
        super ("Fin de la partie") ;
    }

    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        // Initialisations
        Blitzkrieg application = Blitzkrieg.instance() ;
        Partie     partie      = application.partieCourante() ;
        
        // Demander confirmation avant de terminer la partie
        if (partie != null &&
            JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "Terminer la partie ?", "Fin de la partie", JOptionPane.YES_NO_OPTION))
        {
            application.terminerPartie() ;
        }
    }
    
}
