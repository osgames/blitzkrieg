/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.actions;

import java.awt.event.ActionEvent ;

import javax.swing.AbstractAction ;
import javax.swing.JOptionPane ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.igraphique.DialChoixScenario ;



// Création d'une nouvelle partie
public class ActionCreerPartie extends AbstractAction
{

    // Constructeur
    public ActionCreerPartie ()
    {
        super ("Nouvelle partie") ;
    }

    
    // Exécuter l'action
    public void actionPerformed (ActionEvent evt)
    {
        Blitzkrieg application ;                // Instance de l'application
        Partie     partie ;                     // Partie courante

        
        // Initialisations
        application = Blitzkrieg.instance() ;
        partie = application.partieCourante() ;

        // Demander confirmation si une partie est en cours
        if (partie == null ||
            JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog (application.fenPrincipale(), "La partie en cours sera perdue. Continuer ?", "Nouvelle la partie", JOptionPane.YES_NO_OPTION))
        {
            // Afficher la boîte de dialogue de choix de partie
            DialChoixScenario dialScenarios = application.fenPrincipale().dialChoixScenario() ;
            dialScenarios.chargerScenarios() ;
            dialScenarios.setVisible (true) ;
            
            // Récupérer le scénario choisi
            String scenarioChoisi = dialScenarios.scenarioSelectionne() ;
            
            // Créer la partie
            if (scenarioChoisi != null)
            {
                // Terminer l'éventuelle partie en cours
                application.terminerPartie() ;
                
                // Créer la partie selon le type de scénario choisi
//                // TODO Et si on avait un fichier pour le scénario aléatoire, qui serait beaucoup plus léger
//                //      et reconnu lors du chargement ? A ce moment-là, le générateur de carte aléatoire
//                //      serait appelé pour créer tout ça ; ça permettrait même de stocker des scénarios
//                //      "aléatoires" reproductibles (tant qu'on ne change pas le générateur de carte), par
//                //      exemple pour stocker le scénario aléatoire le temps de la partie et permettre de
//                //      le recommencer, en stockant simplement la graine aléatoire
//                //          ! Ca permettrait d'éviter le cas particulier pour ce scénario et d'en avoir
//                //            plusieurs différents (avec des tailles de carte et des nombre de joueurs
//                //            différents par exemple)
//                // TODO Une fois choisi, faire apparaître une fenêtre permettant de tout paramétrer (pas
//                //      comme dans Pendulous, ou des boîtes de dialogues successives apparaissaient pour
//                //      choisir l'IA, les noms, ...)
//                if (scenarioChoisi.equals ("Scénario aléatoire"))
//                {
//                    Blitzkrieg.instance().creerPartie() ;
//                }
//                else
//                {
                // Charger le scénario
                try
                {
                    // TODO Utiliser les constantes (à créer)
                    //      + les remplacements de "/" par File.separator [lors de l'initialisation de la constante]
//                        partie = new StockagePartie().charger ("donnees/scenarios/" + scenarioChoisi) ;
//                        application.creerPartieScenario (partie) ;
                    application.creerPartieScenario (scenarioChoisi) ;
                }
                catch (Throwable e)
                {
                    // TODO Essayer de déterminer la cause plus précisément (droits de lecture, format invalide, ... : faire des tests pour lever des exceptions plus précises si besoin et afficher une erreur la plus explicite possible ; fusionner avec le test actuel qui vérifie l'existence du fichier)
                    // TODO Afficher les piles d'appel comme "détails" dans la boîte de dialogue ? mais ça nécessiterait la création de boîtes de dialogue perso
                    e.printStackTrace() ;
                    JOptionPane.showMessageDialog (application.fenPrincipale(), "Impossible de charger le scénario.", "Erreur de chargement", JOptionPane.ERROR_MESSAGE) ;
                }
//                }
                    
            }
        }
    }
    
}
