/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.animations;

import java.awt.event.ActionListener ;

import javax.swing.Timer ;

import fr.blitzkrieg.donnees.Joueur ;



// Description d'une animation sur une case
// NOTE Pour l'instant c'est implicitement une animation d'attaque
public class Animation
{
    public int   x ;                // Coordonnées de la case
    public int   y ;
    public Joueur  proprietaire ;   // Informations sur l'état de la case avant l'attaque
    public boolean troupePresente ;
    public boolean caseVivante ;
    public long  dateDeb ;          // Date de début de l'animation
    public long  dateFin ;          // Date de fin de l'animation
    public int   nbImages ;         // Nombre total d'images affichées dans l'animation
    public int   dureeImage ;       // Durée pendant laquelle une image de l'animation reste affichée (en ms)
    
    private Timer chrono ;          // Chronomètre déclenchant les réaffichages

    
    
    // Constructeur
    public Animation (int x, int y, Joueur proprietaire, boolean troupePresente, boolean caseVivante,
                      int nbImages, int dureeImage)
    {
        this.x              = x ;
        this.y              = y ;
        this.proprietaire   = proprietaire ;
        this.troupePresente = troupePresente ;
        this.caseVivante    = caseVivante ;
        this.nbImages       = nbImages ;
        this.dureeImage     = dureeImage ;
    }
    
    
    // Active un chronomètre pour demander des réaffichages réguliers de l'image pour créer l'animation
    // L'écouteur en paramètre est celui qui est appelé quand le chronomètre est déclenché
    public void demarrer (ActionListener ecouteur)
    {
//System.out.println ("Lancement de l'animation") ;
        this.dateDeb = System.currentTimeMillis() ;
        this.dateFin = this.dateDeb + this.nbImages * this.dureeImage ;
        this.chrono = new Timer (this.dureeImage, ecouteur) ;
        this.chrono.start() ;
    }
    
    
    // Arrête le chronomètre de l'animation (arrête d'envoyer des événements pour redessiner l'image)
    public void arreter ()
    {
//System.out.println ("Arrêt de l'animation") ;
        if (this.chrono != null)
        {
            this.chrono.stop() ;
            this.chrono = null ;
        }
    }
    
    
    // Indique si l'animation est en cours
    // NOTE Tant que la création se passe dans le fil des événements de Swing, on peut initialiser
    //      l'animation tranquillement et l'animation ne sera pas dessinée avant d'être commencée par
    //      hasard (le panneau ne peut pas être redessiné en même temps que l'animation est créée puisque
    //      tout se passe dans le même fil d'exécution)
    public boolean estEnCours ()
    {
        return (this.chrono != null) && (this.chrono.isRunning()) ;
    }
    
    
    // Indique si l'animation est en phase "explosion"
    public boolean estEnPhaseExplosion ()
    {
        int numPhase = (int) Math.ceil ((double) (System.currentTimeMillis() - this.dateDeb) / this.dureeImage) ;
        return (numPhase % 2) == 0 ;
    }
    
}
