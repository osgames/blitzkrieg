/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique.animations;

import java.util.ArrayList ;
import java.util.Iterator ;
import java.util.List ;

import fr.blitzkrieg.Blitzkrieg ;
import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Joueur ;



// Classe de gestion des animations du jeu
// NOTE Pas de synchronisation dans la gestion des animations, c'est prévu pour être géré
//      intégralement dans le fil des événements de Swing
public class Animations
{

    private static final int DUREE_IMAGE = 100 ;    // Durée d'affichage d'une image dans une animation
    
    
    private List<Animation> animationsEnCours ;     // Animations en cours
    
    
    
    // Constructeur
    public Animations ()
    {
        this.animationsEnCours = new ArrayList() ;
    }
    
    
    // Renvoie les animations en cours
    // (renvoie une copie, notamment parce qu'on peut supprimer des éléments de la liste)
    public List<Animation> animationsEnCours ()
    {
        return new ArrayList (this.animationsEnCours) ;
    }
    
    
    // Crée une animation pour l'ataque d'une case
    // NOTE Conserver l'attaquant comme paramètre, ça peut servir pour créer l'animation d'attaque
    public Animation creerAnimationAttaque (Joueur attaquant, Case caseAttaquee)
    {
        return new Animation (caseAttaquee.x(), caseAttaquee.y(),
                              caseAttaquee.proprietaire(), caseAttaquee.troupePresente(), caseAttaquee.estVivante(),
                              5, DUREE_IMAGE) ;
    }
    
    
    // Lance une animation
    public void lancerAnimation (Animation animation)
    {
        this.animationsEnCours.add (animation) ;
        animation.demarrer (Blitzkrieg.instance().fenPrincipale().panTerrain()) ;
    }
    
    
    // Retire une animation de la liste eds animations en cours
    public void arreterAnimation (Animation animation)
    {
        animation.arreter() ;
        this.animationsEnCours.remove (animation) ;
    }
    
    
    // Arrête toutes les animations dont la date de fin est dépassée
    public void arreterAnimationsTerminees ()
    {
        Iterator<Animation> itAnim = this.animationsEnCours.iterator() ;
        while (itAnim.hasNext())
        {
            Animation animation = itAnim.next() ;
            if (animation.dateFin <= System.currentTimeMillis())
            {
                animation.arreter() ;
                // TODO Erreur à corriger : j'ai eu une ConcurrentModificationException sur cette ligne !
                //      (a priori deux fois cette même méthode lancées en même temps)
                itAnim.remove ();
            }
        }
    }
    
}
