/**
 * 
 * Copyright 2009-2010 Sylvain Lavalley
 * 
 * This file is part of Blitzkrieg.
 *
 * Blitzkrieg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Blitzkrieg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blitzkrieg. If not, see <http://www.gnu.org/licenses/>.
 *  
 */
package fr.blitzkrieg.igraphique;

import static fr.blitzkrieg.igraphique.PanTerrain.COUL_FOND_CARTE ;
import static fr.blitzkrieg.igraphique.PanTerrain.COUL_MERS ;
import static fr.blitzkrieg.igraphique.PanTerrain.COUL_NON_DECOUVERTE ;

import java.awt.BorderLayout ;
import java.awt.Dimension ;
import java.awt.Graphics ;
import java.util.Observable ;
import java.util.Observer ;

import javax.swing.JPanel ;
import javax.swing.JScrollPane ;

import fr.blitzkrieg.donnees.Case ;
import fr.blitzkrieg.donnees.Partie ;
import fr.blitzkrieg.donnees.Terrain ;
import fr.blitzkrieg.donnees.Case.TypeCase ;



// Panneau gérant la mini-carte
// TODO C'est là qu'il faudra ajouter la loupe
public class PanMiniCarte extends JPanel implements Observer
{

    private PanTraceMiniCarte p_traceMiniCarte ;        // Panneau de dessin de la carte
    
    
    
    // Constructeur
    // CODE A arranger (tout ce fichier source d'ailleurs)
    public PanMiniCarte ()
    {
        // Créer le panneau de dessin
        this.p_traceMiniCarte = new PanTraceMiniCarte() ;
        
        // Assembler les composants
        this.setLayout (new BorderLayout()) ;
        JScrollPane defil_minicarte = new JScrollPane (this.p_traceMiniCarte) ;
        defil_minicarte.setBorder (null) ;
        defil_minicarte.setPreferredSize (new Dimension (100, 100)) ;               // TODO Prendre la taille disponible ? Donner une autre taille ? Voir avec la mise en palce finale de l'interface, avec le dimensionnement des apnneaux voisins
        defil_minicarte.setMinimumSize   (defil_minicarte.getPreferredSize()) ;
        defil_minicarte.setMaximumSize   (defil_minicarte.getPreferredSize()) ;
        this.add (defil_minicarte, BorderLayout.CENTER) ;
    }
    
    
    
    // Renvoie la partie observée
    private Partie partie ()
    {
        return this.p_traceMiniCarte.partie() ;
    }
    
//    // Modifie la partie observée
//    private void modifPartie (Partie partie)
//    {
//        this.p_traceMiniCarte.modifPartie (partie) ;
//    }
    
    
    // Modifie la partie à afficher
    public void modifPartieAAfficher (Partie partie)
    {
        // Ne plus regarder l'ancienne partie
        if (this.partie() != null)
            this.partie().deleteObserver (this) ;
        
//        this.modifPartie (partie) ;
        
        // Transmettre l'information au panneau de dessin
        this.p_traceMiniCarte.modifPartieAAfficher (partie) ;

        // Regarder la nouvelle partie
        if (this.partie() != null)
            this.partie().addObserver (this) ;
    }
    

    // Mise à jour du panneau quand la partie observée est modifiée
    // TODO Faire un truc plus précis pour ne pas faire ça à chaque action sur la partie
    //          => Bof : même si la partie indique ce qui est modifié, la plupart du temps c'est le
    //             joueur (poser/retirer une troupe) et il faut mettre à jour le panneau
    // NOTE Avec plein de notifications succissives, on ne refait pas plein de fois le dessin (sans doute
    //      que dans le traitements des événements Swing les appels successifs à repaint s'annulent dans
    //      la file d'attente)
    public void update (Observable o, Object arg)
    {
        this.p_traceMiniCarte.update() ;
    }

}



// Panneau traçant la carte
class PanTraceMiniCarte extends JPanel
{
    
    protected static final int TAILLE_COTE_CASE = 2 ;         // Taille des cases sur la mini-carte
    
    
    private Partie partie ;             // Partie à afficher
    
    
//private JFrame fenMiniCarte ;
//public PanMiniCarte (JFrame fenMiniCarte)
//{
//    this.fenMiniCarte = fenMiniCarte ;
//}
    
    // Constructeur
    public PanTraceMiniCarte ()
    {
// TODO Si ça ne sert à rien, supprimer le constructeur
//        this.setPreferredSize (new Dimension (100, 100)) ;
//        this.setMinimumSize   (this.getPreferredSize()) ;
//        this.setMaximumSize   (this.getPreferredSize()) ;
    }
    
    
    // Renvoie la partie observée
    public Partie partie ()
    {
        return this.partie ;
    }
    
    // Modifie la partie observée
    public void modifPartie (Partie partie)
    {
        this.partie = partie ;
    }

    
    
    
    // Modifie la partie à afficher
    public void modifPartieAAfficher (Partie partie)
    {
        // Redimensionner la fenêtre pour s'ajuster à la taille du terrain
        if (partie != null)
        {
            this.setPreferredSize (new Dimension (partie.terrain().largeur() * TAILLE_COTE_CASE,
                                                  partie.terrain().hauteur() * TAILLE_COTE_CASE)) ;
            this.setSize          (this.getPreferredSize()) ;       // TODO Je ne sais pas pourquoi, mais sinon ça ne fonctionne pas dans la fenêtre défilante
//            this.setMinimumSize   (this.getPreferredSize()) ;
//            this.setMaximumSize   (this.getPreferredSize()) ;
        }

        // Mémoriser la partie
        this.partie = partie ;
        
        // Afficher les nouvelles informations
        mettreAJourAffichage() ;
    }
    
    
    // Mise à jour du panneau quand la partie observée est modifiée
    // TODO Faire un truc plus précis pour ne pas faire ça à chaque action sur la partie
    //          => Bof : même si la partie indique ce qui est modifié, la plupart du temps c'est le
    //             joueur (poser/retirer une troupe) et il faut mettre à jour le panneau
    // NOTE Avec plein de notifications succissives, on ne refait pas plein de fois le dessin (sans doute
    //      que dans le traitements des événements Swing les appels successifs à repaint s'annulent dans
    //      la file d'attente)
//    public void update (Observable o, Object arg)
    public void update ()
    {
//        if (this.partie.terminee())
//            modifPartieAAfficher (null) ;
//        else
//            mettreAJourAffichage() ;
        
        // (le panneau peut continuer à afficher une partie terminée tant qu'on ne lui a pas dit d'arrêter)
        mettreAJourAffichage() ;
    }


    // Met à jour l'affichage du panneau
    private void mettreAJourAffichage ()
    {
        this.repaint() ;
    }
    
    
    // Trace le dessin sur le panneau
    protected void paintComponent (Graphics g)
    {
//System.out.println ("Toujours dessus supporté : " + this.fenMiniCarte.isAlwaysOnTopSupported()) ;
//System.out.println ("Toujours dessus : " + this.fenMiniCarte.isAlwaysOnTop()) ;
//this.fenMiniCarte.setAlwaysOnTop (false) ;
//this.fenMiniCarte.setAlwaysOnTop (true) ;
//this.fenMiniCarte.toFront() ;
        // CODE A arranger et commenter
        // TODO Tracer dans une image tampon
        // PERF Image à conserver en cache ? 
        g.setColor (COUL_FOND_CARTE) ;
        g.fillRect (0, 0, this.getWidth(), this.getHeight()) ;
        if (this.partie == null || ! this.partie.commencee())
        {
        }
        else if (this.partie.observationSuspendue())
        {
            Terrain terrain = this.partie.terrain() ;
            g.setColor (COUL_NON_DECOUVERTE) ;
            g.fillRect (0, 0, terrain.largeur() * TAILLE_COTE_CASE, terrain.largeur() * TAILLE_COTE_CASE) ;
        }
        else
        {
            Terrain terrain = this.partie.terrain() ;
            int largeur = terrain.largeur() ;
            int hauteur = terrain.hauteur() ;
            for (int x = 0 ; x < largeur ; x++)
            for (int y = 0 ; y < hauteur ; y++)
            {
                Case caseCourante = terrain.caseEn (x, y) ;
                if (! caseCourante.decouverte (this.partie.joueurCourant()))
                    g.setColor (COUL_NON_DECOUVERTE) ;
                else if (caseCourante.type() == TypeCase.CASE_MER)
                    g.setColor (COUL_MERS) ;
                else if (caseCourante.proprietaire() != null)
                    g.setColor (caseCourante.proprietaire().couleur()) ;
                else
                    throw new IllegalStateException ("Cas non géré pour la case en (" + x + ", " + y + ")") ;
                g.fillRect (x * TAILLE_COTE_CASE, y * TAILLE_COTE_CASE, TAILLE_COTE_CASE, TAILLE_COTE_CASE) ;
            }
        }
    }
    
}
