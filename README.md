# Blitzkrieg

Originally developed at https://sourceforge.net/projects/blitzkrieg/ by [syll](http://sourceforge.net/users/syll) and published under GPL 3.0.

The original maintainer Sylvain Lavalley prefers if forks do not use the same name.